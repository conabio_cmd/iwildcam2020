#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse

from tensorflow.keras.applications import EfficientNetB3, EfficientNetB7, ResNet152

from conabio_ml.utils.dataset_utils import read_labelmap_file
from conabio_ml_vision.datasets.datasets import ConabioImageDataset
from conabio_ml.utils.logger import get_logger

from scripts.utils import *
from iWildcam2020.src.utils_2020 import *

logger = get_logger(__name__)

"""En este script se realiza un ensamble de los modelos EfficientNetB7, EfficientNetB3 y ResNet152,
además de utilizar el modelo geo prior, entrenado con los datos de localización y fecha de las
colecciones WCS, iNat 2017, 2018 y 2021
"""


experiment = "ensemble_2_efficient_resnet152_geo_priors_train100"
num_submission = 44
add_sub_suffix = False

model_file_name_efficient_b3 = 'efficientnet_b3_train_1.model.hdf5'
model_file_name_efficient_b7 = 'efficientnet_b7_train_1.model.hdf5'
model_file_name_resnet = 'resnet_152_train_1.model.hdf5'

TRAIN_PERC = 1.
BATCH_SIZE_TRAIN = 32
BATCH_SIZE_EVAL = 16
N_EPOCHS = 20
SCORE_THRES = 0.3
EFFIC_B7_SIZES = (600, 600)
EFFIC_B3_SIZES = (224, 224)
RESNET_SIZES = (224, 224)
MEGADET_LABELMAP = ConabioImageDataset.MEGADETECTOR_V4_LABELMAP

repo_base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020"
base_path = os.path.join(repo_base_path, "iWildcam2020")
base_2021_path = os.path.join(repo_base_path, "iWildcam2021")
files_path = os.path.join(base_path, "files")
data_path = os.path.join(base_path, "data")
data_2021_path = os.path.join(base_2021_path, "data")
results_path = os.path.join(base_path, "results", experiment)

inat_images = os.path.join(data_2021_path, "iNat_images")
wcs_metadata = os.path.join(data_path, "WCS_metadata")
wcs_images = os.path.join(data_path, "WCS_images")
wcs_image_level_anns_json_file = os.path.join(wcs_metadata, "iwildcam2020_train_annotations.json")
wcs_test_info_json = os.path.join(wcs_metadata, "iwildcam2020_test_information.json")
sample_submission_path = os.path.join(wcs_metadata, "sample_submission.csv")
wcs_test_images_dir = os.path.join(wcs_images, "test_CLAHE_WB")
wcs_train_images_dir = os.path.join(wcs_images, "train_CLAHE_WB")
test_crops_dir = os.path.join(wcs_images, "crops_of_test_set_MegadetV4")
train_crops_dir = os.path.join(wcs_images, "crops_of_train_set_MegadetV4")
inat_17_18_train_images_dir = os.path.join(inat_images, "inaturalist_17_18_CLAHE_WB")
inat_21_train_images_dir = os.path.join(inat_images, "inaturalist_2021_CLAHE_WB")

files_dataset = os.path.join(files_path, "datasets")
files_dets = os.path.join(files_path, "detections")
files_fp = os.path.join(files_path, "false_positives")
files_iNat = os.path.join(files_path, "inat_cats")
files_gp = os.path.join(files_path, "geo_prior")
wcs_dataset_json_file = os.path.join(files_dataset, "wcs_from_dets_MegadetV4.json")
wcs_dataset_empty_json_file = os.path.join(files_dataset, "wcs_from_dets_empty_MegadetV4.json")
dataset_inat_21_json_file = os.path.join(files_dataset, "inat_21_from_dets_MegadetV4.json")
dataset_inat_17_18_json_file = os.path.join(files_dataset, "inat_17_18_curated.json")
detections_on_test_set_path = os.path.join(files_dets, "test_MegadetV4.csv")
false_pos_dets_test_csv = os.path.join(files_fp, "test_Acc_Thrs40.csv")
inat_17_18_cats_file = os.path.join(files_iNat, "common_cats_inat_17_18_wcs.txt")
inat_17_18_mapping_file = os.path.join(files_iNat, "map_similar_cats_inat_17_18_to_wcs.csv")
inat_21_cats_file = os.path.join(files_iNat, "common_cats_inat_21_wcs.txt")
inat_21_mapping_file = os.path.join(files_iNat, "map_similar_cats_inat_21_to_wcs.csv")
geo_prior_checkpoints_path = os.path.join(files_gp, "ckps_wcs_inat_17_18_21")
test_geo_prior_json = os.path.join(files_gp, "test_wcs_inat_17_18_21.json")

results_classifs = os.path.join(results_path, "classifs_on_test_crops")
result_dataset_path = os.path.join(results_path, f"dataset.csv")
aux_dets_of_megadet_ds_path = os.path.join(results_path, f"aux_dets_of_megadet_ds.csv")
classifs_path = os.path.join(results_classifs, f"ensamble_2.csv")
eval_results_dir = os.path.join(results_path, f"eval_test_part_results")
visualize_results_dir = os.path.join(results_path, "test_images_classifs_bboxes_seq_id")
model_checkpoint_path_efficient = os.path.join(results_path, model_file_name_efficient_b3)
model_checkpoint_path_resnet = os.path.join(results_path, model_file_name_resnet)
model_checkpoint_path_efficient_b7 = os.path.join(results_path, model_file_name_efficient_b7)
labelmap_classif_file = os.path.join(results_path, f"labels.txt")
submission_csv = os.path.join(results_path, f'submission_2020_{num_submission}.csv')

os.makedirs(results_classifs, exist_ok=True)

model_checkpoints = {
    'EfficientNetB7': os.path.join(results_path, model_file_name_efficient_b7),
    'EfficientNetB3': os.path.join(results_path, model_file_name_efficient_b3),
    'ResNet152': os.path.join(results_path, model_file_name_resnet)
}
model_base_instances = {
    'EfficientNetB7': EfficientNetB7,
    'EfficientNetB3': EfficientNetB3,
    'ResNet152': ResNet152
}
model_input_sizes = {
    'EfficientNetB7': EFFIC_B7_SIZES,
    'EfficientNetB3': EFFIC_B3_SIZES,
    'ResNet152': RESNET_SIZES
}
models_names = [
    'EfficientNetB7',
    'EfficientNetB3',
    'ResNet152'
]
model_weights = {
    'EfficientNetB7': .37,
    'EfficientNetB3': .43,
    'ResNet152': .2
}


def main(not_submission, eval, visualize_results, model_name):
    labelmap_classif = read_labelmap_file(labelmap_classif_file)
    dataset = create_combined_dataset(wcs_dataset_json_file=wcs_dataset_json_file,
                                      wcs_images_dir=wcs_train_images_dir,
                                      wcs_dataset_empty_json_file=wcs_dataset_empty_json_file,
                                      inat_17_18_dataset_json_file=dataset_inat_17_18_json_file,
                                      inat_17_18_images_dir=inat_17_18_train_images_dir,
                                      inat_17_18_categories=inat_17_18_cats_file,
                                      inat_17_18_mapping_classes=inat_17_18_mapping_file,
                                      inat_21_dataset_json_file=dataset_inat_21_json_file,
                                      inat_21_images_dir=inat_21_train_images_dir,
                                      inat_21_categories=inat_21_cats_file,
                                      inat_21_mapping_classes=inat_21_mapping_file,
                                      result_crops_dir=train_crops_dir,
                                      train_perc=TRAIN_PERC,
                                      result_dataset_path=result_dataset_path,
                                      IWildcamBaseImageDataset=IWildcam2020ImageDataset)
    if model_name is not None:
        model = train(model_checkpoint_path=model_checkpoints[model_name],
                      dataset=dataset,
                      labelmap_path=labelmap_classif_file,
                      model_type=model_base_instances[model_name],
                      model_name=model_name,
                      images_size=model_input_sizes[model_name],
                      epochs=N_EPOCHS,
                      batch_size=BATCH_SIZE_TRAIN)
        classifs_splt = os.path.splitext(classifs_path)
        csv_path = f"{classifs_splt[0]}-{model_name}{classifs_splt[1]}"
        classify_crops_in_test_set(detections_on_test_set_path=detections_on_test_set_path,
                                   test_images_dir=wcs_test_images_dir,
                                   crops_dir=test_crops_dir,
                                   score_threshold=SCORE_THRES,
                                   model=model,
                                   labelmap_classification=labelmap_classif,
                                   classifs_on_test_crops_path=csv_path,
                                   aux_dets_of_megadet_ds_path=aux_dets_of_megadet_ds_path,
                                   images_size=model_input_sizes[model_name],
                                   batch_size=BATCH_SIZE_EVAL,
                                   labelmap_detection=MEGADET_LABELMAP,
                                   test_ds_json=wcs_test_info_json,
                                   false_positives_dets_csv=false_pos_dets_test_csv,
                                   geo_prior_checkpoints=geo_prior_checkpoints_path,
                                   test_geo_prior_json=test_geo_prior_json,
                                   tta_steps=1,
                                   IWildcamBaseImageDataset=IWildcam2020ImageDataset)
    classifs_on_test_crops_ds = ensemble_classification_models(models_names=models_names,
                                                               classifs_csv=classifs_path,
                                                               images_dir=test_crops_dir,
                                                               method='weighted',
                                                               model_weights=model_weights)
    if not not_submission:
        create_file_for_competition(classifs_on_detection_crops_ds=classifs_on_test_crops_ds,
                                    detections_on_test_set_path=detections_on_test_set_path,
                                    test_images_dir=wcs_test_images_dir,
                                    sample_submission_path=sample_submission_path,
                                    wcs_image_level_anns_json_file=wcs_image_level_anns_json_file,
                                    out_csv_file=submission_csv,
                                    method='sum_classif',
                                    diminishing_factor=0.7)
    if visualize_results:
        visualize_classif_bboxes_in_ds(dataset_json=wcs_test_info_json,
                                       detections_csv=detections_on_test_set_path,
                                       classifs_csv=classifs_path,
                                       images_dir=wcs_test_images_dir,
                                       dest_dir=visualize_results_dir,
                                       submission_file=submission_csv,
                                       cats_file=wcs_image_level_anns_json_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--not_submission", default=False, action="store_true")
    parser.add_argument("--eval", default=False, action="store_true")
    parser.add_argument("--visualize_results", default=False, action="store_true")
    parser.add_argument('--model', default=None, type=str)
    args = parser.parse_args()

    main(not_submission=args.not_submission,
         eval=args.eval,
         visualize_results=args.visualize_results,
         model_name=args.model)
