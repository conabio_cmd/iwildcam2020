#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import json
import pandas as pd
from shutil import copyfile
from datetime import datetime
import uuid
import argparse
from collections import defaultdict
import multiprocessing
from multiprocessing import Manager

from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.utils import str2bool

from conabio_ml_vision.datasets.datasets import ImageDataset, ImagePredictionDataset
from conabio_ml_vision.trainer.model import run_megadetector_inference
from conabio_ml_vision.utils.images_utils import draw_bboxes_in_image

from scripts.utils import *

logger = get_logger(__name__)


GREEN = (0, 255, 0)
MIN_SCORE_TRUE_CLASSIFS = 0.5
NUM_GPUS_PER_NODE = 2


class IWildcam2020ImageDataset(ImageDataset):
    """Definición de un dataset de la colección WCS para la competición iWildcam2020"""

    class IMAGES_FIELDS():
        ID = "id"
        FILENAME = "file_name"
        WIDTH = "width"
        HEIGHT = "height"
        DATE_CAPTURED = "datetime"
        LOCATION = "location"
        SEQ_NUM_FRAMES = "seq_num_frames"
        SEQ_ID = "seq_id"
        SEQ_FRAME_NUM = "frame_num"

        NAMES = [ID, FILENAME, WIDTH, HEIGHT, DATE_CAPTURED, LOCATION, SEQ_NUM_FRAMES,
                 SEQ_ID, SEQ_FRAME_NUM]
        DEFAULTS = {}


IMGS_FLDS = IWildcam2020ImageDataset.IMAGES_FIELDS


def test_create_file_for_competition(classifs_on_detection_crops_csv,
                                     detections_on_test_set_path,
                                     test_images_dir,
                                     sample_submission_path,
                                     wcs_image_level_anns_json_file,
                                     method,
                                     seq_id,
                                     diminishing_factor):
    """Función para depurar la creación del archivo submission para la competición

    Parameters
    ----------
    classifs_on_detection_crops_csv : str
        Ruta del archivo CSV que contiene las clasificaciones sobre el conjunto de prueba
    detections_on_test_set_path : str
        Ruta del archivo CSV con las predicciones hechas por un modelo de detección de objetos
        (p.e. Megadetector) sobre el conjunto de prueba
    test_images_dir : str
        Ruta donde se encuentran las imágenes del conjunto de prueba
    sample_submission_path : str
        Ruta del archivo CSV `sample_submission` de la competición
    wcs_image_level_anns_json_file : str
        Archivo JSON tipo COCO con el campo `categories` del cual se obtiene el labelmap con el id
        y el nombre de cada categoría
    seq_id : str
        Id de una secuencia en particular sobre la que se quiere hacer el procesamiento
    """
    classifs_ds = ImagePredictionDataset.from_csv(source_path=classifs_on_detection_crops_csv)
    create_file_for_competition(classifs_on_detection_crops_ds=classifs_ds,
                                detections_on_test_set_path=detections_on_test_set_path,
                                test_images_dir=test_images_dir,
                                sample_submission_path=sample_submission_path,
                                wcs_image_level_anns_json_file=wcs_image_level_anns_json_file,
                                out_csv_file=None,
                                method=method,
                                seq_id=seq_id,
                                diminishing_factor=diminishing_factor)


def create_file_for_competition(classifs_on_detection_crops_ds,
                                detections_on_test_set_path,
                                test_images_dir,
                                sample_submission_path,
                                wcs_image_level_anns_json_file,
                                out_csv_file,
                                method='mode',
                                seq_id=None,
                                diminishing_factor=1.):
    """Función que a partir del dataset de predicciones `classifs_on_detection_crops_ds` con la 
    clasificación de cada detección sobre el conjunto de prueba, genera un archivo CSV que podrá
    ser enviado a la competición iWildcam2020, cuyo formato está dado en `sample_submission_path`

    Parameters
    ----------
    classifs_on_detection_crops_ds : ImagePredictionDataset
        Dataset que contiene las predicciones del modelo de clasificación sobre cada uno de los
        recuadros encontrados por el Megadetector en el conjunto de prueba.
        Este dataset debe contener las columnas `image_id`, `seq_id`, `seq_frame_num` y
        `id` (el id de la detección)
    detections_on_test_set_path : str
        Ruta del archivo CSV con las predicciones hechas por un modelo de detección de objetos
        (p.e. Megadetector) sobre el conjunto de prueba
    test_images_dir : str
        Ruta donde se encuentran las imágenes del conjunto de prueba
    sample_submission_path : str
        Ruta del archivo CSV `sample_submission` de la competición
    wcs_image_level_anns_json_file : str
        Archivo JSON tipo COCO con el campo `categories` del cual se obtiene el labelmap con el id
        y el nombre de cada categoría
    out_csv_file : str
        Ruta del archivo CSV que se genera para enviar a la competición
    method : str
        Método que por el que se asignarán las etiquetas finales a las imágenes en cada secuencia.
        Puede ser uno de los siguientes:
        - mode: se toma la etiqueta más repetida en la secuencia, y la que tenga score mayor en
        caso de haber más de una, y se asigna a las imágenes de la secuencia.
        - max_classif: se toma la etiqueta con probabilidad de clasificación más alta dentro de la
        secuencia, y se asigna a las imágenes de la secuencia.
        - sum_classif: se realiza la suma de todas las clasificaciones y se toma la que tenga el
        valor más grande
    seq_id : str, optional
        Id de una secuencia en particular sobre la que se quiere hacer el procesamiento,
        by default None
    diminishing_factor : float, optional
        Factor de disminución que se aplicará a todas las clasificaciones para los métodos
        `max_classif` y `sum_classif`
    """
    cats = json.load(open(wcs_image_level_anns_json_file))['categories']
    cats_inverse_dict = {cat['name']: cat['id'] for cat in cats}
    dets_ds = ImagePredictionDataset.from_csv(source_path=detections_on_test_set_path,
                                              images_dir=test_images_dir)
    id_label_dict = Manager().dict()
    classifs_df = classifs_on_detection_crops_ds.as_dataframe()
    dets_df = dets_ds.as_dataframe()
    sample_sub = pd.read_csv(sample_submission_path, header=0)

    dets_classifs_df = filter_non_animal_labels(merge_dets_and_classifs_dfs(dets_df, classifs_df))
    seqs_ids = dets_classifs_df["seq_id"].unique() if seq_id is None else [seq_id]

    logger.info(f"Processing {len(seqs_ids)} sequences with {method}")

    if method == 'mode':
        if seq_id is not None:
            find_classif_mode_in_seq(seq_id, dets_classifs_df, id_label_dict)
        else:
            with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
                pool.starmap(find_classif_mode_in_seq,
                             [(seq_id, dets_classifs_df, id_label_dict) for seq_id in seqs_ids])
    elif method == 'max_classif':
        if seq_id is not None:
            find_max_classif_in_seq(seq_id, dets_classifs_df, id_label_dict, diminishing_factor)
        else:
            with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
                pool.starmap(find_max_classif_in_seq,
                             [(seq_id, dets_classifs_df, id_label_dict, diminishing_factor)
                              for seq_id in seqs_ids])
    elif method == 'sum_classif':
        if seq_id is not None:
            find_sum_classif_in_seq(seq_id, dets_classifs_df, id_label_dict, diminishing_factor)
        else:
            with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
                pool.starmap(find_sum_classif_in_seq,
                             [(seq_id, dets_classifs_df, id_label_dict, diminishing_factor)
                              for seq_id in seqs_ids])
    else:
        raise Exception(f"Invalid method '{method}' to finding the sequence label")

    ds = {
        "Id": [x for x in id_label_dict.keys()],
        "Category": [cats_inverse_dict[label] for label in id_label_dict.values()]
    }
    for id_empty in list(set(sample_sub["Id"].values) - set(ds["Id"])):
        ds["Id"].append(id_empty)
        ds["Category"].append(0)
    df = pd.DataFrame(ds)
    if out_csv_file is not None:
        df.to_csv(out_csv_file, index=False, header=True)
        logger.info(f"Submission file {out_csv_file} created")


def find_classif_mode_in_seq(seq_id, dets_classifs_df, items_dict):
    """Función que genera la etiqueta que se asignará a todas las imágenes de la secuencia `seq_id`
    del conjunto de prueba a partir de la etiqueta más repetida y la que tenga score mayor
    en caso de haber más de una

    Parameters
    ----------
    seq_id : str
        Id de la secuencia
    dets_classifs_df : pd.DataFrame
        DataFrame que contiene las clasificaciones hechas sobre las detecciones del conjunto de
        prueba y sobre el cual se tomarán las imágenes de la secuencia a las que se les asignará la
        etiqueta resultante. Al final se deberá asignar la etiqueta 'empty' a las imágenes que no
        estén presentes en este DataFrame para completar el registro del submission
    items_dict : dict
        Diccionario en el que se agregan los resultados de la forma {`image_id`: `label`}
    """
    dets_classifs_seq = dets_classifs_df[(dets_classifs_df["seq_id"] == seq_id) &
                                         (dets_classifs_df["label_classif"] != "empty")]
    if len(dets_classifs_seq) > 0:
        labels_mode = dets_classifs_seq["label_classif"].mode()
        if len(labels_mode) > 1:
            sorted_dets_classifs_of_seq = dets_classifs_seq.sort_values(by=["score_classif"],
                                                                        axis=0, inplace=False,
                                                                        ascending=False)
            selected_label = \
                sorted_dets_classifs_of_seq[sorted_dets_classifs_of_seq["label_classif"].isin(
                    labels_mode.values)].iloc[0]["label_classif"]
        else:
            selected_label = labels_mode.iloc[0]
        if selected_label in ["misfire", "start", "end"]:  # , "motorcycle"]:
            images_ids = dets_classifs_df[dets_classifs_df["seq_id"]
                                          == seq_id]["image_id"].unique()
        else:
            images_ids = dets_classifs_seq["image_id"].unique()
        for image_id in images_ids:
            items_dict[image_id] = selected_label


def find_max_classif_in_seq(seq_id, dets_classifs_df, items_dict, d):
    """Función que genera la etiqueta que se asignará a todas las imágenes de la secuencia `seq_id`
    del conjunto de prueba a partir de la etiqueta con probabilidad de clasificación más alta
    dentro de la secuencia, aplicando el factor de disminución:
    `d: (detector_confidence + d) / (d + 1) * crop_prediction`

    Parameters
    ----------
    seq_id : str
        Id de la secuencia
    dets_classifs_df : pd.DataFrame
        DataFrame que contiene las clasificaciones hechas sobre las detecciones del conjunto de
        prueba y sobre el cual se tomarán las imágenes de la secuencia a las que se les asignará la
        etiqueta resultante. Al final se deberá asignar la etiqueta 'empty' a las imágenes que no
        estén presentes en este DataFrame para completar el registro del submission
    items_dict : dict
        Diccionario en el que se agregan los resultados de la forma {`image_id`: `label`}
    d : float
        Factor de disminución que se aplicará a todas las clasificaciones
    """
    dets_classifs_seq = dets_classifs_df[(dets_classifs_df["seq_id"] == seq_id) &
                                         (dets_classifs_df["label_classif"] != "empty")]
    if len(dets_classifs_seq) > 0:
        for i, row in dets_classifs_seq.iterrows():
            dets_classifs_seq.loc[i, "score_classif"] = (
                (row["score_det"] + d) / (d + 1) * row["score_classif"])
        sorted_dets_classifs_of_seq = dets_classifs_seq.sort_values(by=["score_classif"],
                                                                    axis=0, inplace=False,
                                                                    ascending=False)
        selected_label = sorted_dets_classifs_of_seq.iloc[0]["label_classif"]
        if selected_label in ["misfire", "start", "end"]:  # , "motorcycle"]:
            images_ids = dets_classifs_df[dets_classifs_df["seq_id"]
                                          == seq_id]["image_id"].unique()
        else:
            images_ids = dets_classifs_seq["image_id"].unique()
        for image_id in images_ids:
            items_dict[image_id] = selected_label


def find_sum_classif_in_seq(seq_id, dets_classifs_df, items_dict, d):
    """Función que genera la etiqueta que se asignará a todas las imágenes de la secuencia `seq_id`
    del conjunto de prueba a partir de hacer la suma de todas las clasificaciones y se toma la que
    tenga el valor más grande, aplicando el factor de disminución:
    `d: (detector_confidence + d) / (d + 1) * crop_prediction`

    Parameters
    ----------
    seq_id : str
        Id de la secuencia
    dets_classifs_df : pd.DataFrame
        DataFrame que contiene las clasificaciones hechas sobre las detecciones del conjunto de
        prueba y sobre el cual se tomarán las imágenes de la secuencia a las que se les asignará la
        etiqueta resultante. Al final se deberá asignar la etiqueta 'empty' a las imágenes que no
        estén presentes en este DataFrame para completar el registro del submission
    items_dict : dict
        Diccionario en el que se agregan los resultados de la forma {`image_id`: `label`}
    d : float
        Factor de disminución que se aplicará a todas las clasificaciones
    """
    dets_classifs_seq = dets_classifs_df[(dets_classifs_df["seq_id"] == seq_id) &
                                         (dets_classifs_df["label_classif"] != "empty")]
    if len(dets_classifs_seq) > 0:
        for i, row in dets_classifs_seq.iterrows():
            dets_classifs_seq.loc[i, "score_classif"] = (
                (row["score_det"] + d) / (d + 1) * row["score_classif"])
        selected_label = dets_classifs_seq.groupby(['label_classif']).sum().sort_values(
            by=["score_classif"], axis=0, inplace=False, ascending=False).index[0]
        if selected_label in ["misfire", "start", "end"]:  # , "motorcycle"]:
            images_ids = dets_classifs_df[dets_classifs_df["seq_id"]
                                          == seq_id]["image_id"].unique()
        else:
            images_ids = dets_classifs_seq["image_id"].unique()
        for image_id in images_ids:
            items_dict[image_id] = selected_label


def fix_sequences_in_set(json_file_2020, json_file_2021, output_json_name, max_secs=3):
    """Función que recibe los archivos `json_file_2020` de un dataset de la competición iWildcam
    2020 y `json_file_2021` de la competición iWildcam2021 y para las imágenes de este
    conjunto genera lo siguiente:
    - Los identificadores de secuencia (`seq_id`)
    - El número de cada frame en la secuencia (`frame_num`)
    - El número total de frames en la secuencia (`seq_num_frames`)

    Parameters
    ----------
    json_file_2020 : str
        Ruta del archivo JSON del dataset de la competición iWildcam2020
    json_file_2021 : str
        Ruta del archivo JSON del dataset de la competición iWildcam2021
    output_json_name : str
        Ruta del archivo JSON resultante
    max_secs : int, optional
        Número máximo de segundos transcurridos entre dos frames consecutivos de una misma
        secuencia, by default 3
    """
    date_format = '%Y-%m-%d %H:%M:%S'

    json_data_2020 = json.load(open(json_file_2020))
    images_dict_2020 = dict()
    for image in json_data_2020['images']:
        images_dict_2020[str(image['id'])] = image

    json_data_2021 = json.load(open(json_file_2021))
    images_dict_2021 = dict()
    for image in json_data_2021['images']:
        images_dict_2021[str(image['id'])] = image

    ds_2020 = IWildcam2020ImageDataset.from_json(source_path=json_file_2020)
    df_2020 = ds_2020.as_dataframe(columns=["image_id", IMGS_FLDS.LOCATION, IMGS_FLDS.SEQ_ID,
                                            IMGS_FLDS.DATE_CAPTURED, IMGS_FLDS.SEQ_FRAME_NUM,
                                            IMGS_FLDS.WIDTH, IMGS_FLDS.HEIGHT],
                                   sort_by=[IMGS_FLDS.LOCATION, IMGS_FLDS.WIDTH, IMGS_FLDS.HEIGHT,
                                            IMGS_FLDS.DATE_CAPTURED],
                                   sort_asc=True)
    common_ids = set(images_dict_2021.keys()) & set(df_2020["image_id"].values)
    for image_id in common_ids:
        images_dict_2020[image_id][IMGS_FLDS.SEQ_ID] = images_dict_2021[image_id]["seq_id"]
        images_dict_2020[image_id][IMGS_FLDS.SEQ_FRAME_NUM] = \
            images_dict_2021[image_id]["seq_frame_num"]
        images_dict_2020[image_id][IMGS_FLDS.SEQ_NUM_FRAMES] = \
            images_dict_2021[image_id]["seq_num_frames"]

    seq_frame_num = 1
    last_location = ""
    last_date = None
    last_width_height_str = ""
    sequence_imgs = defaultdict(list)
    for _, row in df_2020[~df_2020["image_id"].isin(common_ids)].iterrows():
        new_seq = False
        width_height_str = f"{row[IMGS_FLDS.WIDTH]},{row[IMGS_FLDS.HEIGHT]}"
        if row[IMGS_FLDS.LOCATION] == last_location and width_height_str == last_width_height_str:
            d2 = datetime.strptime(row[IMGS_FLDS.DATE_CAPTURED].split('.')[0], date_format)
            d1 = datetime.strptime(last_date, date_format)
            sec_diff = abs((d2 - d1).total_seconds())
            if sec_diff > max_secs:
                new_seq = True
        else:
            new_seq = True
        if new_seq:
            sequence = str(uuid.uuid4())
            seq_frame_num = 1
        else:
            seq_frame_num += 1
        images_dict_2020[row['image_id']][IMGS_FLDS.SEQ_ID] = sequence
        images_dict_2020[row['image_id']][IMGS_FLDS.SEQ_FRAME_NUM] = seq_frame_num
        last_date = row[IMGS_FLDS.DATE_CAPTURED].split('.')[0]
        last_location = row[IMGS_FLDS.LOCATION]
        last_width_height_str = f"{row[IMGS_FLDS.WIDTH]},{row[IMGS_FLDS.HEIGHT]}"
        sequence_imgs[sequence].append(row['image_id'])
    for seq_id, imgs_ids in sequence_imgs.items():
        for img_id in imgs_ids:
            images_dict_2020[img_id][IMGS_FLDS.SEQ_NUM_FRAMES] = len(imgs_ids)

    final_data = {
        "info": json_data_2020.get('info', {}),
        "images": [image for image in images_dict_2020.values()],
        "categories": json_data_2020['categories'],
    }
    if 'annotations' in json_data_2020:
        final_data["annotations"] = json_data_2020['annotations']
    with open(output_json_name, 'w',) as outfile:
        json.dump(final_data, outfile)


def visualize_classif_bboxes_in_ds(dataset_json,
                                   detections_csv,
                                   classifs_csv,
                                   images_dir,
                                   dest_dir,
                                   false_pos_dets_csv=None,
                                   only_detections=False,
                                   split_in_seqs=True,
                                   min_score_true_classifs=MIN_SCORE_TRUE_CLASSIFS,
                                   submission_file=None,
                                   cats_file=None,
                                   seq_id=None):
    """Función que dibuja los recuadros de las detecciones con la etiqueta y el score de su
    clasificación sobre un conjunto de datos, guardando las imágenes agrupadas en carpetas con el
    nombre de la secuencia y con la clasificación y el número de individuos finales encontrados
    como sufijo

    Parameters
    ----------
    dataset_json : str
        Ruta del archivo JSON que contiene el dataset con la información de todas las imágenes del
        conjunto. P.e. `WCS_metadata/iwildcam2021_test_information.json`
    detections_csv : srt
        Ruta del archivo CSV que contiene las detecciones hechas sobre el conjunto representado
        por `dataset_json`. Debe tener la columna `bbox` con las coordenadas de la detección.
        P.e. `files/detections_on_test_set_MegadetV4_CLAHE_WB.csv`
    classifs_csv : str
        Ruta del archivo CSV que contiene las clasificaciones hechas sobre los recuadros formados
        a partir de las detecciones de `detections_csv`. Debe tener las columnas `label` y `score`
        de la clasificación. P.e. `results/classifs_on_test_crops.csv`
    images_dir : str
        Carpeta donde están las imágenes originales del dataset. P.e. `WCS_images/test_CLAHE_WB`
    dest_dir : str
        Ruta de la carpeta raíz donde se guardarán las imágenes resultantes agrupadas por
        secuencia. P.e. `results/test_images_classifs_bboxes_seq_id`
    split_in_seqs : bool, optional
        Bandera que indica si las imágenes serán agrupadas en carpetas que se forman con el campo
        `seq_id` seguido de la clasificación y el número de individuos encontrados ('empty' en caso
        de no haber), by default True
    min_score_true_classifs : float, optional
        Valor de score mínimo para que las clasificaciones sean descartadas como falsas,
        by default MIN_SCORE_TRUE_CLASSIFS
    submission_file : str, optional
        Ruta del archivo CSV submission de la competición que será usado para determinar la
        etiqueta de clasificación y el número de individuos de la secuencia, by default None.
        P.e. `results/submission_x.csv`
    cats_file : str, optional
        Ruta del archivo JSON que contiene la definición de categorías de la competición, y que
        será usado para determinar la etiqueta de clasificación de la secuencia, by default None.
        P.e. `WCS_metadata/iwildcam2021_train_annotations.json`
    seq_id : str, optional
        Id de una secuencia en particular sobre la que se quiere hacer el procesamiento.
        Generalmente usado para depurar, by default None
    """

    orig_ds = IWildcam2020ImageDataset.from_json(source_path=dataset_json, images_dir=images_dir)
    dets_ds = ImagePredictionDataset.from_csv(source_path=detections_csv, images_dir=images_dir)
    classifs_ds = IWildcam2020ImageDataset.from_csv(source_path=classifs_csv, images_dir=images_dir)
    orig_df = orig_ds.as_dataframe(columns=["seq_id", "image_id"])
    dets_df = dets_ds.as_dataframe()
    classifs_df = classifs_ds.as_dataframe()

    cats_ids_to_names = None
    if cats_file is not None:
        cats_ids_to_names = {x["id"]: x["name"] for x in json.load(open(cats_file))["categories"]}
    submission = None
    if submission_file is not None:
        submission = pd.read_csv(submission_file)
    if false_pos_dets_csv is not None and os.path.isfile(false_pos_dets_csv):
        false_pos_dets = pd.read_csv(false_pos_dets_csv, header=0)

    dets_classifs_df = pd.merge(left=dets_df, right=classifs_df, how='left', on='id')
    dets_classifs_df.rename(
        columns={"item_x": "item", "id_x": "id_det", "id_y": "id_classif",
                 "label_x": "label_det", "label_y": "label_classif",
                 "score_x": "score_det", "score_y": "score_classif"}, inplace=True)
    dets_classifs_df = dets_classifs_df[["item", "bbox",  "id_det", "id_classif",
                                         "label_det", "label_classif",
                                         "score_det", "score_classif"]]
    if seq_id is not None:
        draw_classification_bboxes_in_seq(seq_id, orig_df, dets_classifs_df, dest_dir,
                                          split_in_seqs, min_score_true_classifs,
                                          cats_ids_to_names, submission, false_pos_dets)
    else:
        seqs_ids = dets_df["seq_id"].unique() if only_detections else orig_df["seq_id"].unique()
        logger.info(f"Visualizando la información de {len(seqs_ids)} secuencias en {dest_dir}")
        with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
            pool.starmap(draw_classification_bboxes_in_seq,
                         [(seq_id, orig_df, dets_classifs_df, dest_dir, split_in_seqs,
                           min_score_true_classifs, cats_ids_to_names, submission, false_pos_dets)
                          for seq_id in seqs_ids])


def draw_classification_bboxes_in_seq(seq_id,
                                      orig_df,
                                      dets_classifs_df,
                                      dest_dir,
                                      split_in_seqs,
                                      min_score_true_classifs,
                                      cats_ids_to_names,
                                      submission,
                                      false_pos_dets):
    """Función que dibuja los recuadros de las detecciones (con el nombre de la etiqueta de
    clasificación y la probabilidad de clasificación) en las imágenes de una secuencia. Las
    imágenes resultantes las guarda en una carpeta con el nombre del `seq_id` y con el sufijo de la
    clasificación y el conteo de individuos final, y en caso de no tener individuos detectados,
    el `seq_id` seguido de la leyenda 'empty'

    Parameters
    ----------
    seq_id :  str
        Id de la secuencia de imágenes
    orig_df : pd.DataFrame
        DataFrame que contiene todos los registros del conjunto que se va a analizar, incluso
        aquellos que no tengan detecciones o clasificaciones
    dets_classifs_df : pd.DataFrame
        DataFrame que contiene las detecciones encontradas sobre las imágenes de la secuencia,
        con la etiqueta y el score de clasificación en las columnas `label` y `score`
    dest_dir : str
        Ruta de la carpeta raíz donde se guardarán las imágenes resultantes agrupadas por secuencia
    split_in_seqs : bool
        Bandera que indica si las imágenes resultantes serán agrupadas en carpetas según `seq_id`
    min_score_true_classifs : float
        Valor de score mínimo para que las clasificaciones sean descartadas como falsas
    cats_ids_to_names : dict
        Diccionario con el nombre de categoría para cada id
    submission : pd.DataFrame
        DataFrame que contiene el submission de la competición y que será usado junto con
        `cats_ids_to_names` para formar el sufijo del nombre de la carpeta en que se agruparán las
        imágenes de la secuencia
    """
    orig_items_of_seq = orig_df[orig_df["seq_id"] == seq_id]["item"].unique()
    bboxes_info = get_bboxes_info_for_items(orig_items_of_seq, dets_classifs_df,
                                            false_pos_dets, min_score_true_classifs)
    empty_seq = any([clr == GREEN for _, bboxs in bboxes_info.items() for clr in bboxs['colors']])
    items_to_imgs_ids = dict()
    for item in orig_items_of_seq:
        items_to_imgs_ids[item] = orig_df[orig_df["item"] == item].iloc[0]["image_id"]
    imgs_ids = [x for x in items_to_imgs_ids.values()]
    if split_in_seqs:
        dir_suffix = ""
        if cats_ids_to_names is not None and submission is not None:
            if empty_seq:
                dir_suffix = '-empty'
            else:
                subm_cat_ids = submission[submission["Id"].isin(imgs_ids)]["Category"].values
                lbls_sfx = [cats_ids_to_names[cat_id] for cat_id in subm_cat_ids]
                names_sfx = [f'{lbl.replace(" ", "_")}' for lbl in set(lbls_sfx)]
                dir_suffix = f'-{"-".join(names_sfx)}'
        seq_dir = os.path.join(dest_dir, f"{seq_id}{dir_suffix}")
        os.makedirs(seq_dir, exist_ok=True)
    for item in orig_items_of_seq:
        img_id = items_to_imgs_ids[item]
        if img_id not in submission["Id"].values:
            logger.warning(f"Image id {img_id} not in submission ids")
            continue
        cat_id = submission[submission["Id"] == img_id].iloc[0]["Category"]
        cat_lbl = cats_ids_to_names[cat_id]
        if split_in_seqs:
            img_dest_path = os.path.join(seq_dir, f'{img_id}-{cat_lbl.replace(" ", "_")}.jpg')
        else:
            img_dest_path = os.path.join(dest_dir, f'{img_id}-{cat_lbl.replace(" ", "_")}.jpg')
        if item in bboxes_info:
            lbls, scrs = bboxes_info[item]['labels'], bboxes_info[item]['scores']
            lbls_scrs = [f"{lbl}: {int(scr*100)}%" if lbl else ""
                         for lbl, scr in zip(lbls, scrs)]
            draw_bboxes_in_image(item, img_dest_path, bboxes_info[item]['bboxes'],
                                 lbls_scrs, bboxes_info[item]['colors'])
        else:
            copyfile(item, img_dest_path)


def generate_false_pos_dets_in_seqs(detections_csv,
                                    dataset_json,
                                    images_dir,
                                    sub_method,
                                    results_csv_path,
                                    num_tasks=None,
                                    task_num=None):
    """Función para generar las detecciones que son consideras falsos positivos del Megadetector
    a partir de un archivo CSV con las detecciones hechas sobre el conjunto de imágenes.

    Parameters
    ----------
    detections_csv : str
        Archivo CSV que contiene las predicciones hechas por un modelo de detección de objetos 
    dataset_json : str
        Ruta del archivo JSON tipo COCO que se usará para obtener todas las fotos de una secuencia
        y descartar los falsos positivos del Megadetector
    images_dir : str
        Ruta de la carpeta que contiene a las imágenes del dataset
    sub_method : str
        Método que se utilizará para determinar los falsos positivos. Puede ser 'Acc' o 'MOG'
    results_csv_path : str
        Ruta del archivo CSV donde se guardarán los falsos positivos resultantes con las columnas
        `seq_ids` y `det_ids`
    num_tasks : int, optional
        En caso de realizar el procesamiento de forma paralela (p.e. en varios nodos), este es el
        número total de tareas en las que se divide el procesamiento. P.e., si se cuenta con 4
        nodos con 2 GPUs en cada uno, el trabajo se puede dividir en `num_tasks = 8` tareas
        paralelas, para así maximizar el uso de los recursos y realizar el procesamiento en el
        menor tiempo posible
    task_num : int, optional
        En caso de realizar el procesamiento de forma paralela, este es el número de la tarea
        actual. Debe de ser un entero en el rango [1, `task_num`]
    """
    dets_ds = ImagePredictionDataset.from_csv(source_path=detections_csv, images_dir=images_dir)
    get_false_positive_dets_in_seqs(dets_ds,
                                    dataset_json,
                                    images_dir,
                                    sub_method,
                                    results_csv_path,
                                    num_tasks=num_tasks,
                                    task_num=task_num,
                                    IWildcamBaseImageDataset=IWildcam2020ImageDataset)


def run_detector_inference_on_ds(json_file_ds,
                                 out_predictions_csv,
                                 images_dir,
                                 detector_model_path,
                                 num_tasks,
                                 task_num,
                                 categories,
                                 exclude_cats,
                                 min_score_threshold):
    """Función para ejecutar la inferencia de un modelo de detección de objetos sobre las imágenes
    del dataset `json_file_ds` tipo COCO con las columnas por defecto especificadas en
    `IWildcam2020ImageDataset`. Las detecciones se guardan en el archivo CSV de predicción
    `out_predictions_csv`.
    Esta función deberá ejecutarse en un ambiente con TF1 instalado y debidamente configurado

    Parameters
    ----------
    json_file_ds : str
        Ruta del archivo JSON tipo COCO que contiene el dataset
    out_predictions_csv : str
        Ruta del archivo CSV de predicción que se genera
    images_dir : str
        Carpeta que contiene las imágenes del dataset
    detector_model_path : str
        Ruta del archivo que contiene los pesos del modelo de detección de objetos que se utilizará
        en la inferencia
    num_tasks : int
        En caso de realizar el procesamiento de forma paralela (p.e. en varios nodos), este es el
        número total de tareas en las que se divide el procesamiento. P.e., si se cuenta con 4
        nodos con 2 GPUs en cada uno, el trabajo se puede dividir en `num_tasks = 8` tareas
        paralelas, para así maximizar el uso de los recursos y realizar el procesamiento en el
        menor tiempo posible
    task_num : int
        En caso de realizar el procesamiento de forma paralela, este es el número de la tarea
        actual. Debe de ser un entero en el rango [1, `num_tasks`]
    categories : str
        Cadena para filtrar las categorías (separadas por coma) que tendrá el dataset
    exclude_cats : str
        Cadena para filtrar las categorías (separadas por coma) que se quieren quitar del dataset
    min_score_threshold : float
        Score mínimo que deberán tener las detecciones resultantes. El resto se descartarán
    """
    min_score_threshold = min_score_threshold or 0.3
    wcs_ds = IWildcam2020ImageDataset.from_json(source_path=json_file_ds,
                                           images_dir=images_dir,
                                           categories=categories,
                                           exclude_categories=exclude_cats,
                                           exclude_corrupted_images=True)
    run_megadetector_inference(dataset=wcs_ds,
                               out_predictions_csv=out_predictions_csv,
                               images_dir=images_dir,
                               model_path=detector_model_path,
                               min_score_threshold=min_score_threshold,
                               include_id=True,
                               keep_image_id=True,
                               inherit_fields=[IWildcam2020ImageDataset.IMAGES_FIELDS.SEQ_ID,
                                               IWildcam2020ImageDataset.IMAGES_FIELDS.SEQ_FRAME_NUM,
                                               IWildcam2020ImageDataset.IMAGES_FIELDS.SEQ_NUM_FRAMES,
                                               IWildcam2020ImageDataset.IMAGES_FIELDS.LOCATION,
                                               IWildcam2020ImageDataset.IMAGES_FIELDS.DATE_CAPTURED],
                               num_tasks=num_tasks,
                               task_num=task_num,
                               num_gpus_per_node=NUM_GPUS_PER_NODE)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--fix_sequences_in_set", default=False, action="store_true")
    parser.add_argument("--test_create_file_for_competition", default=False, action="store_true")
    parser.add_argument("--generate_false_pos_dets_in_seqs", default=False, action="store_true")
    parser.add_argument("--visualize_classif_bboxes_in_ds", default=False, action="store_true")
    parser.add_argument("--run_detector_inference_on_ds", default=False, action="store_true")
    parser.add_argument("--create_json_for_geo_priors", default=False, action="store_true")
    parser.add_argument("--create_dummy_dets_csv", default=False, action="store_true")

    parser.add_argument('--json_file_2020', default=None, type=str)
    parser.add_argument('--json_file_2021', default=None, type=str)
    parser.add_argument('--output_json_name', default=None, type=str)
    parser.add_argument('--max_secs', default=3, type=int)
    parser.add_argument('--classifs_on_detection_crops_csv', default=None, type=str)
    parser.add_argument('--detections_on_test_set_path', default=None, type=str)
    parser.add_argument('--test_images_dir', default=None, type=str)
    parser.add_argument('--sample_submission_path', default=None, type=str)
    parser.add_argument('--wcs_image_level_anns_json_file', default=None, type=str)
    parser.add_argument('--seq_id', default=None, type=str)
    parser.add_argument('--detections_file', default=None, type=str)
    parser.add_argument('--json_filename', default=None, type=str)
    parser.add_argument('--images_dir', default=None, type=str)
    parser.add_argument('--sub_method', default='Acc', type=str)
    parser.add_argument('--results_csv_path', default=None, type=str)
    parser.add_argument('--num_tasks', default=None, type=int)
    parser.add_argument('--task_num', default=None, type=int)
    parser.add_argument('--classification_csv_file', default=None, type=str)
    parser.add_argument('--images_dir_dest', default=None, type=str)
    parser.add_argument('--method', default='mode', type=str)
    parser.add_argument('--submission_file', default=None, type=str)
    parser.add_argument('--cats_file', default=None, type=str)
    parser.add_argument('--diminishing_factor', default=1., type=float)
    parser.add_argument('--dataset_json_file', default=None, type=str)
    parser.add_argument('--detector_model_path', default=None, type=str, required=False)
    parser.add_argument("--categories", default=None, type=str)
    parser.add_argument("--exclude_cats", default=None, type=str)
    parser.add_argument('--min_score_thresh', default=None, type=float)
    parser.add_argument('--wcs_ds_path', default=None, type=str)
    parser.add_argument('--wcs_location_json', default=None, type=str)
    parser.add_argument('--inat_17_18_images_json', default=None, type=str)
    parser.add_argument('--inat_17_18_categories', default=None, type=str)
    parser.add_argument('--inat_17_18_mapping_classes', default=None, type=str)
    parser.add_argument('--inat_17_images_json', default=None, type=str)
    parser.add_argument('--inat_18_images_json', default=None, type=str)
    parser.add_argument('--inat_17_location_json', default=None, type=str)
    parser.add_argument('--inat_18_location_json', default=None, type=str)
    parser.add_argument('--inat_21_dataset_json_file', default=None, type=str)
    parser.add_argument('--inat_21_categories', default=None, type=str)
    parser.add_argument('--inat_21_mapping_classes', default=None, type=str)
    parser.add_argument('--inat_21_images_json', default=None, type=str)
    parser.add_argument('--out_images_json', default=None, type=str)
    parser.add_argument('--labelmap_path', default=None, type=str)
    parser.add_argument("--add_label", type=str2bool, nargs='?', const=True, default=True)

    args = parser.parse_args()

    if args.fix_sequences_in_set:
        fix_sequences_in_set(json_file_2020=args.json_file_2020,
                             json_file_2021=args.json_file_2021,
                             output_json_name=args.output_json_name,
                             max_secs=args.max_secs)
    if args.test_create_file_for_competition:
        test_create_file_for_competition(
            classifs_on_detection_crops_csv=args.classifs_on_detection_crops_csv,
            detections_on_test_set_path=args.detections_on_test_set_path,
            test_images_dir=args.test_images_dir,
            sample_submission_path=args.sample_submission_path,
            wcs_image_level_anns_json_file=args.wcs_image_level_anns_json_file,
            method=args.method,
            seq_id=args.seq_id,
            diminishing_factor=args.diminishing_factor)
    if args.generate_false_pos_dets_in_seqs:
        generate_false_pos_dets_in_seqs(detections_csv=args.detections_file,
                                        dataset_json=args.json_filename,
                                        images_dir=args.images_dir,
                                        sub_method=args.sub_method,
                                        results_csv_path=args.results_csv_path,
                                        num_tasks=args.num_tasks,
                                        task_num=args.task_num)
    if args.visualize_classif_bboxes_in_ds:
        visualize_classif_bboxes_in_ds(dataset_json=args.json_filename,
                                       detections_csv=args.detections_file,
                                       classifs_csv=args.classification_csv_file,
                                       images_dir=args.images_dir,
                                       dest_dir=args.images_dir_dest,
                                       submission_file=args.submission_file,
                                       cats_file=args.cats_file,
                                       seq_id=args.seq_id)
    if args.run_detector_inference_on_ds:
        run_detector_inference_on_ds(json_file_ds=args.dataset_json_file,
                                     out_predictions_csv=args.results_csv_path,
                                     images_dir=args.images_dir,
                                     detector_model_path=args.detector_model_path,
                                     num_tasks=args.num_tasks,
                                     task_num=args.task_num,
                                     categories=args.categories,
                                     exclude_cats=args.exclude_cats,
                                     min_score_threshold=args.min_score_thresh)
    if args.create_dummy_dets_csv:
        create_dummy_detections_csv(json_file_ds=args.dataset_json_file,
                                    out_predictions_csv=args.results_csv_path,
                                    images_dir=args.images_dir,
                                    IWildcamBaseImageDataset=IWildcam2020ImageDataset)
    if args.create_json_for_geo_priors:
        create_json_for_geo_priors(wcs_ds_path=args.wcs_ds_path,
                                   wcs_location_json=args.wcs_location_json,
                                   inat_17_18_images_json=args.inat_17_18_images_json,
                                   inat_17_18_categories=args.inat_17_18_categories,
                                   inat_17_18_mapping_classes=args.inat_17_18_mapping_classes,
                                   inat_17_images_json=args.inat_17_images_json,
                                   inat_18_images_json=args.inat_18_images_json,
                                   inat_17_location_json=args.inat_17_location_json,
                                   inat_18_location_json=args.inat_18_location_json,
                                   inat_21_dataset_json_file=args.inat_21_dataset_json_file,
                                   inat_21_categories=args.inat_21_categories,
                                   inat_21_mapping_classes=args.inat_21_mapping_classes,
                                   inat_21_images_json=args.inat_21_images_json,
                                   out_images_json=args.out_images_json,
                                   labelmap_path=args.labelmap_path,
                                   add_label=args.add_label,
                                   exclude_cats=args.exclude_cats,
                                   IWildcamBaseImageDataset=IWildcam2020ImageDataset)
