#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse

import tensorflow as tf
from tensorflow.keras.applications import EfficientNetB3
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers.experimental import preprocessing
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
from keras.layers import Dense, Input, GlobalAveragePooling2D, BatchNormalization, Dropout
from keras.models import Model

from conabio_ml.utils.dataset_utils import write_labelmap_file, read_labelmap_file
from conabio_ml.datasets.dataset import Partitions
from conabio_ml_vision.datasets.datasets import ConabioImageDataset, ImagePredictionDataset
from conabio_ml_vision.evaluator.evaluator import ImageClassificationEvaluator
from conabio_ml_vision.evaluator.evaluator import ImageClassificationMetrics
from conabio_ml.utils.logger import get_logger

from scripts.utils import *
from iWildcam2020.src.utils_2020 import *

logger = get_logger(__name__)

"""En este script se utilizan los recuadros de las detecciones del Megadetector V4 sobre las
imágenes del conjunto WCS para entrenar un modelo de clasificación EfficientNetB3 (sólo la última
parte de clasificación).
Sobre el conjunto de prueba se ejecuta la inferencia del Megadetector V4 para obtener los
recuadros de cada imagen y se ejecuta la inferencia del modelo entrenado para clasificar cada
recuadro con las categorías requeridas por la competición.
Después, con las clasificaciones para cada recuadro del conjunto de prueba se aplica la siguiente
regla para calcular el número de individuos de cada categoría en cada secuencia:
Para cada secuencia se calcula una etiqueta que será la más frecuente (moda) y con mayor score (en
caso de haber más de una), y esta etiqueta será la que se asignará a todos los bboxes de la
secuencia. El conteo de individuos se realiza tomando el número de recuadros mayor en una imagen de
la secuencia.
"""


def build_model(num_classes):
    img_augmentation = Sequential(
        [
            preprocessing.RandomRotation(factor=0.15),
            preprocessing.RandomTranslation(height_factor=0.1, width_factor=0.1),
            preprocessing.RandomFlip(),
            preprocessing.RandomContrast(factor=0.1),
        ],
        name="img_augmentation",
    )

    inputs = Input(shape=(IMG_WIDTH, IMG_HEIGHT, 3))
    x = img_augmentation(inputs)
    model = EfficientNetB3(include_top=False, input_tensor=x, weights="imagenet")

    # Freeze the pretrained weights
    model.trainable = False

    # Rebuild top
    x = GlobalAveragePooling2D(name="avg_pool")(model.output)
    x = BatchNormalization()(x)

    top_dropout_rate = 0.2
    x = Dropout(top_dropout_rate, name="top_dropout")(x)
    outputs = Dense(num_classes, activation="softmax", name="pred")(x)

    # Compile
    model = Model(inputs, outputs, name="EfficientNet")
    optimizer = Adam(learning_rate=1e-2)
    model.compile(optimizer=optimizer, loss="categorical_crossentropy", metrics=["accuracy"])
    return model


def train_model(model_checkpoint_path,
                dataset,
                labelmap_path):
    target_size = (IMG_WIDTH, IMG_HEIGHT)
    if not os.path.isfile(model_checkpoint_path):
        df_train = dataset.get_splitted(partitions=Partitions.TRAIN)
        n_cats = dataset.get_num_categories()
        classes = dataset.get_classes()
        train_batches = ImageDataGenerator().flow_from_dataframe(df_train,
                                                                 x_col="item",
                                                                 y_col="label",
                                                                 classes=classes,
                                                                 target_size=target_size,
                                                                 batch_size=BATCH_SIZE_TRAIN)
        labelmap = {v: k for k, v in train_batches.class_indices.items()}
        write_labelmap_file(labelmap=labelmap, dest_path=labelmap_path)
        with tf.distribute.MirroredStrategy().scope():
            model = build_model(num_classes=n_cats)
        history = model.fit(train_batches, epochs=N_EPOCHS, verbose=1,
                            callbacks=[ModelCheckpoint(model_checkpoint_path)])
    else:
        with tf.distribute.MirroredStrategy().scope():
            model = load_model(model_checkpoint_path)

    return model


def eval_model(model,
               dataset,
               labelmap_path,
               results_dir):
    logger.info("Evaluating model...")
    target_size = (IMG_WIDTH, IMG_HEIGHT)
    labelmap = read_labelmap_file(labelmap_path)
    preds_on_test_part_csv = os.path.join(results_dir, "predictions.csv")
    os.makedirs(results_dir, exist_ok=True)
    if os.path.isfile(preds_on_test_part_csv):
        preds_on_test_part_ds = \
            ImagePredictionDataset.from_csv(source_path=preds_on_test_part_csv,
                                            images_dir=dataset.get_images_dir(),
                                            labelmap=labelmap)
    else:
        preds_on_test_part_ds = \
            predict_on_ds(model=model, dataset=dataset, labelmap=labelmap,
                          images_size=target_size, batch_size=BATCH_SIZE_EVAL,
                          partition=Partitions.TEST)
        preds_on_test_part_ds.to_csv(dest_path=preds_on_test_part_csv)
    res_eval = ImageClassificationEvaluator.eval(
        dataset_true=dataset,
        dataset_pred=preds_on_test_part_ds,
        eval_config={
            "dataset_partition": Partitions.TEST,
            "metrics_set": {
                ImageClassificationMetrics.Sets.MULTICLASS: {
                    "average": "micro"
                }
            }
        })
    res_eval.result_plots(dest_path=results_dir, report=True)
    res_eval.store_eval_metrics(dest_path=os.path.join(results_dir,
                                                       "eval_results.json"))


experiment = "efficientnet_b3_late_2"
num_submission = 27
add_sub_suffix = False

model_file_name = 'efficientnet_b3_train_1.model.hdf5'

TRAIN_PERC = 0.8
BATCH_SIZE_TRAIN = 32
BATCH_SIZE_EVAL = 64
N_EPOCHS = 20
SCORE_THRES = 0.3
IMG_WIDTH, IMG_HEIGHT = 224, 224
MEGADET_LABELMAP = ConabioImageDataset.MEGADETECTOR_V4_LABELMAP

repo_base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020"
base_path = os.path.join(repo_base_path, "iWildcam2020")
base_2021_path = os.path.join(repo_base_path, "iWildcam2021")
files_path = os.path.join(base_path, "files")
data_path = os.path.join(base_path, "data")
data_2021_path = os.path.join(base_2021_path, "data")
results_path = os.path.join(base_path, "results", experiment)

wcs_image_level_anns_json_file = os.path.join(
    data_path, "WCS_metadata", "iwildcam2020_train_annotations.json")
wcs_test_info_json = os.path.join(data_path, "WCS_metadata", "iwildcam2020_test_information.json")
sample_submission_path = os.path.join(data_path, "WCS_metadata", "sample_submission.csv")
wcs_test_images_dir = os.path.join(data_path, "WCS_images", "test_CLAHE_WB")
wcs_train_images_dir = os.path.join(data_path, "WCS_images", "train_CLAHE_WB")
test_crops_dir = os.path.join(data_path, "WCS_images", "crops_of_test_set_MegadetV4")
train_crops_dir = os.path.join(data_path, "WCS_images", "crops_of_train_set_MegadetV4")
inat_17_18_train_images_dir = os.path.join(
    data_2021_path, "iNat_images", "inaturalist_17_18_CLAHE_WB")
inat_21_train_images_dir = os.path.join(data_2021_path, "iNat_images", "inaturalist_2021_CLAHE_WB")

wcs_dataset_json_file = os.path.join(files_path, "dataset_WCS_from_dets_MegadetV4.json")
wcs_dataset_empty_json_file = os.path.join(
    files_path, "dataset_WCS_from_dets_empty_MegadetV4.json")
dataset_inat_21_json_file = os.path.join(files_path, "dataset_iNat_21_from_dets.json")
dataset_inat_17_18_json_file = os.path.join(files_path, "dataset_iNat_17_18_curated.json")
detections_on_test_set_path = os.path.join(files_path, "detections_on_test_set_MegadetV4.csv")
false_pos_dets_test_csv = os.path.join(files_path, "false_pos_dets_test_Acc_Thrs40.csv")
inat_17_18_cats_file = os.path.join(files_path, "common_cats_inat_17_18_wcs.txt")
inat_17_18_mapping_file = os.path.join(files_path, "map_similar_cats_inat_17_18_to_wcs.csv")
inat_21_cats_file = os.path.join(files_path, "common_cats_inat_21_wcs.txt")
inat_21_mapping_file = os.path.join(files_path, "map_similar_cats_inat_21_to_wcs.csv")

result_dataset_path = os.path.join(results_path, f"dataset.csv")
aux_dets_of_megadet_ds_path = os.path.join(results_path, f"aux_dets_of_megadet_ds.csv")
classifs_on_crops_path = os.path.join(results_path, f"classifs_on_test_crops.csv")
eval_results_dir = os.path.join(results_path, f"eval_test_part_results")
visualize_results_dir = os.path.join(results_path, "test_images_classifs_bboxes_seq_id")
model_checkpoint_path = os.path.join(results_path, model_file_name)
labelmap_classif_file = os.path.join(results_path, f"labels.txt")
submission_csv = os.path.join(results_path, f'submission_2020_{num_submission}.csv')

os.makedirs(results_path, exist_ok=True)


def main(not_submission, eval, visualize_results):
    labelmap_classif = read_labelmap_file(labelmap_classif_file)
    dataset = create_combined_dataset(wcs_dataset_json_file=wcs_dataset_json_file,
                                      wcs_images_dir=wcs_train_images_dir,
                                      wcs_dataset_empty_json_file=wcs_dataset_empty_json_file,
                                      inat_17_18_dataset_json_file=dataset_inat_17_18_json_file,
                                      inat_17_18_images_dir=inat_17_18_train_images_dir,
                                      inat_17_18_categories=inat_17_18_cats_file,
                                      inat_17_18_mapping_classes=inat_17_18_mapping_file,
                                      inat_21_dataset_json_file=dataset_inat_21_json_file,
                                      inat_21_images_dir=inat_21_train_images_dir,
                                      inat_21_categories=inat_21_cats_file,
                                      inat_21_mapping_classes=inat_21_mapping_file,
                                      result_crops_dir=train_crops_dir,
                                      train_perc=TRAIN_PERC,
                                      result_dataset_path=result_dataset_path,
                                      IWildcamBaseImageDataset=IWildcam2020ImageDataset)
    model = train_model(model_checkpoint_path=model_checkpoint_path,
                        dataset=dataset,
                        labelmap_path=labelmap_classif_file)
    if not not_submission:
        classifs_on_test_crops_ds = \
            classify_crops_in_test_set(detections_on_test_set_path=detections_on_test_set_path,
                                       test_images_dir=wcs_test_images_dir,
                                       crops_dir=test_crops_dir,
                                       score_threshold=SCORE_THRES,
                                       model=model,
                                       labelmap_classification=labelmap_classif,
                                       classifs_on_test_crops_path=classifs_on_crops_path,
                                       aux_dets_of_megadet_ds_path=aux_dets_of_megadet_ds_path,
                                       images_size=(IMG_WIDTH, IMG_HEIGHT),
                                       batch_size=BATCH_SIZE_EVAL,
                                       labelmap_detection=MEGADET_LABELMAP,
                                       test_ds_json=wcs_test_info_json,
                                       false_positives_dets_csv=false_pos_dets_test_csv,
                                       IWildcamBaseImageDataset=IWildcam2020ImageDataset)
        create_file_for_competition(classifs_on_detection_crops_ds=classifs_on_test_crops_ds,
                                    detections_on_test_set_path=detections_on_test_set_path,
                                    test_images_dir=wcs_test_images_dir,
                                    sample_submission_path=sample_submission_path,
                                    wcs_image_level_anns_json_file=wcs_image_level_anns_json_file,
                                    out_csv_file=submission_csv,
                                    method='sum_classif',
                                    diminishing_factor=0.7)
    if eval:
        eval_model(model=model,
                   dataset=dataset,
                   labelmap_path=labelmap_classif_file,
                   results_dir=eval_results_dir)
    if visualize_results:
        visualize_classif_bboxes_in_ds(dataset_json=wcs_test_info_json,
                                       detections_csv=detections_on_test_set_path,
                                       classifs_csv=classifs_on_crops_path,
                                       images_dir=wcs_test_images_dir,
                                       dest_dir=visualize_results_dir,
                                       submission_file=submission_csv,
                                       cats_file=wcs_image_level_anns_json_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--not_submission", default=False, action="store_true")
    parser.add_argument("--eval", default=False, action="store_true")
    parser.add_argument("--visualize_results", default=False, action="store_true")
    args = parser.parse_args()

    main(args.not_submission, args.eval, args.visualize_results)
