import os
import sys
import argparse
from conabio_ml.datasets.dataset import Partitions
from conabio_ml.datasets.images.datasets import COCOImageDataset, \
    ConabioImageDataset, ImageDataset
from conabio_ml.trainer.images.model import ClassificationModel, ObjectDetectionModel
from conabio_ml.datasets.images import ImagePredictionDataset
from conabio_ml.trainer.images.predictor_config import TFPredictorConfig
import conabio_ml.utils.dataset_utils as dataset_utils
from conabio_ml.utils.logger import get_logger

from utils import create_multiclass_dataset_from_classifs_on_crops_and_test_ds
"""
    python predict_classification_model.py --num_tasks_dets 6 --task_num_dets 1
    python predict_classification_model.py --num_tasks_classif_crops 6 --task_num_classif_crops 1 
        --splitted_predictions_detections
    python predict_classification_model.py --splitted_predictions_classif_on_crops
"""

logger = get_logger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument("--splitted_predictions_detections", default=False, action="store_true")
parser.add_argument("--splitted_predictions_classif_on_crops", default=False, action="store_true")
parser.add_argument('--num_tasks_dets', default=None, type=int)
parser.add_argument('--task_num_dets', default=None, type=int)
parser.add_argument('--num_tasks_classif_crops', default=None, type=int)
parser.add_argument('--task_num_classif_crops', default=None, type=int)
parser.add_argument("--force_classification", default=False, action="store_true")
args = parser.parse_args()

results_path = os.path.join("results/predict_megadetector/all_WCS_imgs")
dataset_path = os.path.join(results_path, 'dataset.csv')
final_csv = os.path.join(results_path, 'submission_1.csv')
pred_dataset_dets_path = os.path.join(results_path, f"pred_dataset_dets.csv")
pred_dataset_classif_on_crops_path = os.path.join(results_path, f"pred_dataset_classif_on_crops.csv")
dataset_classif_multiclass_path = os.path.join(results_path, f"dataset_classif_multiclass.csv")
splitted_predictions_classif_on_crops_path = os.path.join(results_path, f"splitted_classif_on_crops")
dataset_crops_path = os.path.join(results_path, f"dataset_crops.csv")
splitted_predictions_path = \
    os.path.join("results/predict_megadetector/all_WCS_imgs", f"splitted_predictions_dataset")
train_dir = os.path.join(
    f"/LUSTRE/users/ecoinf_admin/conabio_ml/examples/scripts/images/detection/results/"
    f"eval_megadetector_coco/WCS_iwild2020/experiments/crops/allimgs/", 'train')
crops_path = os.path.join(results_path, f"crops")
train_path = "results/train_classification_model/all_WCS_imgs/"
tfrecords_path = os.path.join(train_path, "tfrecords")
dataset_clas_labelmap_file = os.path.join(tfrecords_path, "labels.txt")
base_path = "/LUSTRE/users/ecoinf_admin/conabio_ml/examples/scripts/images/detection"
data_path = os.path.join("/LUSTRE/users/ecoinf_admin/iwildcam2020", "data")
json_path = os.path.join(data_path, "metadata", "iwildcam2020_test_information.json")
anns_json_path = os.path.join(data_path, "metadata", "iwildcam2020_train_annotations.json")
WCS_test_json_file = os.path.join(data_path, "metadata", "iwildcam2020_test_information.json")
sample_submission_path = os.path.join(data_path, "metadata", "sample_submission.csv")
images_dir = os.path.join(data_path, "test")
model_path = os.path.join(base_path, "files", "megadetector_v3.pb")
score_threshold = 0.3

os.makedirs(results_path, exist_ok=True)
os.makedirs(crops_path, exist_ok=True)

# Creación del dataset pred_dataset_dets, con las detecciones del Megadetector
predictor_labelmap = {
    1: ConabioImageDataset.ANIMAL_LABEL, 
    2: ConabioImageDataset.PERSON_LABEL
}
if os.path.isfile(pred_dataset_dets_path):
    pred_dataset_dets = \
        ImagePredictionDataset.from_csv(source_path=pred_dataset_dets_path,
                                        labelmap=predictor_labelmap,
                                        columns=["item", "label", "bbox", "score", "id"])
elif args.splitted_predictions_detections:
    pred_dataset_dets = \
        ImagePredictionDataset.from_csv(source_path=splitted_predictions_path,
                                        labelmap=predictor_labelmap,
                                        include_id=True)
    pred_dataset_dets.to_csv(dest_path=pred_dataset_dets_path, 
                             columns=["item", "label", "bbox", "score", "id"])
else:
    execute_only_once = False
    if args.num_tasks_dets is not None and args.task_num_dets is not None:
        os.makedirs(splitted_predictions_path, exist_ok=True)
        prediction_dataset_path = os.path.join(
            splitted_predictions_path,
            f'prediction_dataset_{args.task_num_dets}_of_{args.num_tasks_dets}.csv')
        execute_only_once = True
    else:
        prediction_dataset_path = pred_dataset_dets_path

    if not os.path.isfile(dataset_path):
        wcs_test = ConabioImageDataset.from_json(source_path=json_path,
                                                images_dir=images_dir,
                                                labelmap=predictor_labelmap)
        wcs_test.to_csv(dest_path=dataset_path)
    else:
        wcs_test = ConabioImageDataset.from_csv(source_path=dataset_path,
                                                labelmap=predictor_labelmap)
    megadetector_model = ObjectDetectionModel.load_saved_model(source_path=model_path)
    exec_config = TFPredictorConfig.create(batch_size=1,
                                           num_tasks=args.num_tasks_dets,
                                           task_num=args.task_num_dets)
    predict_dataset = megadetector_model.predict(dataset=wcs_test,
                                                execution_config=exec_config,
                                                prediction_config={
                                                    "max_number_of_boxes": 10,
                                                    "dataset_partition": None # Para el dataset completo
                                                })
    predict_dataset.to_csv(dest_path=prediction_dataset_path)
    if execute_only_once:
        logger.info(
            f"Exit program after create {args.task_num_dets} of {args.num_tasks_dets} splits.")
        sys.exit()


# Generación del dataset dataset_crops, generado con los recortes de las 
# detecciones en pred_dataset_dets
if not os.path.isfile(dataset_crops_path):
    dataset_crops = \
        pred_dataset_dets.create_classif_ds_from_det_crops(
            dest_path=crops_path, 
            score_threshold=score_threshold, 
            only_highest_score=True,
            include_id=True,
            labelmap=predictor_labelmap)
    dataset_crops.to_csv(dest_path=dataset_crops_path,
                        columns=["item", "label", "id"])
else:
    dataset_crops = \
        ImagePredictionDataset.from_csv(source_path=dataset_crops_path, 
                                        labelmap=predictor_labelmap)



# Generación del dataset pred_dataset_classif_on_crops, con las clasificaciones sobre 
# el dataset de recortes dataset_crops
execute_only_once = False
if os.path.isfile(pred_dataset_classif_on_crops_path) and not args.force_classification:
    pred_dataset_classif_on_crops = \
        ImagePredictionDataset.from_csv(source_path=pred_dataset_classif_on_crops_path,
                                        labelmap=dataset_clas_labelmap_file)
elif args.splitted_predictions_classif_on_crops and not args.force_classification:
    pred_dataset_classif_on_crops = \
        ImagePredictionDataset.from_csv(source_path=splitted_predictions_classif_on_crops_path,
                                        labelmap=dataset_clas_labelmap_file)
    pred_dataset_classif_on_crops.to_csv(dest_path=pred_dataset_classif_on_crops_path)
else:
    if args.task_num_classif_crops is not None and args.num_tasks_classif_crops is not None:
        os.makedirs(splitted_predictions_classif_on_crops_path, exist_ok=True)
        pred_dataset_classif_on_crops_path = os.path.join(
            splitted_predictions_classif_on_crops_path,
            f'prediction_classif_dataset_{args.task_num_classif_crops}_of_'
            f'{args.num_tasks_classif_crops}.csv')
        execute_only_once = True
    tf_predictor_config = \
        TFPredictorConfig.create(batch_size=32, 
                                 num_preprocessing_threads=8,
                                 num_tasks=args.num_tasks_classif_crops,
                                 task_num=args.task_num_classif_crops)
    inception_v4 = ClassificationModel.load_saved_model(
        source_path=train_dir,
        model_config={
            "inception_v4": {
                "weight_decay": 0.00004
            }
        })
    pred_dataset_classif_on_crops = \
        inception_v4.predict(dataset=dataset_crops,
                             execution_config=tf_predictor_config,
                             prediction_config={
                                "dataset_partition": None,
                                "keep_detection_id": True,
                                "labelmap": dataset_clas_labelmap_file
                             })
    pred_dataset_classif_on_crops.to_csv(dest_path=pred_dataset_classif_on_crops_path)
    if execute_only_once:
        logger.info(
            f"Exit program after create {args.task_num_classif_crops} of "
            f"{args.num_tasks_classif_crops} splits.")
        sys.exit()


# Generación del dataset de clasificación multiclase dataset_classif_multiclass
# que tiene sola una etiqueta de especie a nivel de imagen
if not os.path.isfile(dataset_classif_multiclass_path):
    wcs_test = ConabioImageDataset.from_json(
        source_path=WCS_test_json_file,
        images_dir=images_dir,
        exclude_corrupted_images=True)
    classif_multiclass_dataset = \
        create_multiclass_dataset_from_classifs_on_crops_and_test_ds(
            dataset=pred_dataset_classif_on_crops,
            test_dataset=wcs_test,
            pred_dataset_dets=pred_dataset_dets
        )
    classif_multiclass_dataset.to_csv(dest_path=dataset_classif_multiclass_path)
else:
    classif_multiclass_dataset = \
        ImageDataset.from_csv(source_path=dataset_classif_multiclass_path)

# Generate the final CSV file to send to the Kaggle competition
dataset_utils.create_file_to_kaggle_competition(
    classif_multiclass_dataset,
    coco_json_file=anns_json_path,
    dest_path=final_csv,
    sample_submission=sample_submission_path
)
