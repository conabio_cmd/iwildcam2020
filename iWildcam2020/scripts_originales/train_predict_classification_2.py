#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Train a classification model (Inception v4) with all categories of the WCS and 
including iNaturalit 2017 and 2018 images to the dataset
"""
import os
import sys
import json
from glob import glob
import uuid
import argparse
import pandas as pd
from datetime import datetime
from collections import defaultdict
from conabio_ml.utils.report_params import languages
from conabio_ml.datasets.images.datasets import ConabioImageDataset, ImagePredictionDataset, ImageDataset
from conabio_ml.trainer.images.trainer_config import TFSlimTrainConfig
from conabio_ml.trainer.images.predictor_config import TFPredictorConfig
from conabio_ml.trainer.images.model import ClassificationModel
from conabio_ml.evaluator.images.evaluator import ImageClassificationEvaluator,\
    ImageClassificationMetrics
from conabio_ml.trainer.images.trainer import ClassificationTrainer
from conabio_ml.datasets.dataset import Partitions
from conabio_ml.utils.evaluator_utils import store_eval_results, load_eval_results, \
    store_classification_results
import conabio_ml.utils.dataset_utils as dataset_utils
from utils import create_WCS_dataset_from_given_detections, \
    create_WCS_dataset_from_Megadetector_v4_detections, create_iNat_dataset_from_detections, \
    get_predictions_on_test_set
from utils import create_multiclass_dataset_from_classifs_on_crops_and_test_ds

from conabio_ml.pipeline import Pipeline
from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)

CLONE_ON_CPU = False
NUM_GPUS_PER_NODE = 2

def create_dataset_from_detection_crops(WCS_dataset_json_file,  # dataset_all_WCS_dets.json
                                         iNat_dataset_json_file,  # inaturalist_to_iwildcam_train.json
                                         WCS_images_dir,  # iwildcam2020/data/train
                                         iNat_images_dir,  # iwildcam2020/data/
                                         train_perc,
                                         result_dataset_path,
                                         tfrecords_path,
                                         result_crops_path,
                                         pipeline):  # ..all_WCS_and_iNat_imgs_2/crops_of_train_dataset
    process_name = "create_dataset"
    dataset_columns = ["item", "label"]
    if os.path.isfile(result_dataset_path):
        pipeline.add_process(
            name=process_name,
            action=ConabioImageDataset.from_csv,
            inputs_from_processes=[],
            reportable=True,
            args={
                "source_path": result_dataset_path,
                "split_by_column": "partition",
                "columns": dataset_columns,
                "tfrecords_path": tfrecords_path,
                "exclude_corrupted_images": True,
                "info": {
                    "version": "detection-bboxes", 
                    "description": "Dataset of iWildCam 2020 competition with image level annotations.", 
                    "year": 2020, 
                    "collection": "WCS, iNat 2017 and iNat 2018", 
                    "url": "https://github.com/visipedia/iwildcam_comp"
                }
            })
        return process_name
    
    pipeline.add_process(
        name="inat_detection_dataset",
        action=ConabioImageDataset.from_json,
        args={
            "source_path": iNat_dataset_json_file,
            "images_dir": iNat_images_dir,
            "exclude_corrupted_images": True
        })
    pipeline.add_process(
        name='inat_classification_dataset',
        action=ImageDataset.create_classif_ds_from_bboxes_crops,
        inputs_from_processes=['inat_detection_dataset'],
        args={
            "dest_path": result_crops_path
        })
    pipeline.add_process(
        name='split_inat_classification_dataset',
        action=ImageDataset.split,
        inputs_from_processes=['inat_classification_dataset'],
        args={
            "train_perc": train_perc,
            "val_perc": 0,
            "test_perc": 1.-train_perc
        })
    
    pipeline.add_process(
        name='wcs_detection_dataset',
        action=ConabioImageDataset.from_json, 
        args={
            "source_path": WCS_dataset_json_file,
            "images_dir": WCS_images_dir,
            "exclude_corrupted_images": True
        })
    pipeline.add_process(
        name='wcs_classification_dataset',
        action=ImageDataset.create_classif_ds_from_bboxes_crops,
        inputs_from_processes=['wcs_detection_dataset'],
        args={
            "dest_path": result_crops_path,
            "inherit_fields": ["location"]
        })
    pipeline.add_process(
        name='split_wcs_classification_dataset',
        action=ImageDataset.split,
        inputs_from_processes=['wcs_classification_dataset'],
        args={
            "train_perc": train_perc, 
            "val_perc": 0, 
            "test_perc": 1.-train_perc, 
            "group_by_field": "location"
        })

    pipeline.add_process(
        name='classification_dataset',
        action=ConabioImageDataset.from_datasets,
        inputs_from_processes=['wcs_classification_dataset', 'inat_classification_dataset']
    )

    pipeline.add_process(
        name='to_tfrecords_classification_dataset',
        action=ImageDataset.to_tfrecords,
        inputs_from_processes=['classification_dataset'],
        args={
            "dest_path": tfrecords_path, 
            "num_shards": 5
        })
    pipeline.add_process(
        name='to_csv_classification_dataset',
        action=ImageDataset.to_csv,
        inputs_from_processes=['classification_dataset'],
        args={
            "dest_path": result_dataset_path, 
            "columns": dataset_columns
        })
    
    return "classification_dataset"

def train_classification_model_on_crops(pretrained_model_path,
                                        classification_dataset,
                                        train_dir,
                                        num_epochs,
                                        inception_v4_config,
                                        pipeline):
    # Set env variable to run training in all the GPUs of the node
    os.environ['CUDA_VISIBLE_DEVICES'] = \
        f"{','.join([str(x) for x in range(NUM_GPUS_PER_NODE)])}"
    os.makedirs(train_dir, exist_ok=True)
    
    pipeline.add_process(
        name='trainer_config',
        action=TFSlimTrainConfig.create,
        args={
            "clone_on_cpu": CLONE_ON_CPU,
            "num_clones": NUM_GPUS_PER_NODE
        })
    pipeline.add_process(
        name='inception_v4_model',
        action=ClassificationModel.load_saved_model,
        reportable=True,
        args={
            "source_path": pretrained_model_path,
            "model_config": {
                "inception_v4": inception_v4_config
            }
        })
    pipeline.add_process(
        name='train_classification_model',
        action=ClassificationTrainer.train,
        inputs_from_processes=[classification_dataset, "inception_v4_model", "trainer_config"],
        args={
            "train_config": {
                "checkpoint_exclude_scopes": "InceptionV4/AuxLogits,InceptionV4/Logits",
                "trainable_scopes": "InceptionV4/AuxLogits,InceptionV4/Logits",
                "batch_size": 32,
                "train_dir": train_dir,
                "num_epochs": num_epochs,
                "optimizer": {
                    'rms_prop': {
                        "learning_rate": {
                            'fixed': {
                                "learning_rate": 0.01
                            }
                        }
                    }
                }
            }
        })
    return "train_classification_model"

def train_and_eval_classif_model(classification_dataset,
                                 pretrained_model_path,
                                 train_dir,
                                 num_epochs,
                                 prediction_dataset_path,
                                 labelmap_filename_dir,
                                 eval_results_file,
                                 inception_v4_config, 
                                 all_classifications_path=None,
                                 wrong_classifications_path=None,
                                 correct_classifications_path=None,
                                 pipeline=None):
    if os.path.isfile(prediction_dataset_path):
        pipeline.add_process(
            name='inception_v4_model',
            action=ClassificationModel.load_saved_model,
            reportable=True,
            args={
                "source_path": pretrained_model_path,
                "model_config": {
                    "inception_v4": inception_v4_config
                }
            })
        pipeline.add_process(
            name='pred_dataset_classif_on_crops',
            action=ImagePredictionDataset.from_csv,
            args={
                "source_path": prediction_dataset_path,
                "labelmap": labelmap_filename_dir,
                "prediction_data": {
                    "prediction_type": "classification",
                    "thresholds": {
                        "max_classifs": 5
                    }
                }
            })
    else:
        # Train Inception v4 model on classification_dataset
        inception_v4_model = train_classification_model_on_crops(
            pretrained_model_path=pretrained_model_path, 
            classification_dataset=classification_dataset,
            train_dir=train_dir,
            num_epochs=num_epochs,
            inception_v4_config=inception_v4_config,
            pipeline=pipeline)
        
        pipeline.add_process(
            name='tf_predictor_config',
            action=TFPredictorConfig.create,
            args={
                "batch_size": 32, 
                "num_preprocessing_threads": 8
            })
        pipeline.add_process(
            name='pred_dataset_classif_on_crops',
            action=ClassificationModel.predict,
            inputs_from_processes=[inception_v4_model, classification_dataset, "tf_predictor_config"],
            args={
                "prediction_config": {
                    "dataset_partition": Partitions.TEST,
                    # "max_classifs": 1,
                    "labelmap": labelmap_filename_dir
                }
            })
        pipeline.add_process(
            name='to_csv_pred_dataset_classif_on_crops',
            action=ImagePredictionDataset.to_csv,
            inputs_from_processes=["pred_dataset_classif_on_crops"],
            args={
                "dest_path": prediction_dataset_path
            })

    pipeline.add_process(
        name='eval_process',
        action=ImageClassificationEvaluator.eval,  
        inputs_from_processes=[classification_dataset, "pred_dataset_classif_on_crops"],
        reportable=True,
        args={
            "eval_config": {
                "metrics_set": {
                    ImageClassificationMetrics.Sets.MULTICLASS: {
                        "average": "micro"
                    }
                },
                "dataset_partition": Partitions.TEST
            }
        }
    )

    if all_classifications_path is not None or \
            wrong_classifications_path is not None or \
            correct_classifications_path is not None:
        store_classification_results(
            dataset_true=classification_dataset, 
            dataset_pred=pred_dataset_classif_on_crops, 
            dataset_partition=None,
            all_classifications_path=all_classifications_path,
            wrong_classifications_path=wrong_classifications_path,
            correct_classifications_path=correct_classifications_path)

def create_sequences_in_test_set(WCS_test_json_file, test_images_path, max_secs=2):
    date_format = '%Y-%m-%d %H:%M:%S'
    last_location = ""
    last_date = None
    sequence_items = defaultdict(list)
    item_sequence = {}

    wcs_test = ConabioImageDataset.from_json(
        source_path=WCS_test_json_file,
        images_dir=test_images_path,
        mapping_images_fields={
            "date_captured": "datetime"
        },
        exclude_corrupted_images=True)

    wcs_df = wcs_test.as_dataframe(columns=["item", "location", "date_captured"])

    sorted_classifs = wcs_df.sort_values(
        by=["location", "date_captured"], axis=0, ascending=False, inplace=False)
    sorted_classifs["seq_id"] = ""
    for _, row in sorted_classifs.iterrows():
        if row['location'] == last_location:
            d2 = datetime.strptime(row['date_captured'].split('.')[0], date_format)
            d1 = datetime.strptime(last_date, date_format)
            sec_diff = abs((d2 - d1).total_seconds())
            if sec_diff > max_secs:
                sequence = str(uuid.uuid4())
        else:
            sequence = str(uuid.uuid4())
        # sequence_items[sequence].append(row["item"])
        row["seq_id"] = sequence
        # item_sequence[row["item"]] = sequence
        last_date = row['date_captured'].split('.')[0]
        last_location = row['location']
    # Order sequences to have the ones with most elements at the beginning
    # sequence_items = dict(sorted(sequence_items.items(), key=lambda k: len(k[1]), reverse=True))
    return sorted_classifs

def classify_crops_in_test_set(megadet_predicts_in_test_set,
                                WCS_test_json_file,
                                test_images_path,
                                crops_path,
                                score_threshold,
                                dataset_crops_path,
                                train_dir,
                                labelmap_file,
                                pred_dataset_path,
                                megadetector_path,
                                inception_v4_config,
                                num_tasks,
                                task_num):
    if os.path.isfile(dataset_crops_path):
        crops_of_test_set_dataset = ImagePredictionDataset.from_csv(
            source_path=dataset_crops_path, 
            labelmap=ConabioImageDataset.MEGADETECTOR_V4_LABELMAP)
    else:
        megadet_preds_on_test_set = get_predictions_on_test_set(
            json_path=WCS_test_json_file,
            dataset_path=megadet_predicts_in_test_set,
            images_dir=test_images_path,
            megadetector_path=megadetector_path,
            num_tasks=num_tasks,
            task_num=task_num)
        wcs_test_with_sequences = create_sequences_in_test_set(
            WCS_test_json_file, test_images_path, max_secs=2)

        data = pd.merge(megadet_preds_on_test_set.data, wcs_test_with_sequences, on="item")
        
        images_info = defaultdict(dict)
        for _, row in data.iterrows():
            images_info["seq_id"][row["item"]] = row["seq_id"]
        info = {
            "images_info": images_info
        }
        data = data.drop(columns=["location", "date_captured", "seq_id"], axis=1)
        megadet_preds_on_test_set = ImagePredictionDataset(data, info)
        crops_of_test_set_dataset = megadet_preds_on_test_set.create_classif_ds_from_det_crops(
            dest_path=crops_path,
            score_threshold=score_threshold,
            only_highest_score=False,
            include_id=True,
            inherit_fields=["seq_id"],
            labelmap=ConabioImageDataset.MEGADETECTOR_V4_LABELMAP)
        crops_of_test_set_dataset.to_csv(
            dest_path=dataset_crops_path,
            columns=["item", "label", "id", "seq_id"])

    if os.path.isfile(pred_dataset_path):
        pred_dataset_classif_on_crops = ImagePredictionDataset.from_csv(
            source_path=pred_dataset_path,
            labelmap=labelmap_file)
    else:
        tf_predictor_config = \
            TFPredictorConfig.create(batch_size=32, 
                                    num_preprocessing_threads=8)
        inception_v4 = ClassificationModel.load_saved_model(
            source_path=train_dir,
            model_config={
                "inception_v4": inception_v4_config
            })
        os.environ['CUDA_VISIBLE_DEVICES'] = "0"
        pred_dataset_classif_on_crops = \
            inception_v4.predict(dataset=crops_of_test_set_dataset,
                                execution_config=tf_predictor_config,
                                prediction_config={
                                    "dataset_partition": None,
                                    "keep_detection_id": True,
                                    "labelmap": labelmap_file
                                })
        pred_dataset_classif_on_crops.to_csv(dest_path=pred_dataset_path)

    data = crops_of_test_set_dataset.data.drop(columns=["label"], axis=1)
    data = pd.merge(data, pred_dataset_classif_on_crops.data, on="item")
    images_info = defaultdict(dict)
    for _, row in data.iterrows():
        images_info["seq_id"][row["item"]] = row["seq_id"]
    info = {
        "images_info": images_info
    }
    pred_dataset_classif_on_crops = ImagePredictionDataset(data, info)
    pred_dataset_classif_on_crops.to_csv(
        dest_path=pred_dataset_path, 
        columns=["item","id","seq_id","label","score","detection_id", "seq_id"])
    
    return pred_dataset_classif_on_crops

def generate_multiclass_dataset_for_competition(pred_dataset_classif_on_crops,
                                                dataset_classif_multiclass_path,
                                                megadet_predicts_in_test_set,
                                                WCS_image_level_anns_json_file,
                                                WCS_test_json_file,
                                                test_images_path,
                                                final_csv,
                                                sample_submission_path,
                                                images_results_path):
    if not os.path.isfile(dataset_classif_multiclass_path):
        megadet_preds_on_test_set = ImagePredictionDataset.from_csv(
            source_path=megadet_predicts_in_test_set,
            labelmap=ConabioImageDataset.MEGADETECTOR_V4_LABELMAP,
            columns=["item", "label", "bbox", "score", "id"])
        wcs_test = ConabioImageDataset.from_json(
            source_path=WCS_test_json_file,
            images_dir=test_images_path,
            exclude_corrupted_images=True)
        classif_multiclass_dataset = \
            create_multiclass_dataset_from_classifs_on_crops_and_test_ds(
                dataset=pred_dataset_classif_on_crops,
                test_dataset=wcs_test,
                pred_dataset_dets=megadet_preds_on_test_set,
                mode='sequence_grouping'
            )
        classif_multiclass_dataset.to_csv(dest_path=dataset_classif_multiclass_path)
    else:
        classif_multiclass_dataset = \
            ImageDataset.from_csv(source_path=dataset_classif_multiclass_path)

    classif_multiclass_dataset.to_folder(dest_path=images_results_path,
                                         split_in_labels=True,
                                         keep_originals=True)

    # Generate the final CSV file to send to the Kaggle competition
    dataset_utils.create_file_to_kaggle_competition(
        classif_multiclass_dataset,
        coco_json_file=WCS_image_level_anns_json_file,
        dest_path=final_csv,
        sample_submission=sample_submission_path
    )

def main():
    # Principals
    experiment = "all_WCS_and_iNat_imgs_2_3"
    base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020/"
    data_path = os.path.join(base_path, "data")    
    experiments_path = os.path.join("results", "train_predict_classification_model")
    results_path = os.path.join(experiments_path, experiment)
    # Annotations provided by (or created from) JSON files of competition
    WCS_detections_json_file = os.path.join(data_path, "metadata", "iwildcam2020_megadetector_results.json")
    WCS_image_level_anns_json_file = os.path.join(data_path, "metadata", "iwildcam2020_train_annotations.json")
    iNat_2017_image_level_anns_json_file = os.path.join(data_path, "metadata", "inaturalist_2017_to_iwildcam_train.json")
    iNat_2018_image_level_anns_json_file = os.path.join(data_path, "metadata", "inaturalist_2018_to_iwildcam_train.json")
    iNat_dataset_json_file = os.path.join(data_path, "metadata", "dataset_curated_iNat_images.json")
    WCS_dataset_json_file = os.path.join(data_path, "metadata", "dataset_all_WCS_dets_MegadetV4.json")
    WCS_test_json_file = os.path.join(data_path, "metadata", "iwildcam2020_test_information.json")
    # Folders
    WCS_images_dir = os.path.join(data_path, "train")
    tfrecords_path = os.path.join(results_path, "tfrecords")
    train_dir = os.path.join(results_path, 'train')
    WCS_test_images_dir = os.path.join(data_path, "test")
    crops_of_train_dataset_path = os.path.join(experiments_path, "crops_of_train_dataset")
    test_crops_path = os.path.join(experiments_path, "crops_of_test_set")
    wrong_classifications_test_part_path = os.path.join(results_path, 'classifications_test_part', 'wrong')
    all_classifications_test_part_path = os.path.join(results_path, 'classifications_test_part', 'all')
    correct_classifications_test_part_path = os.path.join(results_path, 'classifications_test_part', 'correct')
    classifications_test_set_path = os.path.join(results_path, 'classifications_test_set')
    # Datasets in CSV files
    dataset_path = os.path.join(results_path, "dataset.csv")
    iNat_detections = os.path.join(results_path, 'iNat_crops_annotations.csv')
    classifs_on_test_part_crops = os.path.join(results_path, f"classifs_on_test_part_crops.csv")
    megadet_predicts_in_test_set = os.path.join(data_path, "metadata", "megadet_v4_predicts_on_test_set.csv")
    aux_dataset_crops_of_megadet_preds = os.path.join(results_path, f"aux_dataset_crops_of_megadet_preds.csv")
    aux_megadetv4_preds_on_wcs = os.path.join(results_path, "WCS_train_dets_megadetV4.csv")
    pred_dataset_classif_on_crops_path = os.path.join(results_path, f"pred_dataset_classif_on_crops.csv")
    dataset_classif_multiclass_path = os.path.join(results_path, f"dataset_classif_multiclass.csv")
    # Pretrained models
    megadetector_v4_path = os.path.join(base_path, "scripts", "files", "megadetector_v4.pb")
    pretrained_inception_v4_path = os.path.join(base_path, "scripts", "files", 'inception_v4.ckpt')
    # Other files
    eval_results_on_TEST_partition = os.path.join(results_path, f"evaluation_on_test_part_results.json")
    labelmap_file = os.path.join(tfrecords_path, "labels.txt")
    final_csv = os.path.join(results_path, 'submission_11.csv')
    sample_submission_path = os.path.join(data_path, "metadata", "sample_submission.csv")
    score_threshold=0.3
    num_epochs=32
    train_perc=1.
    inception_v4_config = {
        "weight_decay": 0.00004
    }

    os.makedirs(results_path, exist_ok=True)

    parser = argparse.ArgumentParser()
    parser.add_argument('--num_tasks', default=None, type=int)
    parser.add_argument('--task_num', default=None, type=int)
    args = parser.parse_args()

    pipeline = Pipeline(results_path, draw_graph=False, name="pipeline_assets")

    if not os.path.isfile(WCS_dataset_json_file):
        create_WCS_dataset_from_Megadetector_v4_detections(
            annotations_file=WCS_image_level_anns_json_file,
            images_dir=WCS_images_dir,
            out_json_name=WCS_dataset_json_file,
            aux_csv_file=aux_megadetv4_preds_on_wcs,
            megadetector_path=megadetector_v4_path,
            num_tasks=args.num_tasks,
            task_num=args.task_num)

    if not os.path.isfile(iNat_dataset_json_file):
        create_iNat_dataset_from_detections(
            WCS_image_level_anns_json_file=WCS_image_level_anns_json_file,
            iNat_2017_image_level_anns_json_file=iNat_2017_image_level_anns_json_file,
            iNat_2018_image_level_anns_json_file=iNat_2018_image_level_anns_json_file,
            iNat_dataset_json_file=iNat_dataset_json_file, 
            iNat_images_dir=data_path, 
            megadetector_path=megadetector_v4_path,
            detections_dataset_path=iNat_detections, 
            score_threshold=score_threshold,
            num_tasks=args.num_tasks,
            task_num=args.task_num)

    crops_dataset = create_dataset_from_detection_crops(
        WCS_dataset_json_file=WCS_dataset_json_file,
        iNat_dataset_json_file=iNat_dataset_json_file,
        WCS_images_dir=WCS_images_dir,
        iNat_images_dir=data_path, 
        train_perc=train_perc,
        result_dataset_path=dataset_path,
        tfrecords_path=tfrecords_path,
        result_crops_path=crops_of_train_dataset_path,
        pipeline=pipeline)
    train_and_eval_classif_model(
        classification_dataset=crops_dataset,
        pretrained_model_path=pretrained_inception_v4_path,
        train_dir=train_dir,
        num_epochs=num_epochs,
        prediction_dataset_path=classifs_on_test_part_crops,
        labelmap_filename_dir=tfrecords_path,
        eval_results_file=eval_results_on_TEST_partition,
        inception_v4_config=inception_v4_config,
        pipeline=pipeline)
    pipeline.run(report_lang=languages.ES)
    pred_dataset_classif_on_crops = classify_crops_in_test_set(
        megadet_predicts_in_test_set=megadet_predicts_in_test_set,
        WCS_test_json_file=WCS_test_json_file,
        test_images_path=WCS_test_images_dir,
        crops_path=test_crops_path,
        score_threshold=score_threshold,
        dataset_crops_path=aux_dataset_crops_of_megadet_preds,
        train_dir=train_dir,
        labelmap_file=labelmap_file,
        pred_dataset_path=pred_dataset_classif_on_crops_path,
        megadetector_path=megadetector_v4_path,
        inception_v4_config=inception_v4_config,
        num_tasks=args.num_tasks,
        task_num=args.task_num)
    generate_multiclass_dataset_for_competition(
        pred_dataset_classif_on_crops=pred_dataset_classif_on_crops,
        dataset_classif_multiclass_path=dataset_classif_multiclass_path,
        megadet_predicts_in_test_set=megadet_predicts_in_test_set,
        WCS_image_level_anns_json_file=WCS_image_level_anns_json_file,
        WCS_test_json_file=WCS_test_json_file,
        test_images_path=WCS_test_images_dir,
        final_csv=final_csv,
        sample_submission_path=sample_submission_path,
        images_results_path=classifications_test_set_path)

if __name__ == "__main__":
    main()
