import os
from conabio_ml.datasets.images.datasets import COCOImageDataset
from conabio_ml.trainer.images.trainer_config import TFObjectDetectionAPITrainConfig
from conabio_ml.trainer.images.model import ObjectDetectionModel
from conabio_ml.trainer.images.trainer import ObjectDetectionTrainer

test_path = "/home/ecoinfo_usr/Projects/iwildcam2020/scripts"
results_path = os.path.join("results", "retrain_megadetector", "all_images")
dataset_path = os.path.join(results_path, 'dataset.csv')
train_dir = os.path.join(results_path, 'train')
json_path = os.path.join(results_path, "dataset_from_all_detections.json")
tfrecords_path = os.path.join(results_path, "tfrecords")
images_dir = "/mnt/ssd/Kaggle_competitions/iWildCam2020/train"
model_path = "/mnt/ssd/Kaggle_competitions/iWildCam2020/megadetector_v3.pb"

os.makedirs(tfrecords_path, exist_ok=True)
os.makedirs(train_dir, exist_ok=True)

if not os.path.isfile(dataset_path):
    dataset_json = COCOImageDataset.from_json(source_path=json_path,
                                              dest_path=results_path,
                                              #images_size=(800,600),
                                              #copy_images=True,
                                              images_dir=images_dir)
    dataset_json.split(train_perc=0.9, val_perc=0.1, test_perc=0)
    dataset_json.to_csv(dest_path=dataset_path)
    dataset_json.to_tfrecords(dest_path=tfrecords_path, num_shards=5)
else:
    dataset_json = COCOImageDataset.from_csv(source_path=dataset_path, 
                                             split_by_column="partition",
                                             tfrecords_path=tfrecords_path)

trainer_config = TFObjectDetectionAPITrainConfig.create(
    clone_on_cpu=False, num_clones=2)

detection_model = ObjectDetectionModel.load_saved_model(
    source_path=model_path,
    model_config={
        "faster_rcnn": {
            "image_resizer": {
                "keep_aspect_ratio_resizer": {
                    "min_dimension": 600,
                    "max_dimension": 1024
                }
            },
            "feature_extractor": {
                "type": 'faster_rcnn_inception_resnet_v2',
                "first_stage_features_stride": 8
            },
            "first_stage_anchor_generator": {
                "grid_anchor_generator": {
                    "height_stride": 8,
                    "width_stride": 8,
                    "scales": [0.25, 0.5, 1.0, 2.0],
                    "aspect_ratios": [0.5, 1.0, 2.0]
                }
            },
            "first_stage_atrous_rate": 2,
            "first_stage_box_predictor_conv_hyperparams": {
                "op": "CONV",
                "regularizer": {
                    "l2_regularizer": {
                        "weight": 0.0
                    }
                },
                "initializer": {
                    "truncated_normal_initializer": {
                        "stddev": 0.01
                    }
                }
            },
            "first_stage_nms_score_threshold": 0.0,
            "first_stage_nms_iou_threshold": 0.7,
            "first_stage_max_proposals": 300,
            "first_stage_localization_loss_weight": 2.0,
            "first_stage_objectness_loss_weight": 1.0,
            "initial_crop_size": 17,
            "maxpool_kernel_size": 1,
            "maxpool_stride": 1,
            "second_stage_box_predictor": {
                "mask_rcnn_box_predictor": {
                    "fc_hyperparams": {
                        "op": "FC",
                        "regularizer": {
                            "l2_regularizer": {
                                "weight": 0.0
                            }
                        },
                        "initializer": {
                            "variance_scaling_initializer": {
                                "factor": 1.0,
                                "uniform": True,
                                "mode": "FAN_AVG"
                            }
                        }
                    }
                }
            },
            "second_stage_post_processing": {
                "batch_non_max_suppression": {
                    "score_threshold": 0.0,
                    "iou_threshold": 0.6,
                    "max_detections_per_class": 100,
                    "max_total_detections": 100
                },
                "score_converter": "SOFTMAX"
            },
            "second_stage_localization_loss_weight": 2.0,
            "second_stage_classification_loss_weight": 1.0
        }
    })

ObjectDetectionTrainer.train(
    dataset=dataset_json, 
    model=detection_model,
    execution_config=trainer_config,
    train_config={
    "train_dir": train_dir,
    # Hay que poner este valor igual a `num_clones`
    "batch_size": 2,
    "data_augmentation_options": [
        {
            "random_crop_image": {
                "min_object_covered": 0.8,
                "min_area": 0.8
            }
        }
    ],
    "optimizer": {
        "momentum_optimizer": {
            "learning_rate": {
                "manual_step_learning_rate": {
                    "initial_learning_rate": 3.000000106112566e-06,
                    "schedule": [
                        {
                            "epoch": 5,
                            "learning_rate": 3.000000106112566e-06
                        }
                    ]
                }
            },
            "momentum_optimizer_value": 0.9
        },
        "use_moving_average": False
    },
    "gradient_clipping_by_norm": 10.0,
    "dropout_keep_probability": 1.0,
    "num_epochs": 10
})