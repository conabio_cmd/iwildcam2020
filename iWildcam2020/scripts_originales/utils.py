import os
import sys
import json
import numpy
from glob import glob
from math import floor
import numpy as np
import pandas
import uuid
import multiprocessing
from multiprocessing import Manager
from conabio_ml.datasets.dataset import Dataset
from conabio_ml.datasets.images.datasets import ConabioImageDataset, ImagePredictionDataset, ImageDataset
from conabio_ml.trainer.images.predictor_config import TFPredictorConfig
from conabio_ml.trainer.images.model import ObjectDetectionModel
from conabio_ml.utils.model_utils import get_coordinates_from_str_to_array
from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)

NUM_GPUS_PER_NODE = 2
def create_WCS_dataset_from_given_detections(detections_file, 
                                             annotations_file,
                                             include_empty, 
                                             json_name):
    detections_data = json.load(open(detections_file))
    annotations_data = json.load(open(annotations_file))
    images_to_ann = dict()
    images_dict = dict()
    categories_dict = dict()

    MEGADETECTOR_ANIMAL_LABEL = "1"
    MEGADETECTOR_PERSON_LABEL = "2"
    EMPTY_CAT_ID = 0

    for ann in annotations_data['annotations']:
        images_to_ann[ann['image_id']] = ann
    for image in annotations_data['images']:
        images_dict[image['id']] = image
    for category in annotations_data['categories']:
        categories_dict[category['id']] = category['name']
    anns_json = []

    def _insert_empty(id, image_id):
        anns_json.append({
            "id": id,
            "image_id": image_id,
            "category_id": EMPTY_CAT_ID
        })

    for image_det in detections_data['images']:
        image_id = image_det['id']
        if image_id not in images_to_ann: # Test images
            continue
        image = images_dict[image_id]
        detections = image_det['detections']
        annotation = images_to_ann[image_id]
        category_id = annotation['category_id']

        if category_id in (79, 290):            # ('unknown', 'unidentifiable')
            continue

        is_human = False
        ann_inserted = False
        if category_id in (558, 347, 348, 404):      # ('motorcycle', 'start', 'end', 'misfire')
            is_human = True

        if category_id == EMPTY_CAT_ID and include_empty:
            _insert_empty(annotation['id'], image_id)
        else:
            for i, detection in enumerate(detections):
                if is_human and detection["category"] != MEGADETECTOR_PERSON_LABEL:
                    continue
                # elif not is_human and detection["category"] != MEGADETECTOR_ANIMAL_LABEL:
                #     continue
                (x, y, w, h) = detection['bbox']
                (height, width) = image['height'], image['width']
                (x, y, w, h) = (floor(x*width) , floor(y*height), floor(w*width), floor(h*height))
                bbox = [x, y, w, h]
                anns_json.append({
                    "id": f"{annotation['id']}-{str(i).zfill(3)}",
                    "image_id": image_id,
                    "category_id": category_id,
                    "bbox": bbox
                })
                ann_inserted = True
            if not ann_inserted and include_empty:
                _insert_empty(annotation['id'], image_id)
    categories = [{"id": k, "name": v} for k, v in categories_dict.items()]
    json_data = {
        "info": annotations_data['info'],
        "images": annotations_data["images"],
        "categories": categories,
        "annotations": anns_json
    }
    with open(json_name, 'w',) as outfile:
        json.dump(json_data, outfile)

def create_WCS_dataset_from_Megadetector_v4_detections(annotations_file,
                                                       images_dir,
                                                       out_json_name,
                                                       aux_csv_file,
                                                       megadetector_path,
                                                       num_tasks,
                                                       task_num):
    if os.path.isfile(aux_csv_file):
        detections_dataset = ImageDataset.from_csv(source_path=aux_csv_file)
    else:
        wcs_dataset = ConabioImageDataset.from_json(
            source_path=annotations_file,
            images_dir=images_dir,
            exclude_corrupted_images=True)
        splitted_dets_path = ''
        if num_tasks is not None and task_num is not None:
            format_split_path = 'prediction_dataset_{0}_of_{1}.csv'
            glob_pattern = "prediction_dataset_*_of_*.csv"
            predictions_dirname = os.path.dirname(aux_csv_file)
            splitted_predictions_dets_path = \
                os.path.join(predictions_dirname, "splitted_detections_wcs")
            os.makedirs(splitted_predictions_dets_path, exist_ok=True)
            splitted_dets_path = os.path.join(
                splitted_predictions_dets_path,
                format_split_path.format(task_num, num_tasks))
            gpu_device = (task_num-1) % NUM_GPUS_PER_NODE
            os.environ['CUDA_VISIBLE_DEVICES'] = f"{gpu_device}"
        megadetector_model = \
            ObjectDetectionModel.load_saved_model(source_path=megadetector_path)
        exec_config = TFPredictorConfig.create(
            batch_size=1,
            num_tasks=num_tasks,
            task_num=task_num)
        detections_dataset = megadetector_model.predict(
            dataset=wcs_dataset,
            execution_config=exec_config,
            prediction_config={
                "min_score_threshold": 0.3,
                "dataset_partition": None,
                "include_id": True,
                "keep_image_id": True,
                "labelmap": ConabioImageDataset.MEGADETECTOR_V4_LABELMAP
            })
        if num_tasks is not None and task_num is not None:
            detections_dataset.to_csv(dest_path=splitted_dets_path)
            splits_files = glob(os.path.join(splitted_predictions_dets_path, glob_pattern))
            if len(splits_files) < num_tasks:
                logger.info(
                    f"Exit program after create  split {task_num} of {num_tasks}.")
                sys.exit()
            logger.info(f"Creation of {num_tasks} splits complete.")
            detections_dataset = ImagePredictionDataset.from_csv(
                source_path=splitted_predictions_dets_path,
                labelmap=ConabioImageDataset.MEGADETECTOR_V4_LABELMAP)
        detections_dataset.to_csv(dest_path=aux_csv_file)

    annotations_data = json.load(open(annotations_file))
    images_to_ann = dict()
    images_dict = dict()
    categories_dict = dict()
    EMPTY_CAT_ID = 0
    for ann in annotations_data['annotations']:
        images_to_ann[ann['image_id']] = ann
    for image in annotations_data['images']:
        images_dict[image['id']] = image
    for category in annotations_data['categories']:
        categories_dict[category['id']] = category['name']

    anns_json = []        
    df = detections_dataset.as_dataframe()
    for _, row in df.iterrows():
        annotation = images_to_ann[row["image_id"]]
        bbox = get_coordinates_from_str_to_array(row['bbox'])
        category_id = annotation['category_id']
        # 'empty'
        if category_id == EMPTY_CAT_ID:
            continue
        # ('unknown', 'unidentifiable')
        if category_id in (79, 290):
            continue
        # 'misfire'
        if category_id == 404 and row["label"] != ConabioImageDataset.PERSON_LABEL:
            continue
        # ('motorcycle', 'start', 'end')
        if category_id in (558, 347, 348) and row["label"] not in \
                (ConabioImageDataset.PERSON_LABEL, ConabioImageDataset.VEHICLE_LABEL):
            continue
        if category_id not in (558, 347, 348, 404) and row["label"] != ConabioImageDataset.ANIMAL_LABEL:
            continue
        anns_json.append({
            "id": row['id'],
            "image_id": row["image_id"],
            "category_id": category_id,
            "bbox": bbox
        })
    all_image_ids = [x for x in images_to_ann.keys()]
    inserted_image_ids = [x["image_id"] for x in anns_json]
    empty_items = numpy.setdiff1d(all_image_ids, inserted_image_ids)
    for image_id in empty_items:
        anns_json.append({
            "id": str(uuid.uuid4()),
            "image_id": image_id,
            "category_id": EMPTY_CAT_ID
        })
    categories = [{"id": k, "name": v} for k, v in categories_dict.items()]
    json_data = {
        "info": annotations_data['info'],
        "images": annotations_data["images"],
        "categories": categories,
        "annotations": anns_json
    }
    with open(out_json_name, 'w',) as outfile:
        json.dump(json_data, outfile)

def create_iNat_dataset_from_detections(
        WCS_image_level_anns_json_file,        # iwildcam2020_train_annotations.json
        iNat_2017_image_level_anns_json_file, # inaturalist_2017_to_iwildcam_train.json
        iNat_2018_image_level_anns_json_file, # inaturalist_2018_to_iwildcam_train.json
        iNat_dataset_json_file, 
        iNat_images_dir,
        megadetector_path,
        detections_dataset_path,            # iNat_crops_annotations.csv
        score_threshold=0.3,
        num_tasks=None,
        task_num=None):
    if os.path.isfile(iNat_dataset_json_file):
        return
    dataset_columns = ["item", "label", "bbox", "score", "id"]
    wcs_json = json.load(open(WCS_image_level_anns_json_file))
    # Fix iNat 2017 and 2018 "categories" field to be the same as WCS

    inat_2017_json = json.load(open(iNat_2017_image_level_anns_json_file))
    inat_2017_json_new = {
        "images": inat_2017_json["images"],
        "annotations": inat_2017_json["annotations"],
        "categories": wcs_json["categories"],
        "info": inat_2017_json["info"],
    }
    with open(iNat_2017_image_level_anns_json_file, 'w') as _f:
        json.dump(inat_2017_json_new, _f)
    
    inat_2018_json = json.load(open(iNat_2018_image_level_anns_json_file))
    inat_2018_json_new = {
        "images": inat_2018_json["images"],
        "annotations": inat_2018_json["annotations"],
        "categories": wcs_json["categories"],
        "info": inat_2018_json["info"],
    }
    with open(iNat_2018_image_level_anns_json_file, 'w') as _f:
        json.dump(inat_2018_json_new, _f)

    inat_2017_dataset = ConabioImageDataset.from_json(
        source_path=iNat_2017_image_level_anns_json_file,
        images_dir=iNat_images_dir,
        collection="inat_2017")
    inat_2018_dataset = ConabioImageDataset.from_json(
        source_path=iNat_2018_image_level_anns_json_file,
        images_dir=iNat_images_dir,
        collection="inat_2018")
    inat_dataset = ConabioImageDataset.from_datasets(
        inat_2017_dataset,
        inat_2018_dataset)

    if os.path.isfile(detections_dataset_path):
        detections_dataset = ImagePredictionDataset.from_csv(
            source_path=detections_dataset_path, 
            columns=dataset_columns)
    else:
        splitted_dets_path = ''
        if num_tasks is not None and task_num is not None:
            format_split_path = 'prediction_dataset_{0}_of_{1}.csv'
            glob_pattern = "prediction_dataset_*_of_*.csv"
            predictions_dirname = os.path.dirname(detections_dataset_path)
            splitted_predictions_dets_path = \
                os.path.join(predictions_dirname, "splitted_detections_inat")
            os.makedirs(splitted_predictions_dets_path, exist_ok=True)
            splitted_dets_path = os.path.join(
                splitted_predictions_dets_path,
                format_split_path.format(task_num, num_tasks))
            gpu_device = (task_num-1) % NUM_GPUS_PER_NODE
            os.environ['CUDA_VISIBLE_DEVICES'] = f"{gpu_device}"

        megadetector_model = \
            ObjectDetectionModel.load_saved_model(source_path=megadetector_path)
        exec_config = TFPredictorConfig.create(batch_size=1,
                                            num_tasks=num_tasks,
                                            task_num=task_num)
        detections_dataset = megadetector_model.predict(
            dataset=inat_dataset,
            execution_config=exec_config,
            prediction_config={
                "max_number_of_boxes": 10,
                "dataset_partition": None,
                "include_id": True,
                "labelmap": ConabioImageDataset.MEGADETECTOR_V4_LABELMAP
            })

        if num_tasks is not None and task_num is not None:
            detections_dataset.to_csv(dest_path=splitted_dets_path)
            splits_files = glob(os.path.join(splitted_predictions_dets_path, glob_pattern))
            if len(splits_files) < num_tasks:
                logger.info(
                    f"Exit program after create  split {task_num} of {num_tasks}.")
                sys.exit()
            logger.info(f"Creation of {num_tasks} splits complete.")
            detections_dataset = ImagePredictionDataset.from_csv(
                source_path=splitted_predictions_dets_path,
                labelmap=ConabioImageDataset.MEGADETECTOR_V4_LABELMAP)
        
        detections_dataset.to_csv(dest_path=detections_dataset_path, columns=dataset_columns)

    # Change labels of Megadetector ('Animal', 'Person') with labels of iNat 
    # image level annotations
    anns_json = list()
    imgs_json = list()
    items = list()
    
    categories_dict_inv = dict()
    for category in wcs_json["categories"]:
        categories_dict_inv[category['name']] = category['id']

    df_predict = detections_dataset.as_dataframe()
    df_inat = inat_dataset.as_dataframe(columns=["item", "image_id", "height", "width", "file_name"])

    for i, row in df_predict.iterrows():
        # Insert at least one annotation for each image
        if row['score'] < score_threshold and row['item'] in items:
            continue
        inat_row = df_inat.loc[df_inat["item"] == row["item"]].iloc[0]
        bbox = [int(x) for x in row["bbox"].split(',')]
        anns_json.append({
            "id": row["id"],
            "image_id": inat_row["image_id"],
            "category_id": int(categories_dict_inv[inat_row["label"]]),
            "bbox": bbox
        })
        if row['item'] not in items:
            imgs_json.append({
                "id": inat_row["image_id"],
                "file_name": inat_row["file_name"],
                "height": int(inat_row["height"]),
                "width": int(inat_row["width"])
            })
        items.append(row['item'])
    inat_json_new = {
        "images": imgs_json,
        "annotations": anns_json,
        "categories": wcs_json["categories"],
        "info": inat_2018_json["info"],
    }
    with open(iNat_dataset_json_file, 'w') as _f:
        json.dump(inat_json_new, _f)

def get_predictions_on_test_set(json_path,
                                dataset_path,
                                images_dir,
                                megadetector_path,
                                num_tasks,
                                task_num):
    columns = ["item", "label", "bbox", "score", "image_id", "id"]
    if os.path.isfile(dataset_path):
        detections_dataset = ImagePredictionDataset.from_csv(
            source_path=dataset_path,
            labelmap=ConabioImageDataset.MEGADETECTOR_V4_LABELMAP,
            columns=columns)
    else:
        wcs_test = ConabioImageDataset.from_json(
            source_path=json_path,
            images_dir=images_dir,
            exclude_corrupted_images=True)
        splitted_dets_path = ''
        if num_tasks is not None and task_num is not None:
            format_split_path = 'prediction_dataset_{0}_of_{1}.csv'
            glob_pattern = "prediction_dataset_*_of_*.csv"
            predictions_dirname = os.path.dirname(dataset_path)
            splitted_predictions_dets_path = \
                os.path.join(predictions_dirname, "splitted_detections_test_wcs")
            os.makedirs(splitted_predictions_dets_path, exist_ok=True)
            splitted_dets_path = os.path.join(
                splitted_predictions_dets_path,
                format_split_path.format(task_num, num_tasks))
            gpu_device = (task_num-1) % NUM_GPUS_PER_NODE
            os.environ['CUDA_VISIBLE_DEVICES'] = f"{gpu_device}"
        megadetector_model = \
            ObjectDetectionModel.load_saved_model(source_path=megadetector_path)
        exec_config = TFPredictorConfig.create(
            batch_size=1,
            num_tasks=num_tasks,
            task_num=task_num)
        detections_dataset = megadetector_model.predict(
            dataset=wcs_test,
            execution_config=exec_config,
            prediction_config={
                "min_score_threshold": 0.3,
                "dataset_partition": None,
                "include_id": True,
                "keep_image_id": True,
                "labelmap": ConabioImageDataset.MEGADETECTOR_V4_LABELMAP
            })
        if num_tasks is not None and task_num is not None:
            detections_dataset.to_csv(dest_path=splitted_dets_path)
            splits_files = glob(os.path.join(splitted_predictions_dets_path, glob_pattern))
            if len(splits_files) < num_tasks:
                logger.info(
                    f"Exit program after create  split {task_num} of {num_tasks}.")
                sys.exit()
            logger.info(f"Creation of {num_tasks} splits complete.")
            detections_dataset = ImagePredictionDataset.from_csv(
                source_path=splitted_predictions_dets_path,
                labelmap=ConabioImageDataset.MEGADETECTOR_V4_LABELMAP,
                columns=columns)
        detections_dataset.to_csv(dest_path=dataset_path, columns=columns)

    return detections_dataset

def create_multiclass_dataset_from_classifs_on_crops_and_test_ds(dataset: Dataset.DatasetType,
                                                                 test_dataset,
                                                                 pred_dataset_dets,
                                                                 mode='higher_detection'):
        if dataset.is_detection_dataset():
            raise Exception(f"You must use a classification dataset made by detections "
                            f"crops for this operation")
        logger.info(f"Creating a multiclass dataset from classifications on crops")
        manager = Manager()
        item_label_dict = manager.dict()
        num_processes = multiprocessing.cpu_count()
        tuples = list()
        classifs_df = dataset.as_dataframe(splitted=False)
        test_df = test_dataset.as_dataframe(splitted=False)
        detections_df = pred_dataset_dets.as_dataframe(splitted=False)
        if mode == 'higher_classification':
            for item in detections_df["item"].unique():
                tuples.append((item, detections_df, classifs_df, item_label_dict))
            with multiprocessing.Pool(processes=num_processes) as pool:
                pool.starmap(find_higher_classification_of_detections, tuples)
        if mode == 'sequence_grouping':
            for seq_id in classifs_df["seq_id"].unique():
                tuples.append((seq_id, detections_df, classifs_df, item_label_dict))
            with multiprocessing.Pool(processes=num_processes) as pool:
                pool.starmap(find_higher_classification_of_detections_in_sequences, tuples)
        else:  # higher_detection
            for _, row in classifs_df.iterrows():
                tuples.append((row, detections_df, item_label_dict))
            with multiprocessing.Pool(processes=num_processes) as pool:
                pool.starmap(find_classification_of_higher_detection, tuples)
        items = list()
        labels = list()
        for item, label in item_label_dict.items():
            items.append(item)
            labels.append(label)
        ds = {
            "item": items,
            "label": labels
            # TODO add id
        }
        logger.debug("Adding empty labels in dataset")
        test_items = test_df["item"].unique()
        added_items = ds["item"]
        empty_items = np.setdiff1d(test_items, added_items)
        for empty_item in empty_items:
            ds["item"].append(empty_item)
            ds["label"].append('empty')
        data = pandas.DataFrame(ds)
        return ImageDataset(data, {})

def find_classification_of_higher_detection(row, detections_df, items_dict):
    item = detections_df.loc[detections_df["id"] == row['detection_id']].iloc[0]["item"]
    items_dict[item] = row['label']

def find_higher_classification_of_detections(item, detections_df, classif_ds, items_dict):
    dets_id = detections_df.loc[detections_df["item"] == item]["id"]
    classifs = classif_ds.loc[classif_ds["detection_id"].isin(dets_id)]
    sorted_classifs = classifs.sort_values(by=["score"], axis=0, ascending=False, inplace=False)
    selected_classif = sorted_classifs.iloc[0]
    if selected_classif["score"] >= 0.3:
        items_dict[item] = selected_classif["label"]
    else:
        items_dict[item] = 'empty'

def find_higher_classification_of_detections_in_sequences(seq_id, detections_df, classif_ds, items_dict):
    # dets_id = classif_ds.loc[classif_ds["seq_id"] == seq_id]["detection_id"]
    classifs_of_seq = classif_ds.loc[classif_ds["seq_id"] == seq_id]
    sorted_classifs_of_seq = classifs_of_seq.sort_values(by=["score"], axis=0, ascending=False, inplace=False)
    
    selected_label = sorted_classifs_of_seq["label"].mode().iloc[0]
    
    selected_detection_ids = sorted_classifs_of_seq["detection_id"]
    items = detections_df.loc[detections_df["id"].isin(selected_detection_ids)]["item"]
    for item in items:
        items_dict[item] = selected_label

def create_file_to_kaggle_competition(dataset, 
                                      coco_json_file, 
                                      dest_path, 
                                      sample_submission=None):
    annotations_data = json.load(open(coco_json_file))
    categories_inverse_dict = dict()
    for category in annotations_data['categories']:
        categories_inverse_dict[category['name']] = category['id']
    new_ds = {
        "Id": [],
        "Category": []
    }
    if sample_submission is not None:
        sample_sub = pandas.read_csv(sample_submission, header=0)
        sample_ids = [row["Id"] for _, row in sample_sub.iterrows()]
    df = dataset.as_dataframe()
    for _, row in df.iterrows():
        id = os.path.basename(os.path.splitext(row["item"])[0])
        if sample_submission is not None and id not in sample_ids:
            continue
        category_id = categories_inverse_dict[row["label"]]
        new_ds["Id"].append(id)
        new_ds["Category"].append(category_id)
    data = pandas.DataFrame(new_ds)
    data.to_csv(dest_path, index=False, header=True)
    
    logger.debug(f"CSV file {dest_path} created")
