
    basename_site_date = os.path.basename(load_path)
    name = os.path.join(out_dir, f"{basename_site_date}_rgb.png")
    r = I_multispectral[:,:,rs.multispectral_idx['red']]
    g = I_multispectral[:,:,rs.multispectral_idx['green']]
    b = I_multispectral[:,:,rs.multispectral_idx['blue']]
    rgb = np.dstack((r, g, b)).astype(np.uint16)
    rgb = rs._stretch_im(rgb)
    r = rs.norm(rgb[:,:,0])
    g = rs.norm(rgb[:,:,1])
    b = rs.norm(rgb[:,:,2])
    rgb = np.dstack((r, g, b)).astype(np.uint8)

    # rgb = equalization(r, g, b)
    im_data = Image.fromarray(rgb)
    im_data = im_data.convert('RGB')
    im_data.save(name)
    # plt.imsave(name, rgb, format="png")
    
    # Store infrared image
    name = os.path.join(out_dir, f"{basename_site_date}_infrared.png")
    data = I_multispectral[:,:,rs.multispectral_idx['infrared']].astype(np.uint16)
    data = rs.norm(rs._stretch_im(data)).astype(np.uint8)
    im_data = Image.fromarray(data)
    im_data = im_data.convert('RGB')
    im_data.save(name)


    if I_pixelqa_parsed is not None:
        #Store cloud images
        name = os.path.join(out_dir, f"{basename_site_date}_cloud.png")
        data = I_pixelqa_parsed[:,:,rs.pixelqa_idx['cloud']]
        plt.imsave(name, data, format="png")
        #Store shadow images
        name = os.path.join(out_dir, f"{basename_site_date}_shadow.png")
        data = I_pixelqa_parsed[:,:,rs.pixelqa_idx['cloud_shadow']]
        plt.imsave(name, data, format="png")


    
    
    

    # Store infrared image (latest)
    file_path = os.path.join(out_dir, f"{basename}.png")
    data = rs.norm(I_multispectral[:,:,rs.multispectral_idx['infrared']]).astype(np.uint8)
    data = cv2.equalizeHist(data)
    im_data = Image.fromarray(data)
    im_data = im_data.convert('RGB')
    im_data.save(file_path)


    model = Sequential([
        Conv2D(16, 3, padding='same', activation='relu', input_shape=(IMG_HEIGHT, IMG_WIDTH ,3)),
        MaxPooling2D(),
        Conv2D(16, 3, padding='same', activation='relu'),
        MaxPooling2D(),
        Conv2D(16, 3, padding='same', activation='relu'),
        MaxPooling2D(),
        Flatten(),
        Dense(256, activation='relu'),
        Dense(len(total_train_data_gen.class_indices))
    ])


import os, sys
import glob
import pandas as pd
import json

submission = pd.read_csv("/Users/jlopez/Downloads/submission_11.csv")

data_json = json.load(open("/Users/jlopez/Downloads/iwildcam2020_test_information.json"))
data_images = {}

xx = [y["id"] for y in [x for x in data_json["images"] if x["location"] == 352]]

# Bos taurus
submission.set_value((submission["Category"] == 71) & (submission["Id"].isin(xx)), "Category", 96)

# Madoqua guentheri
submission.set_value((submission["Category"] == 115) & (submission["Id"].isin(xx)), "Category", 96)

records = glob.glob("/Users/jlopez/Downloads/new/empty/*.jpg")
empties = [os.path.basename(record).split(".")[0] for record in records]

submission.set_value(submission["Id"].isin(empties), "Category", 0)

submission.to_csv("/Users/jlopez/Downloads/submission_13.csv", index=False, header=True)
