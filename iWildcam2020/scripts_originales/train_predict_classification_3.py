"""
Train a classification model (Inception v3) with all categories of the WCS and
using Landsat imagery.
"""
import os
import sys
import json
from glob import glob
import uuid
import re
from operator import itemgetter
import argparse
import numpy as np
from conabio_ml.datasets.images.datasets import ConabioImageDataset, ImagePredictionDataset, ImageDataset
from conabio_ml.trainer.images.trainer_config import TFSlimTrainConfig
from conabio_ml.trainer.images.predictor_config import TFPredictorConfig
from conabio_ml.trainer.images.model import ClassificationModel, ObjectDetectionModel
from conabio_ml.evaluator.images.evaluator import ImageClassificationEvaluator,\
    ImageClassificationMetricsSets
from conabio_ml.trainer.images.trainer import ClassificationTrainer
from conabio_ml.datasets.dataset import Partitions
from conabio_ml.utils.evaluator_utils import store_eval_results, load_eval_results, \
    store_classification_results
import conabio_ml.utils.dataset_utils as dataset_utils
from utils import create_WCS_dataset_from_given_detections, \
    create_WCS_dataset_from_Megadetector_v4_detections, create_iNat_dataset_from_detections, \
    get_predictions_on_test_set
from conabio_ml.utils.logger import get_logger

from conabio_ml.trainer.images.models.research.slim.datasets import dataset_utils as slim_utils


import tensorflow as tf
import keras
from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D, MaxPool1D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.utils import class_weight
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.preprocessing import image

logger = get_logger(__name__)

NUM_GPUS_PER_NODE = 2
BATCH_SIZE = 32
IMG_HEIGHT = 299
IMG_WIDTH = 299
LABELS_FILENAME = 'labels.txt'
checkpoints_filename_glob = "model.*.h5"
checkpoints_filename = "model.{epoch:02d}.h5"
checkpoints_filename_regex = 'model\.(?P<_0>[0-9]+)\.h5'
    
def create_dataset_from_detection_crops(WCS_dataset_json_file,
                                         iNat_dataset_json_file,
                                         WCS_images_dir,
                                         iNat_images_dir,
                                         train_perc,
                                         result_dataset_path,
                                         tfrecords_path,
                                         result_crops_path):
    dataset_columns = ["item", "label"]
    if os.path.isfile(result_dataset_path):
        classification_dataset = \
            ConabioImageDataset.from_csv(
                source_path=result_dataset_path,
                split_by_column="partition",
                columns=dataset_columns,
                # tfrecords_path=tfrecords_path
                )
        return classification_dataset
    
    inat_detection_dataset = ConabioImageDataset.from_json(
        source_path=iNat_dataset_json_file,
        images_dir=iNat_images_dir)
    inat_classification_dataset = \
        inat_detection_dataset.create_classif_ds_from_bboxes_crops(dest_path=result_crops_path)
    inat_classification_dataset.split(train_perc=train_perc, 
                                      val_perc=0, 
                                      test_perc=1.-train_perc)
    
    # wcs_detection_dataset = ConabioImageDataset.from_json(
    #     source_path=WCS_dataset_json_file,
    #     images_dir=WCS_images_dir)
    # wcs_classification_dataset = wcs_detection_dataset\
    #     .create_classif_ds_from_bboxes_crops(
    #         dest_path=result_crops_path,
    #         inherit_fields=["location"])
    # wcs_classification_dataset.split(train_perc=train_perc, 
    #                                  val_perc=0, 
    #                                  test_perc=1.-train_perc, 
    #                                  group_by_field="location")

    # classification_dataset = \
    #     ConabioImageDataset.from_datasets(
    #         wcs_classification_dataset, inat_classification_dataset)
    classification_dataset = inat_classification_dataset
    classification_dataset.to_csv(dest_path=result_dataset_path, columns=dataset_columns)

    return classification_dataset

def get_data_generator(classification_dataset, images_dir):
    df = classification_dataset.as_dataframe()  # get split
    total_train_image_generator = ImageDataGenerator(
        rescale=1./255, 
        rotation_range=20, 
        zoom_range=0.15, 
        width_shift_range=0.1, 
        height_shift_range=0.1, 
        shear_range=0.15, 
        horizontal_flip=True, 
        fill_mode="nearest")
    train_data_gen = total_train_image_generator.flow_from_dataframe(
        dataframe=df, 
        directory=images_dir, 
        x_col='item', 
        y_col='label', 
        class_mode="categorical", 
        target_size=(IMG_HEIGHT, IMG_WIDTH),
        batch_size=BATCH_SIZE)

    return train_data_gen

def get_base_model(classification_dataset):
    NUM_CLASSES = classification_dataset.get_num_categories()
    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        base_model = InceptionV3(weights='imagenet', include_top=False)
        base_model.trainable = False
        x = base_model.output
        x = GlobalAveragePooling2D(name='avg_pool')(x)
        x = Dropout(0.4)(x)
        predictions = Dense(NUM_CLASSES, activation='softmax')(x)
        model = Model(inputs=base_model.input, outputs=predictions)
        model.compile(
            optimizer='rmsprop',
            loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            metrics=['accuracy'])
    return model

def train_classif_model(classification_dataset,
                        train_dir,
                        train_images_dir,
                        rs_imagery_path,
                        num_epochs):
    # Set env variable to run training in all the GPUs of the node
    os.environ['CUDA_VISIBLE_DEVICES'] = \
        f"{','.join([str(x) for x in range(NUM_GPUS_PER_NODE)])}"

    records = glob(os.path.join(rs_imagery_path, "iwc_*.png"))
    if not records:
        raise ValueError(f"No images files stored in {rs_imagery_path}")
    location_img_dict = {}
    for record in records:
        record_name = os.path.basename(record)
        location = int(os.path.splitext(record_name)[0].split('_')[1])
        location_img_dict[location] = record
    
    os.makedirs(train_dir, exist_ok=True)

    train_data_gen = get_data_generator(classification_dataset, train_images_dir)

    class_weights = class_weight.compute_class_weight(
        'balanced',
        np.unique(train_data_gen.classes),
        train_data_gen.classes)

    records = glob(os.path.join(train_dir, checkpoints_filename_glob))
    if len(records) > 0:
        records = sorted(records, reverse=True)
        model = load_model(records[0])
        record_name = os.path.basename(records[0])
        m = re.match(checkpoints_filename_regex, record_name)
        res = [x for x in map(itemgetter(1), sorted(m.groupdict().items()))]
        initial_epoch = num_shards = int(res[0])
    else:
        model = get_base_model(classification_dataset)
        initial_epoch = 0

    cp_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=os.path.join(train_dir, checkpoints_filename),
        save_weights_only=False,
        verbose=1)

    history = model.fit(
        train_data_gen,
        epochs=num_epochs,
        class_weight=class_weights,
        initial_epoch=initial_epoch,
        callbacks=[cp_callback]
    )

def eval_classif_model(dataset_crops_path,
                       train_dataset,
                       train_images_dir,
                       train_dir,
                       labelmap_file):
    crops_of_test_set_dataset = ImagePredictionDataset.from_csv(
        source_path=dataset_crops_path,
        labelmap=ConabioImageDataset.MEGADETECTOR_V4_LABELMAP)
    results = {
        "item": [],
        "label": [],
        "score": []
    }
    train_data_gen = get_data_generator(train_dataset, train_images_dir)
    inverse_labelmap = train_data_gen.class_indices
    labelmap = {v: k for k, v in inverse_labelmap.items()}
    
    # with open(labelmap_file, 'rb') as f:
    #     lines = f.read().decode()
    # lines = lines.split('\n')
    # lines = filter(None, lines)
    # labelmap = {}
    # for line in lines:
    #     index = line.index(':')
    #     labelmap[int(line[:index])] = line[index+1:]

    records = glob(os.path.join(train_dir, checkpoints_filename_glob))
    if len(records) > 0:
        records = sorted(records, reverse=True)
        model = load_model(records[0])
        df_test = crops_of_test_set_dataset.as_dataframe()
        for _, row in df_test.iterrows():
            img = image.load_img(row['item'], target_size=(IMG_WIDTH, IMG_HEIGHT))
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            images = np.vstack([x])
            preds = model.predict(images, batch_size=1)

            for j in range(len(preds)):
                probs = preds[j, 0:]
                sorted_inds = [y[0] for y in sorted(enumerate(-probs), key=lambda x:x[1])]
                item = row["item"]
                max_clasif_var = 1
                for k in range(max_clasif_var):
                    ind = sorted_inds[k]
                    results["item"].append(item)
                    results["label"].append(labelmap[ind])
                    results["score"].append(probs[ind])

def main():
    # Principals
    experiment = "all_WCS_and_iNat_imgs_3_1"
    base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020/"
    data_path = os.path.join(base_path, "data")
    experiments_path = os.path.join("results", "train_predict_classification_model")
    results_path = os.path.join(experiments_path, experiment)
    # Annotations provided by (or created from) JSON files of competition
    iNat_dataset_json_file = os.path.join(data_path, "metadata", "dataset_curated_iNat_images.json")
    WCS_dataset_json_file = os.path.join(data_path, "metadata", "dataset_all_WCS_dets_MegadetV4.json")
    # Folders
    WCS_images_dir = os.path.join(data_path, "train")
    rs_imagery_path = os.path.join(data_path, "iwildcam_rs")
    tfrecords_path = os.path.join(results_path, "tfrecords")
    train_dir = os.path.join(results_path, 'train')
    crops_of_train_dataset_path = os.path.join(experiments_path, "crops_of_train_dataset")
    # Datasets in CSV files
    dataset_path = os.path.join(results_path, "dataset.csv")
    test_dataset_crops_of_megadet_preds = os.path.join(results_path, f"aux_dataset_crops_of_megadet_preds.csv")
    labelmap_file = "/LUSTRE/users/ecoinf_admin/iwildcam2020/scripts/results/train_predict_classification_model/all_WCS_and_iNat_imgs_2_3/tfrecords/labels.txt"
    
    num_epochs=20
    train_perc=0.8

    os.makedirs(results_path, exist_ok=True)

    parser = argparse.ArgumentParser()
    parser.add_argument('--num_tasks', default=None, type=int)
    parser.add_argument('--task_num', default=None, type=int)
    args = parser.parse_args()

    crops_dataset = create_dataset_from_detection_crops(
        WCS_dataset_json_file=WCS_dataset_json_file,
        iNat_dataset_json_file=iNat_dataset_json_file,
        WCS_images_dir=WCS_images_dir,
        iNat_images_dir=data_path, 
        train_perc=train_perc,
        result_dataset_path=dataset_path,
        tfrecords_path=tfrecords_path,
        result_crops_path=crops_of_train_dataset_path)
    train_classif_model(
        classification_dataset=crops_dataset,
        train_dir=train_dir,
        train_images_dir="/LUSTRE/users/ecoinf_admin/iwildcam2020/scripts",
        rs_imagery_path=rs_imagery_path,
        num_epochs=num_epochs)
    
    eval_classif_model(
        dataset_crops_path=test_dataset_crops_of_megadet_preds,
        train_dataset=crops_dataset,
        train_images_dir="/LUSTRE/users/ecoinf_admin/iwildcam2020/scripts",
        train_dir=train_dir, 
        labelmap_file=labelmap_file)

if __name__ == "__main__":
    main()
