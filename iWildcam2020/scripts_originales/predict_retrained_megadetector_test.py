import os
import sys
import argparse
from conabio_ml.datasets.images import ImagePredictionDataset
from conabio_ml.datasets.images.datasets import ConabioImageDataset
from conabio_ml.trainer.images.model import ObjectDetectionModel
from conabio_ml.trainer.images.predictor_config import TFPredictorConfig

parser = argparse.ArgumentParser()
parser.add_argument('--num_tasks', default=None, type=int)
parser.add_argument('--task_num', default=None, type=int)
args = parser.parse_args()

num_images = '5K'
max_num_boxes = 100
num_classes = '5'
sufix_results = f'_{max_num_boxes}_boxes'

test_path = "/LUSTRE/users/ecoinf_admin/conabio_ml/examples/scripts/images/detection/results/eval_megadetector_coco/WCS_iwild2020/"
results_path = os.path.join(test_path, "experiments", f"{num_classes}_classes", f"{num_images}imgs")
dataset_path = os.path.join(results_path, f'dataset.csv')
train_dir = os.path.join(results_path, 'train')
tfrecords_path = os.path.join(results_path, "tfrecords")
images_dir = os.path.join(test_path, "train")
json_path = os.path.join(test_path, "metadata", "iwildcam2020_train_annotations.json")
model_path = os.path.join(train_dir, "inference_graph", "frozen_inference_graph.pb")
# model_path = os.path.join('/LUSTRE/users/ecoinf_admin/conabio_ml/examples/scripts/images/detection/files', 'megadetector_v3.pb')
# model_path = "/LUSTRE/users/ecoinf_admin/conabio_ml/examples/scripts/images/detection/results/test_conversion/training_faster_rcnn_resnet101_6classes_2/inference_model/frozen_inference_graph.pb"

if args.num_tasks and args.task_num:
    splitted_prediction_path = os.path.join(
        results_path, 
        f"splitted_prediction_datasets{sufix_results}")
    os.makedirs(splitted_prediction_path, exist_ok=True)
    prediction_dataset_path = os.path.join(
        splitted_prediction_path,
        f'prediction_dataset_{args.task_num}_of_{args.num_tasks}.csv')
else:
    prediction_dataset_path = os.path.join(
        results_path, f'prediction_dataset{sufix_results}.csv')

os.makedirs(results_path, exist_ok=True)

if not os.path.isfile(dataset_path):
    wcs_test = ConabioImageDataset.from_json(source_path=json_path,
                                             dest_path=results_path,
                                             images_dir=images_dir,
                                             copy_images=False,
                                             images_size = (800,600),
                                             tfrecords_path=tfrecords_path)
    wcs_test.to_csv(dest_path=dataset_path)
else:
    wcs_test = ConabioImageDataset.from_csv(source_path=dataset_path, tfrecords_path=tfrecords_path)

detector_model = ObjectDetectionModel.load_saved_model(source_path=model_path)
exec_config = TFPredictorConfig.create(batch_size=1,
                                       num_tasks=args.num_tasks,
                                       task_num=args.task_num)
predict_dataset = detector_model.predict(dataset=wcs_test,
                                             execution_config=exec_config,
                                             prediction_config={
                                                "max_number_of_boxes": 10,
                                                "dataset_partition": None # Para el dataset completo
                                             })
predict_dataset.to_csv(dest_path=prediction_dataset_path)
