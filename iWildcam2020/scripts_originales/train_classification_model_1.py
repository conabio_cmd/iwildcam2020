"""
Train a classification model (Inception v4) with all categories of the WCS 
dataset
"""
import os
from conabio_ml.datasets.images.datasets import ConabioImageDataset
from conabio_ml.trainer.images.trainer_config import TFSlimTrainConfig
from conabio_ml.trainer.images.model import ClassificationModel
from conabio_ml.trainer.images.trainer import ClassificationTrainer

data_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020/data"
results_path = os.path.join("results/train_classification_model/all_WCS_imgs/")
dataset_path = os.path.join(results_path, 'dataset.csv')
train_dir = os.path.join(results_path, 'train')
json_path = os.path.join(results_path, "dataset_all_dets.json")
images_dir = os.path.join(data_path, "train")
dest_images = os.path.join(results_path, "crops")
tfrecords_path = os.path.join(results_path, "tfrecords")
inference_graph_path = os.path.join('files', 'inception_v4.ckpt')

os.makedirs(tfrecords_path, exist_ok=True)
os.makedirs(train_dir, exist_ok=True)
os.makedirs(dest_images, exist_ok=True)

if not os.path.isfile(dataset_path):
    detection_dataset = ConabioImageDataset.from_json(
        source_path=json_path,
        images_dir=images_dir, 
        mapping_images_fields={
            ConabioImageDataset.IMAGE_FIELD_DATE_CAPTURED: "datetime"
        })
    classification_dataset = detection_dataset.create_classif_ds_from_det_crops(
        dest_path=dest_images)
    classification_dataset.split(train_perc=1., val_perc=0, test_perc=0)
    classification_dataset.to_csv(dest_path=dataset_path)
    classification_dataset.to_tfrecords(dest_path=tfrecords_path, num_shards=5)
else:
    classification_dataset = \
        ConabioImageDataset.from_csv(source_path=dataset_path,
                                     split_by_column="partition",
                                     tfrecords_path=tfrecords_path,
                                     info={
                                        # "collection": "iwildcam2020"
                                     })

trainer_config = TFSlimTrainConfig.create(clone_on_cpu=False, num_clones=2)
inception_v4_model = ClassificationModel.load_saved_model(
    source_path=inference_graph_path,
    model_config={
        "inception_v4": {
            "weight_decay": 0.00004
        }
    })
ClassificationTrainer.train(dataset=classification_dataset,
                            model=inception_v4_model,
                            execution_config=trainer_config,
                            train_config = {
                                "checkpoint_exclude_scopes": "InceptionV4/AuxLogits,InceptionV4/Logits",
                                "trainable_scopes": "InceptionV4/AuxLogits,InceptionV4/Logits",
                                "batch_size": 32,
                                "train_dir": train_dir,
                                "num_epochs": 15,
                                "optimizer": {
                                    'rms_prop': {
                                        "learning_rate": {
                                            'fixed': {
                                                "learning_rate": 0.01
                                            }
                                        }
                                    }
                                }
                            })