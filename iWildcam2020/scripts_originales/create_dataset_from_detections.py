import os
import json
from math import floor
import pandas as pd
from collections import defaultdict
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--detections_file', default='iwildcam2020_megadetector_results.json', type=str, help="Path of the detections JSON file.")
parser.add_argument('--annotations_file', default='iwildcam2020_train_annotations.json', type=str, help="Path of the annotations JSON file.")
parser.add_argument("--include_empty", default=False, action="store_true" , help="Whether to include empty images or not.")
parser.add_argument("--only_count_detections", default=False, action="store_true" , help="If only include the 'count' detections with the highest score.")
parser.add_argument('--images_dir', default='', type=str, help="Path where the images are and which will be prefixed in the CSV 'item' field.")
parser.add_argument('--csv_name', default='dataset.csv', type=str, help="Name of the generated CSV file.")
parser.add_argument('--json_name', default='dataset.json', type=str, help="Name of the generated JSON file.")
args = parser.parse_args()

detections_data = json.load(open(args.detections_file))
annotations_data = json.load(open(args.annotations_file))

images_to_ann = dict()
images_dict = dict()
categories_dict = dict()

for ann in annotations_data['annotations']:
    images_to_ann[ann['image_id']] = ann
for image in annotations_data['images']:
    images_dict[image['id']] = image
for category in annotations_data['categories']:
    categories_dict[category['id']] = category['name']

res_csv = []
anns_json = []
for image_det in detections_data['images']:
    image_id = image_det['id']
    if image_id not in images_to_ann: # iNat2017 or iNat2018
        continue
    image = images_dict[image_id]
    detections = image_det['detections']
    annotation = images_to_ann[image_id]
    category_id = annotation['category_id']
    count = annotation['count']
    if category_id == 0 and args.include_empty:
        item = os.path.join(args.images_dir, image['file_name'])
        res_csv.append({
            "item": item,
            "label": categories_dict[annotation['category_id']],
            "bbox": ""
        })
        anns_json.append({
            "id": annotation['id'],
            "image_id": image_id,
            "category_id": category_id
        })
    else:
        for i, detection in enumerate(detections):
            if i >= count and args.only_count_detections:
                break
            (x, y, w, h) = detection['bbox']
            (height, width) = image['height'], image['width']
            (x, y, w, h) = (floor(x*width) , floor(y*height), floor(w*width), floor(h*height))
            bbox = [x, y, w, h]
            bbox_str = ','.join([str(i) for i in bbox])
            item = os.path.join(args.images_dir, images_dict[image_id]['file_name'])
            res_csv.append({
                "item": item,
                "label": categories_dict[annotation['category_id']],
                "bbox": bbox_str
            })
            anns_json.append({
                "id": f"{annotation['id']}-{str(i).zfill(3)}",
                "image_id": image_id,
                "category_id": category_id,
                "bbox": bbox
            })

data = pd.DataFrame(res_csv)
data.to_csv(args.csv_name, index=False, header=True, columns=["item", "label", "bbox"])

categories = [{"id": k, "name": v} for k, v in categories_dict.items()]
json_data = {
    "info": annotations_data['info'],
    "images": annotations_data["images"],
    "categories": categories,
    "annotations": anns_json
}
with open(args.json_name, 'w',) as outfile:
    json.dump(json_data, outfile)