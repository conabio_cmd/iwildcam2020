
Bos taurus -> limpiar, tiene muchas de humanos

Mazama temama - 
    Localizada  en Centroamérica

Aepyceros melampus -
    Se localiza en: África Sudoriental
    Se confunde con:
        - acryllium vulturinum: salen juntos en fotos de train

Odocoileus virginianus - 

Cephalophus nigrifrons




Only a limited number of animals are grouped together. 266 species are taken in train set, but only 14 species were photographed, with more than 10 animals at a time. It turns out that there is a relationship between the animal species and the number of individuals reflected.　


# Landsat

- Banda 3 (rojo):
    - No refleja bien la vegetación sana
    - No refleja bien el agua
- Banda 2 (verde):
    - Refleja mejor el agua (puede ser usado para distinguir entre el agua clara y el agua turbia)
    - Refleja muy bien la vegetación
- Banda 1 (azul):
    - Penetra el agua clara muy bien (Es usada para detectar fondos oceánicos)
    - Puede distinguir suelos de la vegetación (es usado para distinguir árboles Caducifolios de árboles de coníferas)
- Banda 4 (infraroja):
    - Refleja muy bien la vegetación sana
- Combinaciones de bandas
    - Near infrared compositing:
        - Asignar: Banda 4 a roja, Banda 3 a verde y banda 2 a azul
        - La vegetación aparece con un brillo rojo ()
    - False color composite
        - Asignar: Banda 7 a roja, Banda 4 a verde y banda 2 a azul
        - La nieve y la roca de fondo se distinguen mejor. Los tipos de vegetación son más definidos.
        - Se usa para la detección de camuflaje, detección de cambios, suelos perturbados, tipo de suelo y estrés de la vegetación