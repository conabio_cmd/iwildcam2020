import os
import numpy as np
from glob import glob
import multiprocessing
import iwildcam_rs_utils as rs
import matplotlib.pyplot as plt
from PIL import Image
import cv2
    
def get_images_of_site(rs_path, out_dir):
    os.makedirs(out_dir, exist_ok=True)
    sites_dirs_pattern = "iwc_*"
    sites_dirs = glob(os.path.join(rs_path, sites_dirs_pattern))
    tuples = []
    for site_dir in sites_dirs:
        tuples.append((rs_path, out_dir, site_dir))
        # create_site_image(rs_path, out_dir, site_dir)
    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        pool.starmap(create_site_image, tuples)
    # create_site_image(rs_path, out_dir, "/LUSTRE/users/ecoinf_admin/iwildcam2020/data/iwildcam_rs_npy/iwc_115")

def create_site_image(rs_path, out_dir, site_dir):
    try:
        site_files = os.listdir(os.path.join(rs_path, site_dir))
        site_dates = sorted(np.unique([fname.split('_')[2] for fname in site_files]))
        basename_site = os.path.basename(site_dir)
        lower_pixels_cloud_shadow = None
        lower_site_date_cloud_shadow = None
        for site_date in site_dates:
            load_path = os.path.join(site_dir, f"{basename_site}_{site_date}")
            I_pixelqa = np.load(load_path+'_pixelqa.npy') # 200 x 200 x 1
            # Avoid cutted images
            I_multispectral = np.load(f'{load_path}_multispectral.npy') # 200 x 200 x 9
            data = rs.norm(I_multispectral[:,:,rs.multispectral_idx['infrared']])
            if np.sum(data == 0) / (np.sum(data == 0) + np.sum(data != 0)) > 0.1:
                continue
            I_pixelqa_parsed = np.zeros((I_pixelqa.shape[0], I_pixelqa.shape[1], len(rs.pixelqa_idx)))
            for r in range(I_pixelqa.shape[0]):
                for c in range(I_pixelqa.shape[1]):
                    I_pixelqa_parsed[r,c,:] = rs.parse_pixelqa(int(I_pixelqa[r,c]))
            cloud_pixels = I_pixelqa_parsed[:,:,rs.pixelqa_idx['cloud']]
            shadow_pixels = I_pixelqa_parsed[:,:,rs.pixelqa_idx['cloud_shadow']]
            num_cloud_shadow_pixels = np.sum((cloud_pixels+shadow_pixels) != 0)
            out_site = os.path.join(out_dir, basename_site)
            store_images_files(load_path, out_site, os.path.basename(load_path))
            if lower_pixels_cloud_shadow is None or num_cloud_shadow_pixels < lower_pixels_cloud_shadow:
                lower_pixels_cloud_shadow = num_cloud_shadow_pixels
                lower_site_date_cloud_shadow = load_path
        basename_site_date = os.path.basename(lower_site_date_cloud_shadow)
        basename_site = "_".join([basename_site_date.split("_")[0], basename_site_date.split("_")[1]])
        store_images_files(lower_site_date_cloud_shadow, out_dir, basename_site)

    except Exception as ex:
        print(f"Error while create file to {site_dir}: {ex}")

def store_images_files(load_path, out_dir, basename, I_pixelqa_parsed=None):
    os.makedirs(out_dir, exist_ok=True)
    I_multispectral = np.load(f'{load_path}_multispectral.npy') # 200 x 200 x 9

    # Store infrared image
    file_path = os.path.join(out_dir, f"{basename}.png")
    data = rs.norm(I_multispectral[:,:,rs.multispectral_idx['infrared']]).astype(np.uint8)
    data = cv2.equalizeHist(data)
    im_data = Image.fromarray(data)
    im_data = im_data.convert('RGB')
    im_data.save(file_path)

rs_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020/data/iwildcam_rs_npy/"
out_dir = "/LUSTRE/users/ecoinf_admin/iwildcam2020/data/iwildcam_rs/"
get_images_of_site(rs_path, out_dir)