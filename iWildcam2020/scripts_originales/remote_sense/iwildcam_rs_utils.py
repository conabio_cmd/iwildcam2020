import numpy as np
import functools
import warnings
import cv2

'''
map each multispectral channel name to a channel index
'''
multispectral_idx = {
    'ultrablue': 0,
    'blue': 1,
    'green': 2,
    'red': 3,
    'infrared': 4,
    'shortwave_infrared_1': 5,
    'shortwave_infrared_2': 6,
    'brightness_temp_1': 7,
    'brightness_temp_2': 8
}

'''
map each pixelqa bit name to a channel index
'''
pixelqa_idx = { 
    'fill': 0,
    'clear': 1,
    'water': 2,
    'cloud_shadow': 3,
    'snow': 4,
    'cloud': 5,
    'cloud_confidence': 6,
    'cirrus_confidence': 7,
    'terrain_occlusion': 8
}

'''
map each radsatqa bit name to a channel index
'''
radsatqa_idx = {
    'fill': 0,
    'ultrablue': 1,
    'blue': 2,
    'green': 3,
    'red': 4,
    'infrared': 5,
    'band_6_sat': 6,
    'band_7_sat': 7,
    'band_9_sat': 9,
    'band_10_sat': 10,
    'band_11_sat': 11
}

def parse_pixelqa(pixel_value):
    
    '''
    convert pixel value to bit string
    '''
    bit_string = '{0:011b}'.format(pixel_value)
    
    '''
    convert binary substrings to integers
    '''
    parsed_pixel = np.array([
        int(bit_string[10]),
        int(bit_string[9]),
        int(bit_string[8]),
        int(bit_string[7]),
        int(bit_string[6]),
        int(bit_string[5]),
        int(bit_string[3] + bit_string[4],base=2),
        int(bit_string[1] + bit_string[2],base=2),
        int(bit_string[0])
        ])
    
    return parsed_pixel

def parse_radsatqa(pixel_value):
    
    '''
    convert pixel value to bit string
    '''
    bit_string = '{0:012b}'.format(pixel_value)
    
    '''
    convert binary substrings to integers
    '''
    parsed_pixel = np.array([
        int(bit_string[11]),
        int(bit_string[10]),
        int(bit_string[9]),
        int(bit_string[8]),
        int(bit_string[7]),
        int(bit_string[6]),
        int(bit_string[5]),
        int(bit_string[4]),
        int(bit_string[2]),
        int(bit_string[1]),
        int(bit_string[0])
    ])
    
    return parsed_pixel
    

def norm(band):
    band_min, band_max = band.min(), band.max()
    return 255 * ((band - band_min)/(band_max - band_min + 0.01))

def _stretch_im(arr, str_clip=2):
    s_min = str_clip
    s_max = 100 - str_clip
    arr_rescaled = np.zeros_like(arr)
    for ii, band in enumerate(arr):
        lower, upper = np.percentile(band, (s_min, s_max))
        arr_rescaled[ii] = rescale_intensity(
            band, in_range=(lower, upper)
        )
    return arr_rescaled.copy()

_integer_types = (np.byte, np.ubyte,          # 8 bits
                  np.short, np.ushort,        # 16 bits
                  np.intc, np.uintc,          # 16 or 32 or 64 bits
                  np.int_, np.uint,           # 32 or 64 bits
                  np.longlong, np.ulonglong)  # 64 bits
_integer_ranges = {t: (np.iinfo(t).min, np.iinfo(t).max)
                   for t in _integer_types}
dtype_range = {np.bool_: (False, True),
               np.bool8: (False, True),
               np.float16: (-1, 1),
               np.float32: (-1, 1),
               np.float64: (-1, 1)}
dtype_range.update(_integer_ranges)
DTYPE_RANGE = dtype_range.copy()
DTYPE_RANGE.update((d.__name__, limits) for d, limits in dtype_range.items())
DTYPE_RANGE.update({'uint10': (0, 2 ** 10 - 1),
                    'uint12': (0, 2 ** 12 - 1),
                    'uint14': (0, 2 ** 14 - 1),
                    'bool': dtype_range[np.bool_],
                    'float': dtype_range[np.float64]})

warn = functools.partial(warnings.warn, stacklevel=2)

def rescale_intensity(image, in_range='image', out_range='dtype'):
    if out_range in ['dtype', 'image']:
        out_dtype = _output_dtype(image.dtype.type)
    else:
        out_dtype = _output_dtype(out_range)

    imin, imax = map(float, intensity_range(image, in_range))
    omin, omax = map(float, intensity_range(image, out_range,
                                            clip_negative=(imin >= 0)))

    if np.any(np.isnan([imin, imax, omin, omax])):
        warn(
            "One or more intensity levels are NaN. Rescaling will broadcast "
            "NaN to the full image. Provide intensity levels yourself to "
            "avoid this. E.g. with np.nanmin(image), np.nanmax(image).",
            stacklevel=2
        )

    image = np.clip(image, imin, imax)

    if imin != imax:
        image = (image - imin) / (imax - imin)
        return np.asarray(image * (omax - omin) + omin, dtype=out_dtype)
    else:
        return np.clip(image, omin, omax).astype(out_dtype)

def intensity_range(image, range_values='image', clip_negative=False):
    if range_values == 'dtype':
        range_values = image.dtype.type

    if range_values == 'image':
        i_min = np.min(image)
        i_max = np.max(image)
    elif range_values in DTYPE_RANGE:
        i_min, i_max = DTYPE_RANGE[range_values]
        if clip_negative:
            i_min = 0
    else:
        i_min, i_max = range_values
    return i_min, i_max

def _output_dtype(dtype_or_range):
    if type(dtype_or_range) in [list, tuple, np.ndarray]:
        # pair of values: always return float.
        return np.float_
    if type(dtype_or_range) == type:
        # already a type: return it
        return dtype_or_range
    if dtype_or_range in DTYPE_RANGE:
        # string key in DTYPE_RANGE dictionary
        try:
            # if it's a canonical numpy dtype, convert
            return np.dtype(dtype_or_range).type
        except TypeError:  # uint10, uint12, uint14
            # otherwise, return uint16
            return np.uint16
    else:
        raise ValueError(
            'Incorrect value for out_range, should be a valid image data '
            f'type or a pair of values, got {dtype_or_range}.'
        )

# def equalization(r, g, b):
    # equ_b = cv2.equalizeHist(b)
    # equ_g = cv2.equalizeHist(g)
    # equ_r = cv2.equalizeHist(r)
    # equ = cv2.merge((equ_b, equ_g, equ_r))
    #cv2.imwrite('output_name.png', equ)
    # return img_out