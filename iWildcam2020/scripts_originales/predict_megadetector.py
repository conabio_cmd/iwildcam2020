import os
import argparse
from conabio_ml.datasets.images import ImagePredictionDataset
from conabio_ml.datasets.images.datasets import ConabioImageDataset
from conabio_ml.trainer.images.model import ObjectDetectionModel
from conabio_ml.trainer.images.predictor_config import TFPredictorConfig

parser = argparse.ArgumentParser()
parser.add_argument('--num_tasks', default=None, type=int, help='The total number of tasks to make the prediction.')
parser.add_argument('--task_num', default=None, type=int, help='1-based number of the task to make the prediction.')
args = parser.parse_args()

base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020/"
data_path = os.path.join(base_path, "data")    
results_path = os.path.join("results", "predict_megadetector", "all_WCS_imgs")
dataset_path = os.path.join(results_path, f'dataset.csv')
images_dir = os.path.join(data_path, "test")
json_path = os.path.join(data_path, "metadata", "iwildcam2020_test_information.json")
megadetector_path = os.path.join(base_path, "scripts", "files", "megadetector_v3.pb")

if args.num_tasks and args.task_num:
    splitted_prediction_path = os.path.join(
        results_path, 
        "splitted_prediction_datasets")
    os.makedirs(splitted_prediction_path, exist_ok=True)
    prediction_dataset_path = os.path.join(
        splitted_prediction_path,
        f'prediction_dataset_{args.task_num}_of_{args.num_tasks}.csv')
else:
    prediction_dataset_path = os.path.join(results_path, f'prediction_dataset.csv')

os.makedirs(results_path, exist_ok=True)

labelmap = { # Para mapear las categorías del Megadetector
    1: ConabioImageDataset.ANIMAL_LABEL, 
    2: ConabioImageDataset.PERSON_LABEL
}
wcs_test = ConabioImageDataset.from_json(source_path=json_path,
                                             dest_path=results_path,
                                             images_dir=images_dir,
                                             labelmap=labelmap)
    
if not os.path.isfile(dataset_path):
    wcs_test = ConabioImageDataset.from_json(source_path=json_path,
                                             dest_path=results_path,
                                             images_dir=images_dir,
                                             labelmap=labelmap)
    wcs_test.to_csv(dest_path=dataset_path)
else:
    wcs_test = ConabioImageDataset.from_csv(source_path=dataset_path,
                                            labelmap=labelmap)
megadetector_model = ObjectDetectionModel.load_saved_model(source_path=megadetector_path)
exec_config = TFPredictorConfig.create(batch_size=1,
                                        num_tasks=args.num_tasks,
                                        task_num=args.task_num)
predict_dataset = megadetector_model.predict(dataset=wcs_test,
                                             execution_config=exec_config,
                                             prediction_config={
                                                "max_number_of_boxes": 10,
                                                "dataset_partition": None # Para el dataset completo
                                             })
predict_dataset.to_csv(dest_path=prediction_dataset_path)