#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Train a simple classification model (Inception v3) with all categories of the WCS and 
including iNaturalit 2017 and 2018 images to the dataset
"""
import os
import sys
import json
from glob import glob
import uuid
import argparse
from conabio_ml.datasets.images.datasets import ConabioImageDataset, ImagePredictionDataset, ImageDataset
from conabio_ml.trainer.images.trainer_config import TFSlimTrainConfig
from conabio_ml.trainer.images.predictor_config import TFPredictorConfig
from conabio_ml.trainer.images.model import ClassificationModel, ObjectDetectionModel
from conabio_ml.evaluator.images.evaluator import ImageClassificationEvaluator,\
    ImageClassificationMetricsSets
from conabio_ml.trainer.images.trainer import ClassificationTrainer
from conabio_ml.datasets.dataset import Partitions
from conabio_ml.utils.evaluator_utils import store_eval_results, load_eval_results, \
    store_classification_results
import conabio_ml.utils.dataset_utils as dataset_utils
from utils import create_WCS_dataset_from_given_detections, \
    create_WCS_dataset_from_Megadetector_v4_detections, create_iNat_dataset_from_detections
from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)

CLONE_ON_CPU = False
NUM_GPUS_PER_NODE = 2

def create_dataset_from_train_anns(json_file,
                                    images_dir,
                                    dest_images_path,
                                    train_perc,
                                    result_dataset_path,
                                    tfrecords_path):
    dataset_columns = ["item", "label"]
    if os.path.isfile(result_dataset_path):
        classification_dataset = \
            ConabioImageDataset.from_csv(
                source_path=result_dataset_path,
                split_by_column="partition",
                columns=dataset_columns,
                tfrecords_path=tfrecords_path,
                exclude_corrupted_images=True)
        return classification_dataset
    
    classification_dataset = ConabioImageDataset.from_json(
        source_path=json_file,
        images_size=(800, 600),
        copy_images=True,
        dest_path=dest_images_path,
        images_dir=images_dir,
        exclude_corrupted_images=True)
    classification_dataset.split(
        train_perc=train_perc, 
        val_perc=0, 
        test_perc=1.-train_perc,
        group_by_field="location")

    os.environ['CUDA_VISIBLE_DEVICES'] = ""
    classification_dataset.to_tfrecords(dest_path=tfrecords_path, num_shards=5)
    classification_dataset.to_csv(dest_path=result_dataset_path, columns=dataset_columns)

    return classification_dataset

def train_classification_model_on_crops(pretrained_model_path,
                                        classification_dataset,
                                        train_dir,
                                        num_epochs,
                                        inception_v4_config):
    # Set env variable to run training in all the GPUs of the node
    os.environ['CUDA_VISIBLE_DEVICES'] = \
        f"{','.join([str(x) for x in range(NUM_GPUS_PER_NODE)])}"
    os.makedirs(train_dir, exist_ok=True)
    trainer_config = TFSlimTrainConfig.create(
        clone_on_cpu=CLONE_ON_CPU, num_clones=NUM_GPUS_PER_NODE)
    inception_v4_model = ClassificationModel.load_saved_model(
        source_path=pretrained_model_path,
        model_config={
            "inception_v4": inception_v4_config
        })
    ClassificationTrainer.train(
        dataset=classification_dataset,
        model=inception_v4_model,
        execution_config=trainer_config,
        train_config = {
            "checkpoint_exclude_scopes": \
                "InceptionV4/AuxLogits,InceptionV4/Logits",
            "trainable_scopes": \
                "InceptionV4/AuxLogits,InceptionV4/Logits",
            "batch_size": 32,
            "train_dir": train_dir,
            "num_epochs": num_epochs,
            "optimizer": {
                'rms_prop': {
                    "learning_rate": {
                        'fixed': {
                            "learning_rate": 0.01
                        }
                    }
                }
            }
        })
    return inception_v4_model

def eval_model(dataset_true, dataset_pred, eval_results_file):
    if os.path.isfile(eval_results_file):
        return load_eval_results(eval_results_file)
    classification_results = ImageClassificationEvaluator.eval(
        dataset_true=dataset_true, 
        dataset_pred=dataset_pred, 
        eval_config={
            "metrics_set": {
                ImageClassificationMetricsSets.MULTICLASS: {
                    "average": "micro"
                }
            },
            "dataset_partition": Partitions.TEST
        }
    )
    store_eval_results(classification_results, dest_path=eval_results_file)
    return classification_results

def train_and_eval_classif_model(classification_dataset,
                                 pretrained_model_path,
                                 train_dir,
                                 num_epochs,
                                 prediction_dataset_path,
                                 labelmap_filename_dir,
                                 eval_results_file,
                                 inception_v4_config, 
                                 all_classifications_path=None,
                                 wrong_classifications_path=None,
                                 correct_classifications_path=None):
    if os.path.isfile(prediction_dataset_path):
        pred_dataset_classif_on_crops = ImagePredictionDataset.from_csv(
            source_path=prediction_dataset_path,
            labelmap=labelmap_filename_dir)
    else:
        # Train Inception v4 model on classification_dataset
        inception_v4_model = train_classification_model_on_crops(
            pretrained_model_path=pretrained_model_path, 
            classification_dataset=classification_dataset,
            train_dir=train_dir,
            num_epochs=num_epochs,
            inception_v4_config=inception_v4_config)
        tf_predictor_config = TFPredictorConfig.create(
            batch_size=32, 
            num_preprocessing_threads=8)
        pred_dataset_classif_on_crops = inception_v4_model.predict(
            dataset=classification_dataset,
            execution_config=tf_predictor_config,
            prediction_config={
                "dataset_partition": Partitions.TEST,
                "labelmap": labelmap_filename_dir
            })
        pred_dataset_classif_on_crops.to_csv(dest_path=prediction_dataset_path)

    classification_results = eval_model(
        dataset_true=classification_dataset, 
        dataset_pred=pred_dataset_classif_on_crops, 
        eval_results_file=eval_results_file)

    if all_classifications_path is not None or \
            wrong_classifications_path is not None or \
            correct_classifications_path is not None:
        store_classification_results(
            dataset_true=classification_dataset, 
            dataset_pred=pred_dataset_classif_on_crops, 
            dataset_partition=Partitions.TEST,
            all_classifications_path=all_classifications_path,
            wrong_classifications_path=wrong_classifications_path,
            correct_classifications_path=correct_classifications_path)

def classify_test_set(test_dataset_json_path,
                      test_images_dir,
                      classification_model_train_dir,
                      labelmap_file,
                      preds_test_dataset_path,
                      inception_v4_config):

    if os.path.isfile(preds_test_dataset_path):
        pred_dataset_classif_on_crops = ImagePredictionDataset.from_csv(
            source_path=preds_test_dataset_path,
            labelmap=labelmap_file,
            exclude_corrupted_images=True)
    else:
        test_dataset = ConabioImageDataset.from_json(
            source_path=test_dataset_json_path,
            images_dir=test_images_dir,
            exclude_corrupted_images=True)
        tf_predictor_config = \
            TFPredictorConfig.create(
                batch_size=32, 
                num_preprocessing_threads=8)
        inception_v4 = ClassificationModel.load_saved_model(
            source_path=classification_model_train_dir,
            model_config={
                "inception_v4": inception_v4_config
            })
        os.environ['CUDA_VISIBLE_DEVICES'] = "0"
        pred_dataset_classif_on_crops = \
            inception_v4.predict(dataset=test_dataset,
                                execution_config=tf_predictor_config,
                                prediction_config={
                                    "dataset_partition": None,
                                    "labelmap": labelmap_file
                                })
        pred_dataset_classif_on_crops.to_csv(dest_path=preds_test_dataset_path)
    return pred_dataset_classif_on_crops

def generate_multiclass_dataset_for_competition(classif_multiclass_dataset,
                                                WCS_image_level_anns_json_file,
                                                final_csv,
                                                sample_submission_path,
                                                images_results_path):
    # classif_multiclass_dataset.to_folder(dest_path=images_results_path,
    #                                      split_in_labels=True,
    #                                      keep_originals=True)
    # Generate the final CSV file to send to the Kaggle competition
    dataset_utils.create_file_to_kaggle_competition(
        classif_multiclass_dataset,
        coco_json_file=WCS_image_level_anns_json_file,
        dest_path=final_csv,
        sample_submission=sample_submission_path
    )

def main():
    # Principals
    experiment = "all_WCS_imgs_1"
    base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020/"
    data_path = os.path.join(base_path, "data")    
    experiments_path = os.path.join("results", "train_predict_simple_classification_model")
    results_path = os.path.join(experiments_path, experiment)
    # Annotations provided by (or created from) JSON files of competition
    WCS_image_level_anns_json_file = os.path.join(data_path, "metadata", "iwildcam2020_train_annotations.json")
    WCS_test_json_file = os.path.join(data_path, "metadata", "iwildcam2020_test_information.json")
    # Folders
    WCS_train_images_dir = os.path.join(data_path, "train")
    WCS_test_images_dir = os.path.join(data_path, "test")
    WCS_images_copy_dir = os.path.join(results_path, "data")
    tfrecords_path = os.path.join(results_path, "tfrecords")
    train_dir = os.path.join(results_path, 'train')
    classifications_test_set_path = os.path.join(results_path, 'classifications_test_set')
    # Datasets in CSV files
    dataset_path = os.path.join(results_path, "dataset.csv")
    dataset_test_set = os.path.join(data_path, "metadata", "dataset_test_set.csv")
    dataset_classif_multiclass_path = os.path.join(results_path, f"dataset_classif_multiclass.csv")
    classifs_on_test_part = os.path.join(results_path, f"classifs_on_test_part.csv")
    # Pretrained models
    pretrained_inception_v4_path = os.path.join(base_path, "scripts", "files", 'inception_v4.ckpt')
    # Other files
    eval_results_on_test_partition = os.path.join(results_path, f"evaluation_on_test_part_results.json")
    labelmap_file = os.path.join(tfrecords_path, "labels.txt")
    final_csv = os.path.join(results_path, 'submission_5.csv')
    sample_submission_path = os.path.join(data_path, "metadata", "sample_submission.csv")
    num_epochs=32
    train_perc=0.8
    inception_v4_config = {
        "weight_decay": 0.00004
    }

    os.makedirs(results_path, exist_ok=True)

    wcs_dataset = create_dataset_from_train_anns(
        json_file=WCS_image_level_anns_json_file,
        images_dir=WCS_train_images_dir,
        dest_images_path=WCS_images_copy_dir,
        train_perc=train_perc,
        result_dataset_path=dataset_path,
        tfrecords_path=tfrecords_path)
    train_and_eval_classif_model(
        classification_dataset=wcs_dataset,
        pretrained_model_path=pretrained_inception_v4_path,
        train_dir=train_dir,
        num_epochs=num_epochs,
        prediction_dataset_path=classifs_on_test_part,
        labelmap_filename_dir=tfrecords_path,
        eval_results_file=eval_results_on_test_partition,
        inception_v4_config=inception_v4_config,
        all_classifications_path=None,
        wrong_classifications_path=None,
        correct_classifications_path=None)
    pred_dataset_classif_on_test_set = classify_test_set(
        test_dataset_json_path=WCS_test_json_file,
        test_images_dir=WCS_test_images_dir,
        classification_model_train_dir=train_dir,
        labelmap_file=labelmap_file,
        preds_test_dataset_path=dataset_classif_multiclass_path,
        inception_v4_config=inception_v4_config)
    generate_multiclass_dataset_for_competition(
        classif_multiclass_dataset=pred_dataset_classif_on_test_set,
        WCS_image_level_anns_json_file=WCS_image_level_anns_json_file,
        final_csv=final_csv,
        sample_submission_path=sample_submission_path,
        images_results_path=classifications_test_set_path)

if __name__ == "__main__":
    main()