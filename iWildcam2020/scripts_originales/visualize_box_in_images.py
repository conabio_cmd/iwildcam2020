import json
from conabio_ml.datasets.images.datasets import COCOImageDataset
from conabio_ml.utils.evaluator_utils import visualize_ground_truth_boxes_in_dataset
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--json_path', required=True, type=str, help="Path of the detections JSON file.")
parser.add_argument('--images_dir', required=True, type=str, help="Path to the images folder.")
parser.add_argument('--path_to_save', required=True, type=str, help="Path where images will be stored.")
args = parser.parse_args()

dataset_json = COCOImageDataset.from_json(source_path=args.json_path,
                                          dest_path="results",
                                          copy_images=True,
                                          images_size=(800,600),
                                          images_dir=args.images_dir)
# visualize_ground_truth_boxes_in_dataset(
#     dataset_json, 
#     path_to_save=args.path_to_save, 
#     split_in_categories = True, 
#     box_visualization_color='Chartreuse')
