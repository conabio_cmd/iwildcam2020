apt-get update -y
apt-get install -y git wget

pip install --upgrade pip
pip install tqdm Cython contextlib2 pillow lxml jupyter matplotlib

cd /lib

git clone -b dev https://bitbucket.org/conabio_cmd/conabio_ml.git
cd conabio_ml
pip install -r requirements.txt

cd ..

git clone https://github.com/cocodataset/cocoapi.git
cd cocoapi/PythonAPI
make
cp -r pycocotools /lib/conabio_ml/conabio_ml/trainer/images/models/research/
cd ../..

export PYTHONPATH=$PYTHONPATH:`pwd`/conabio_ml
export PYTHONPATH=$PYTHONPATH:`pwd`/conabio_ml/conabio_ml/trainer/images/models/research:`pwd`/conabio_ml/conabio_ml/trainer/images/models/research/slim

cd /lib/conabio_ml/conabio_ml/trainer/images/models/research/

python object_detection/builders/model_builder_test.py
