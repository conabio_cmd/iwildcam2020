# iWildCam2020 Repository

## What is this repository for? ###

* This repository contains notebooks, scripts and documentation for the iWildCam 2020 competition on Kaggle

## How do I get set up? ###
```
git clone git@bitbucket.org:conabio_cmd/iwildcam2020.git
cd iwildcam2020/docker_image
docker image build -t iwildcam2020:1.0 .
docker run -it --gpus 2 --name iwildcam2020 -p 3332:3332 iwildcam2020:1.0 bash
```
## Who do I talk to? ###

* Raúl Sierra Alcocer raul.sierra@conabio.gob.mx
* Juan M. Barrios jbarrios@conabio.gob.mx
* Ramón Rivera Camacho rrivera@conabio.gob.mx
* Juan Carlos López Enriquez jlopez@conabio.gob.mx
