# Imágenes Docker para ejecutar con el repo conabio_ml_vision

## Para ejecutar la imagen TF1_OD
**Construir la imagen**:
```
docker image build -t jlopez/conabio_ml_vision:tf_1 .
```
**Ejecutar el contenedor**:
```
docker run -it --gpus 2 --name API_VISION_TF_1 -p 3330:3330 -v /LUSTRE/Ecoinformatica/jlopez:/LUSTRE/Ecoinformatica/jlopez/ -v /LUSTRE/users/ecoinf_admin/:/LUSTRE/users/ecoinf_admin/ -e ADD_TO_PYTHONPATH="/LUSTRE/users/ecoinf_admin/iwildcam2020" jlopez/conabio_ml_vision:tf_1 bash
```
## Para ejecutar la imagen TF2
**Construir la imagen**:
```
docker image build -t jlopez/conabio_ml_vision:tf_2 . --build-arg TF_VERSION=2.4.1-gpu-jupyter
```

**Ejecutar el contenedor**:
```
docker run -it --gpus 2 --name API_VISION_TF_2 -p 3331:3331 -v /LUSTRE/Ecoinformatica/jlopez:/LUSTRE/Ecoinformatica/jlopez/ -v /LUSTRE/users/ecoinf_admin/:/LUSTRE/users/ecoinf_admin/ -e ADD_TO_PYTHONPATH="/LUSTRE/users/ecoinf_admin/iwildcam2020" -e PIP_INSTALL="tqdm keras opencv-python opencv-contrib-python" -e APT_INSTALL="ffmpeg libsm6 libxext6" jlopez/conabio_ml_vision:tf_2 bash
```
