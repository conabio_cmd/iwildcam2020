
Utilicé las imágenes de la colección WCS (train) y las de las colecciones iNat 2017, 2018 y 2021 con categorías comunes, para formar un conjunto de entrenamiento/validación (80/20) agrupando las imágenes de WCS por el campo 'location'.

Apliqué las técnicas de preprocesamiento CLAHE y White Balance sobre las imágenes del conjunto de entrenamiento/validación y sobre las de prueba del conjunto WCS, para mejorar las imágenes nocturnas.

Apliqué el Megadetector V4 a las imágenes del conjunto de entrenamiento/validación que tuvieran etiqueta de las especies de interés, y tomé las detecciones con score ≥ 0.3 para el conjunto WCS y las detecciones con score ≥ 0.95 para las de iNat. Para asignar las etiquetas a nivel de objeto tomé las etiquetas a nivel de imagen de estos conjuntos.
También apliqué el Megadetector V4 sobre las imágenes etiquetadas como vacías del conjunto WCS e incluí las detecciones con score ≥ 0.1 como imágenes de la clase 'empty' al conjunto de entrenamiento/validación, esto para ayudar a descartar falsos positivos del Megadetector V4 sobre el conjunto de prueba.
Todas las detecciones fueron ajustadas para que los bboxes fueran siempre cuadrados, tomando el lado mayor del bbox como el lado del cuadrado.

Realicé el entrenamiento por 20 épocas en diferentes resoluciones de imagen de tres modelos pre-entrenados con Imagenet: ResNet152 (224 px), EfficientNetB3 (300 px) y EfficientNetB7 (600 px).
Durante el entrenamiento se realizó aumento de datos sencillo aplicando las operaciones random rotation, random translation, random horizontal flip y random contrast.
Durante la inferencia sobre el conjunto de prueba realicé un ensamble de los tres modelos, asignándoles un peso en función del accuracy total que resultó de la evaluación de cada uno sobre el conjunto de validación.

Para ayudar a encontrar más falsos positivos del Megadetector sobre el conjunto de prueba utilicé la técnica de promedio acumulado (Accumulated averaging) para encontrar aquellas regiones en las imágenes donde se detectó un animal pero que no presentan cambio aparente (movimiento). Así, si el score de la detección < 0.9 y en la región del bbox de la detección no se detectó movimiento a través de los frames de la secuencia, la detección es descartada.

Utilicé la representación en funciones seno y coseno de la información de localización y momento del año de las imágenes de los conjuntos WCS y iNat para entrenar un modelo geo prior, que utilicé como información a priori complementaria al momento de realizar la inferencia sobre el conjunto de prueba.

Apliqué el Megadetector V4 sobre las imágenes del conjunto de prueba y tomé las detecciones con score ≥ 0.3 para determinar la especie y el número de individuos presentes en cada secuencia, de la siguiente manera:
- La especie se determina a partir de la clasificación final del ensamble de modelos (usando también el modelo geo prior) sobre las detecciones válidas*, tomando como 'especie de la secuencia' aquella que más se repite (moda).
- El número de individuos se calcula simplemente tomando el número máximo de detecciones válidas* presentes en alguna de las imágenes de la secuencia.

Así, para cada secuencia se asigna el número de individuos detectados solo para la columna de la 'especie de la secuencia', y a las demás columnas se les asigna 0. Para las secuencias que no tengan ninguna detección válida* se asignan todas las columnas a 0.

* Las detecciones válidas son aquellas que no fueron descartadas por el método de detección de movimiento, que no fueron clasificadas como 'empty' por el ensamble de modelos y que tengan una probabilidad de clasificación ≥ 0.5.

Como una última prueba (que envié como un late submission) usé las imágenes enteras del conjunto WCS para entrenar otros dos modelos: EfficientNetB3 (res 300 px) y EfficientNetB7 (res 600 px), e hice un ensamble ponderado de ambos. A este lo llamaré 'modelo complete'.
A su vez, ensamblé los modelos EfficientNetB3 y EfficientNetB7 entrenados con los crops de los bboxes (como se describió anteriormente). A este lo llamaré 'modelo crops'.
Finalmente ensamblé el 'modelo complete' y el 'modelo crops', asignando un peso de 0.3 al primero y 0.7 al segundo. Este modelo superó los scores público y privado del modelo anterior, pero al ser un late submission no se reflejó en el LB.