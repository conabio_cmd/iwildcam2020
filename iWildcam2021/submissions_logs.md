
# Log de las diferentes entregas (submission) que fueron enviados a la competición

## submission_1
### Descripción
se utilizan los recuadros de las detecciones del Megadetector V3 sobre las imágenes del conjunto WCS (dadas por la competición) para entrenar un modelo de clasificación **Vgg16** (la última parte de clasificación y las últimas N_TRAINABLE_LAYERS capas convolucionales). Sobre el conjunto de prueba se ejecuta la inferencia del Megadetector V4 para obtener los recuadros de cada imagen y se ejecuta la inferencia del modelo entrenado para clasificar cada recuadro con las categorías requeridas por la competición. Después, con las clasificaciones para cada recuadro del conjunto de prueba se aplica la siguiente regla para calcular el número de individuos de cada categoría en cada secuencia: Para cada secuencia se itera sobre cada categoría, y se toma el **número máximo de recuadros de esa categoría que aparezcan en cualquiera de las imágenes de la secuencia**.
### Resultados
Error de 0.0518925

## submission_2
### Descripción
Se entrena un modelo **EfficientNetB3** y en las clasificaciones finales sobre los crops del conjunto de test se toma **la etiqueta más frecuente (moda) como la especie de toda la secuencia**.
### Resultados
Error 0.0446395

## submission_3
### Descripción
Se usaron imágenes preprocesadas con las ténicas **CLAHE** y **SimpleWB** solo en el conjunto **test**.
### Resultados
Error 0.0450533

## submission_4
### Descripción
Se **re-entrenó y evaluó** el modelo usando imágenes preprocesadas con las ténicas **CLAHE** y **SimpleWB**.
### Resultados
Mejoró de 0.0446395 a 0.0413428 (8%).

## submission_5
### Descripción
Para esta entrega utilicé los recuadros generados por el Megadetector sobre las imágenes con etiqueta **vacía** en el dataset de entrenamiento de WCS e incluí los recuadros de esta categoría para entrenar el mismo modelo de submission_4 y descartar los falsos positivos del Megadetector sobre el conjunto `test`.
### Resultados
El score público fue de 0.0399994 (3.4% menos que submission_4)

## submission_6
### Descripción
Para esta entrega utilicé la técnica de promedio acumulado **Accumulated averaging** en las secuencias de imágenes para descartar los falsos positivos del Megadetector. Esto se hace de la siguiente manera: Para cada detección del Megadetector, se hace la suma acumulada de los pixeles de la región del bbox de esa detección para todas las imágenes de la secuencia, y al final esta suma acumulada, que representa un modelo de esa región de las imágenes de la secuencia, se compara con el recuadro de la detección a través de la diferencia absoluta y se aplica una operación de umbralado (threshold) a estos valores. A este resultado se aplican operaciones morfológicas para finalmente aplicar una función que busca contornos, y en caso de no haber ningún contorno en el resultado, se asume que en esta región de la secuencia no hay movimiento y por lo tanto la detección original se descarta.
### Resultados
El error de esta entrega fue de 0.0371631 usando un threshold de 30 (7.6% menos).

## submission_7
### Descripción
Igual que submission_6 pero se usó un *umbral de 40* para la técnica *Accumulated averaging*.
### Resultados
Se obtuvo un **error de 0.0368224** (1% menos)
En total, entre submission_4 y submission_7 se redujo el error en un 21%

## submission_8
### Descripción
Se cambió un poco la forma de calcular el 'modelo' de fondo para detectar movimiento. En el for que itera sobre todos los crops de la región de la detección en todas las imágenes de la secuencia, se salta repetir el proceso sobre la primera ([1:])
### Resultados
El error de esta entrega no mejoró respecto a la anterior: 0.0368612

## submission_9
### Descripción
Para esta entrega se tratan de eliminar también los **falsos positivos del Megadetector en el conjunto de entrenamiento**. Para ello se filtran las detecciones sobre este conjunto en las que no se encontró movimiento en esa región en todas las fotos de la secuencia; pero además, se filtran las detecciones (de entrenamiento y prueba) solo si su **score ≤ 0.9**, para no quitar aquellas con mayor probabilidad de ser correctas.
### Resultados
Esta entrega tampoco superó a submission_7 en cuanto a error: 0.0385581

## submission_10
### Descripción
Igual que submission_9, pero también se descartan las **clasificaciones con probabilidad ≤ 0.5** para que se tienda a dejar como vacías secuencias cuya clasificación no es muy confiable. 
### Resultados
Esta entrega tampoco superó a submission_7 en cuanto a error: 0.0383024

## submission_11
### Descripción
Igual que submission_10 pero se incluyen las clases comunes de los conjuntos **iNaturalist 2017 y 2018** al conjunto de entrenamiento.
### Resultados
Mejoró el error de submission_10: 0.0371250 (3%)

## submission_12
### Descripción
Se agrega la colección **iNaturalist 2021** al conjunto de entrenamiento, tomando solo las detecciones con `score ≥ 0.95`
### Resultados
El error fue de 0.0389220

## submission_13
### Descripción
Para esta entrega se vuelven a generar los recuadros de las detecciones sobre las imágenes con **etiqueta 'empty'** en el conjunto de entrenamiento, pero esta vez se toman las detecciones con `score ≥ 0.1` para así tener más recuadros de esa clase y tratar de mejorar su rendimiento. También se toman únicamente las **detecciones de la clase `Animalia` del conjunto test, descartando las demás**. 
### Resultados
El error fue de 0.0394702

## submission_14
### Descripción
Se agregan operaciones de **aumento de datos** en el pipeline del entrenamiento: RandomRotation, RandomTranslation, RandomFlip, RandomContrast
### Resultados
El error fue de 0.0384403

## submission_15
### Descripción
Se generan imágenes nocturnas simuladas a partir de las imágenes diurnas, convirtiendo a blanco y negro aquella que cumplan 8 ≤ hora del dia ≤ 17
### Resultados
El error fue de 0.0392061

## submission_16
### Descripción
A partir de submission submission_12 (iNat 17, 18 y 21) se aplicó el modelo **geo prior**
### Resultados
El error fue de 0.0355381
Siendo una **mejora del 9%**

## submission_17
### Descripción
A partir de submission submission_11 (iNat 17 y 18) se aplicó el modelo **geo prior**
### Resultados
El error fue de 0.0356494

## submission_18
### Descripción
Se volvió a entrenar el modelo geo prior quitando las imágenes de algunas clases: ave desconocida, empty, end, misfire, motorcycle, start, unknown bat, unknown bird, unknown dove, unknown raptor, unknown rat.
### Resultados
El error fue de 0.0353385

## submission_18 2
### Descripción
Se utiliza el 100% de las imágenes para el entrenamiento
### Resultados
El error fue de **0.0342672**

## submission_19
### Descripción
Se utiliza el modelo EfficientNet B7 con una resolución de imágenes de 224 px
### Resultados
El error fue de **0.0347830**

## submission_20
### Descripción
Se utiliza el modelo EfficientNet B7 con una resolución de imágenes de 600 px
### Resultados
El error fue de **0.0342779**

## submission_21
### Descripción
Se hace un **ensamble** de los modelos: EfficientNet B7 (res 600 px), EfficientNet B3 (res 300 px) and ResNet152 (res 224 px).
El ensamble se realiza a partir de la **media** de las predicciones de los tres modelos, dividiendo entre el número de predicciones de cada clase.
### Resultados
El error fue de 0.0353733

## submission_22
### Descripción
Igual que submission_21, pero el ensamble se realiza a partir de la **media** de las predicciones de los tres modelos, **dividiendo entre el número de modelos**.
### Resultados
El error fue de 0.0359276

## submission_23
### Descripción
Igual que submission_21, pero el ensamble se hace dando **peso a cada modelo**: EfficientNetB7: .37, EfficientNetB3: .43, ResNet152: .2
También se filtraron las detecciones que no son de animal, según la clasificación que les corresponde.
### Resultados
El error fue de **0.0308570** privado y **0.0320110** público que es un 9.5% menos

## submission_24
### Descripción
Se entrena un modelo **EfficientNetB3** (res 300 px) con las **imágenes completas** del conjunto WCS
### Resultados
El error fue de El error fue de 0.0395940 privado y 0.0397545 público

## submission_25
### Descripción
Se entrena un modelo **EfficientNetB7** (res 600 px) con las **imágenes completas** del conjunto WCS
### Resultados
El error fue de El error fue de 0.0353795 privado y 0.0358369 público

## submission_26
### Descripción
Se hace un **ensamble** de los modelos de los submission_24 y submission_25 asignando los pesos {'EfficientNetB7': .45, 'EfficientNetB3': .55}
### Resultados
El error fue de El error fue de 0.0306301 privado y 0.0329548 público

## submission_27 (late)
### Descripción
Se hace un **ensamble doble** de la siguiente manera: 
* Se ensamblan los modelos EfficientNetB3 y EfficientNetB7 de submission_23 entrenados con los **recortes** de las colecciones WCS y iNat con los siguientes pesos: {'EfficientNetB7-crops': .45, 'EfficientNetB3-crops': .55}. Este ensamble se llamará `crops`
* Se usa el ensamble de submission_26, entrenado con las **imágenes completas** de la colección WCS. Este ensamble se llamará `compl`
* **Se ensamblan ambos ensambles**, usando los siguientes pesos: {'compl': 0.3, 'crops': 0.7 }
### Resultados
El error fue de **0.0293573 privado y 0.0308132 público**

## submission_28 (late)
### Descripción
Se hace un **ensamble doble** parecido a submission_27, pero se cambian algunas cosas.
Se considera como `image-crop` un crop dummy de toda la imagen (coordenadas de toda la imagen, etiqueta Animalia y score 1). Todas las imágenes del conjunto de prueba tienen su `image-crop`.
Para cada imagen:
- Si no tiene detecciones, se usan sólo las clasificaciones del `image-crop` para determinar la clase presente (si no es `empty` se considerará 1 individuo).
- Si sí tiene detecciones, para cada una se hace una **suma ponderada de sus clasificaciones con las del `image-crop`**, usando los pesos: {crops: 0.7, compl: 0.3}
- Se crea un diccionario de la forma {`id`: [{`label`: `score`},...]} con las N clasificaciones resultantes ordenadas, donde `id` será ya sea el id de la detección o el id de la imagen.
- Para cada elemento en el diccionario se toma la clasificación más alta, que será la clasificación del crop o del `image-crop` y se crea con ello un dataset.
- Este dataset se pasará a la función `create_file_for_competition`, con el parámetro `how_merge=None` para que no se haga merge entre el dataset resultante y el de detecciones.
### Resultados
El error fue de 0.0316651 privado y 0.0331639 público