# Competición iWildcam2021

## Creación del dataset de entrenamiento
- Quitar los *crops demasiado pequeños* (< 35px) y aquellos dentro de la misma imagen cuyo score del Megadetector sea *mucho menor que el de la detección más alta* (diferencia > .5)
- *Asegurarme que los bboxes tengan un tamaño de lado mínimo (~224 px), incluso si el bbox es más pequeño, ajustando el bbox para que el animal esté lo más centrado posible. Además usar reflection padding*
- The *bbox with the max confidence* in each image is cropped to train a bbox model.
- I found that instead of directly cropping and resizing the detections, cropping and padding the detections using **reflection padding** improved the score.


## Aumento de datos
- Probar más operaciones de aumento de datos con **imgaug**:
    - [Repositorio en Github](https://github.com/aleju/imgaug#code_examples)
    - [Ejemplos en Keras](https://keras.io/examples/vision/randaugment/)
- *hue shift, gaussian noise, cutout, scale/rotate/shift, brightness/contrast adjustments and grayscale*
- *Color Jitter* : Brightness and Contrast (0.9 to 1.2). *Gaussian Blur* (sigma = 0.0 to 0.8)


## Métodos de entrenamiento para hacer más robustos los modelos
- **Noisy-student**
    - [Modelo Efficientnet pre-entrenado con Noisy-student](https://github.com/qubvel/efficientnet) using *label-smoothed cross-entropy focal loss*
- **Adversarial training (AdvProp)** was utilized to learn a noise robust representation.


## Modelos
- **InceptionResNetV2** 
    - [Stackoverflow: Fine-Tune pre-trained InceptionResnetV2](https://stackoverflow.com/questions/50069431/fine-tune-pre-trained-inceptionresnetv2) 
    - [keras-inception-resnet-v2](https://github.com/yuyang-huang/keras-inception-resnet-v2/blob/master/README.md)
    - [EfficientNet Keras (and TensorFlow Keras)](https://github.com/qubvel/efficientnet/blob/master/efficientnet/model.py)
- **SeResNeXt101**


## Data samplers
- **Balanced Group Softmax**:
    - [Artículo](https://arxiv.org/pdf/2006.10408.pdf)
    - [Repositorio](https://github.com/FishYuLi/BalancedGroupSoftmax)
- **Weighted cross-entropy loss**: assign weights to the cross-entropy loss such that it will penalize more to the smaller classes and the less to larger classes. 
    - [Implementación en PyTorch](https://discuss.pytorch.org/t/dealing-with-imbalanced-datasets-in-pytorch/22596).
- **Focal loss**: Originally proposed for object detection, but we can also use this for any other use cases.
    - [What is Focal Loss and when should you use it?](https://amaarora.github.io/2020/06/29/FocalLoss.html)
    - [Multi-class Focal Loss in Pytorch](https://github.com/AdeelH/pytorch-multi-class-focal-loss)
- **Over Sampling and Under Sampling**: I used [imblearn](https://pypi.org/project/imbalanced-learn/) and randomly upsampled classes smaller than 25 to 25, and randomly down sampled classes larger than 7000 to 7000. I kept in mind that the competition test set will also be imbalanced, so I didn't want to correct imbalance totally.
- For the long-tailed data distribution which utilizes *two data samplers*, one is the **uniformed sampler** which samples each image with a uniformed probability, another is a **reversed sampler** which samples each image with a probability proportional to the reciprocal of corresponding class sample size. The images from those two data samplers are then *mixed-up* for better performance.
- Used routine class weights calculated by 1 / # of images of each class.

# Geo-prior
- [Paper](https://arxiv.org/pdf/1906.05272.pdf)
- [Repo Pytorch](https://github.com/macaodha/geo_prior/tree/master/geo_prior)
- [Repo TF](https://github.com/alcunha/geo_prior_tf)

## Clasificador con metadatos de las imágenes
- An **auxiliary classifier for different locations** is added to our network, in the front of classification for location, the feature will go through a **gradient reversal layer** to ensure the model can learn a generalizable feature across locations.
- Created a **custom classifier head which incorporated metadata from the image**. This used cos/sin representations of of the time of year and **time of day**, and information regarding the crop (width, height, **pitch, yaw**).

## Inferencia en el conjunto test
- **TTA with every training augmentation** (Usar aumento de datos en el conjunto de prueba) except for noise to get predictions.
    - [Medium: Test Time Augmentation (TTA) and how to perform it with Keras](https://towardsdatascience.com/test-time-augmentation-tta-and-how-to-perform-it-with-keras-4ac19b67fb4d)

## Metadatos e imágenes Landsat
- Entrenar un Random Forest que tomen en cuenta: la salida del modelo de clasificación de especies, metadatos adicionales (Hora, mes, # imgs en la secuencia) y Datos Landsat.
    - [Artículo en Medium: iWildCam 2020 Trail Camera Animal Classification](https://medium.com/@bbouslog/iwildcam-2020-trail-camera-animal-classification-2535a23cebae)
    - Resumen:
        - The activity patterns that animals tend to follow which coincide with the time of day or the seasons.
        - We took Landsat images for each location and averaged all of the pixel values of the 9 bands of information which we deemed most important
        - We had information about how many images were taken for a given sequence of images. We figured that this might be important as larger animals tend to walk more slowly than smaller animals and therefore stay in frame longer, producing more images for each time they were spotted on the trail camera.
        - We trained a random forest classifier on this new dataset with the outputs of the convolutional neural network and the metadata as features
    - [Code Repository](https://gitlab.com/guanyuzhang/ee461p_2020spring)

## Otros
- Ensambles de 3 modelos: Renset 101, EfficientNet-B0 and EfficientNet-B3, y para cada uno en las imágenes de test aplicar: la imagen orignial, con CLAHE y en escala de grises, y con las 9 predicciones hacer un promedio ponderado.
- The training data and test data are from different regions. There are domain shift between two dataset. GAN for domain adaptation
- Transfer learning with domain shift


## Aplicados
- ~~During test, for the image which has at least one bbox, **the prediction is a weighted average of bbox model and full image model(0.3 full image + 0.7 bbox)**, for the images which have no bbox, we will use the full image model.~~
- ~~We train **two versions of each model**, one with **original full image** and one with the **cropped max confidence bbox**.~~
- ~~CLAHE & Simple White Balance~~: https://www.kaggle.com/seriousran/image-pre-processing-for-iwild-2020
- ~~Convertir a gray las imágenes diurnas~~
- ~~random horizontal flips, shifts, and rotations~~
- ~~Sacar detecciones sobre las fotos vacías del conjunto de entrenamiento y con los recuadros resultantes formar la clase "background" y reentrenar el modelo con este dataset "añadido"~~
- ~~Cuando en una foto de una secuencia se encuentra una detección con score <= umbral checar en la misma región de las otras fotos si el contenido es muy similar (p.e. diferencia pequeña en los valores de los pixeles) y descartar esa detección.~~
- ~~También descartar los falsos positivos del Megadetector en el conjunto de entrenamiento. Para esto hay que calcular las secuencias a partir de la localización y la fecha-hora.~~
- ~~Usar: resnet101, efficientnet-b0 y *efficientnet-b3* y hacer un ensamble.~~


