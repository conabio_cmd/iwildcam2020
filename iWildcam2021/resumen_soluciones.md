# iWildcam 2020

## LB 8
[Discussion](https://www.kaggle.com/c/iwildcam-2020-fgvc7/discussion/154211)
- Simple ensemble of a resnet 152 and a resnet 101
- Data augmentation (random flip, random rotate, **random zoom**, random contrast and **lighting modification**, **random warp**, and **mixup**)
- Both models were further fine-tuned for a couple epochs with a **class-balanced** subset of crops which contained *no more than 100 examples* per class.
- Within each image, each Megadetector detection is cropped, sent through the classifiers, and then weighted by Megadetector’s detection confidence. We tuned how much to weight this using a simple ‘**diminishing factor**’ `d: (detector_confidence + d) / (d + 1) * crop_prediction`.
- We used the datetime (encoded to sin/cos as in the paper) and the *mean normalized difference vegetation index* (basically infrared minus red) of each satellite image to construct the geographical priors model. We used a ‘diminishing factor’ for these priors too
- We tried just using resnet-extracted image features for each satellite image as features for the model (replacing latitude and longitude in the paper’s model. We eliminated all the cloud, shadow, and 0-value pixels from the satellite images using the QA data, so the NDVI we calculate is only for visible land.


## LB 4
[Discussion](https://www.kaggle.com/c/iwildcam-2020-fgvc7/discussion/156420)
### Megadetector bounding boxes
- Simple InceptionV3 model, trained on the provided Megadetector bounding boxes. We have used the Megadetector results to crop images out before training.
- We decided to use a square crop around the bounding box generated by Megadetector. The square size is the same as the largest dimension of the bounding box. Additionally, we have limited the **square crop to have at least 450x450** of resolution and the maximum of the height of each image. In addition, the square crop will always keep the bounding box area centered unless the crop gets out the image. In those cases, we move the crop to get valid pixels and the animal is no more centered.
- For the images without a bounding box, we just **crop a centered square with the image height**. This procedure was applied on all images before training, including images from the test set.
### Model training
- We have used an InceptionV3 model and train it for 48 epochs. The best accuracy model among all epochs was selected as the final model.
- Data augmentation: **zoom**, rotation, horizontal flip, **shearing**, horizontal and vertical shifts.
- SGD optimizer with momentum of 0.9.
- The learning rate schedule was 1-5: 0.005, 6-10: 0.001, 11-20: 0.005, 21-27: 0.001, 28-35: 0.0005, 36-onwards: 0.0001
### Majority voting among images from the same sequence
- To determine the final prediction for each image, we first check for **empty images**. If our model predicts “empty” and Megadetector does not provide bounding box, the class “empty” is assigned to the input image.
- Otherwise, **majority vote** is calculated considering all images from the same sequence, excluding the ones already marked as empty.
- Images are considered as belonging to the **same sequence** when they are from the same location and the difference between their timestamps is at most **30 minutes**.
### What we would like to have tested
- Big Transfer (BiT)
- Context R-CNN: Long Term Temporal Context for Per-Camera Object Detection (https://www.youtube.com/watch?v=eI8xTdcZ6VY)


## LB 3
[Discussion](https://www.kaggle.com/c/iwildcam-2020-fgvc7/discussion/157932)

### Data
- 224x224 crops provided by the official detector results.
### Preprocessing methods
- Horizontal-flip & CLAHE
### Models
- EfficientNet, ResNet152, NTS with ResNet50, NTS with ResNext50
- **GridSearch** to search for the optimal weights of each weak learner, using the local validation score as the benchmark.
### Prediction technique
- Test crops were sorted into clips following the following logic: Sequence ID --> Locations --> Time Stamp --> image dimension
- For each clip, we average predicted probs of all images to calculate the final label; then the final label was shared by all the images under this clip.
### Class imbalance
- Used routine class weights calculated by 1 / # of images of each class.
- Fine-grained features
- We used a SOTA network, NTS, which consists of a Navigator, a Teacher and a Scrutinizer.

## LB 2
[Discussion](https://www.kaggle.com/c/iwildcam-2020-fgvc7/discussion/157946)

- Removed noisy and/or 'human' class categories - 'start', 'end', 'unknown', and 'unidentifiable'. Retaining the empty class seemed important for reducing activation on background to generalise better.
- Encorporated iNat 2017/2018 data for shared classes.
- Ran MegaDetector v4 on all images.
- Took only animal detection crops with confidence >0.3 and **discarded detections which were too small**. Also **discarded crops within the same image if they were much less confident than the highest**.
- **Made sure to expand height of crop to be 224 pixels**, even if the bounding box was smaller, then used **reflection padding to square the crop** (https://kegui.medium.com/image-padding-reflection-padding-d3dee4dc2f63) and resized to 224x224 resolution.
- Applied various data augmentations including CLAHE, **hue shift, gaussian noise, cutout, scale/rotate/shift, brightness/contrast adjustments and grayscale**.
- Created a **custom classifier head which incorporated metadata from the image**. This used cos/sin representations of of the time of year and **time of day**, and information regarding the crop (width, height, **pitch, yaw**).
- Trained for 7 epochs with a single **EfficientNetB4** model pretrained with **imagenet noisystudent** weights ("Self-training with Noisy Student improves ImageNet classification"), using **label-smoothed cross-entropy focal loss**. This was 2 hours of training.
- Used **TTA with every training augmentation** (https://towardsdatascience.com/test-time-augmentation-tta-and-how-to-perform-it-with-keras-4ac19b67fb4d) except for noise to get predictions.
- Combined the predictions for each crop within an image to get a total average for the image.
- Averaged these predictions across images which were clustered by time and location for the final submission. This was only done as the sequence ID labels appeared to be unreliable.

## LB 1
[Discussion](https://www.kaggle.com/c/iwildcam-2020-fgvc7/discussion/158370)
- Efficientnet-b6 and **SeResNeXt101** (https://stackoverflow.com/questions/66679241/import-resnext-into-keras) as our backbone networks
- For the long-tailed data distribution which utilizes *two data samplers*, one is the *uniformed sampler* which samples each image with a uniformed probability, another is a *reversed sampler* which samples each image with a probability proportional to the reciprocal of corresponding class sample size. The images from those two data samplers are then *mixed-up* for better performance.
- An **auxiliary classifier for different locations** is added to our network, in the front of classification for location, the feature will go through a **gradient reversal layer** to ensure the model can learn a generalizable feature across locations.
- **Adversarial training (AdvProp)** was utilized to learn a noise robust representation.
- The **bbox with the max confidence** in each image is cropped to train a bbox model.
- We train **two versions of each model**, one with **original full image** and one with the **cropped max confidence bbox**.
- During test, for the image which has at least one bbox, **the prediction is a weighted average of bbox model and full image model(0.3 full image + 0.7 bbox)**, for the images which have no bbox, we will use the full image model.
- We first obtain the predictions of **Efficientnet-b6 and SeResNeXt101** using the aforementioned procedure, **then the final prediction is a simple average of the two network architectures**.
- We **average the predictions** which are clustered by location and datetime, as the **sequence** annotation appears to be noisy.


# iWildcam 2021

## LB 7
[Discussion](https://www.kaggle.com/c/iwildcam2021-fgvc8/discussion/242978)
- I created a **mini dataset** (10% of the original dataset) with the same class proportion as in the original.
- I found that instead of directly cropping and resizing the detections, cropping and padding the detections using **reflection padding** improved the score.
- Augmentations: **Color Jitter** : Brightness and Contrast (0.9 to 1.2), **Gaussian Blur** (sigma = 0.0 to 0.8)
- handled imbalance: https://www.kaggle.com/c/iwildcam2021-fgvc8/discussion/242455
- I used the **efficient b5 noisy student**. I did not use pre-trained image net weights.
- For the count of individuals I follow the **maximum frequency logic**.
- A **higher megadetector detection confidence** threshold worked better, so I used **0.7** as detection confidence


## LB 4
[Discussion](https://www.kaggle.com/c/iwildcam2021-fgvc8/discussion/243509)
- **two-stage model**, one **image-based**, the second, **detection-based**, where the output of first model was fed as metadata into the second model along with detection cropped images
- The two-stage pipeline was **trained separately for each of the six regions**. EfficientNet B3, was used in both stages with images cropped/resized to 400x400.

- To deal with *imbalanced classes*. The first was to randomly **under-sample the most common classes**. A weighted random sampler was used to **oversample the rarer classes**. The third method used in some cases was to incorporate species data from other regions.

## LB 2
[Discussion](https://www.kaggle.com/c/iwildcam2021-fgvc8/discussion/245559)
- A few EfficientNetB2 classifiers using a combination of under sampling and over sampling during training.
- For training the **megadetector cutoff confidence was 0.8**
- Optimized the images prior to the bounding box cropping using the *CLAHE* algorithm.
- We trained a full **EfficientNetB2 classifier** with input size 256x256. We used **exponential moving average (EMA)** on the weights during training.
- Data augmentation is **random crops, color jitter**. We fiddled with **CutMix** and **MixUp**
- For the imbalance problem we combined **under and over sampling**. The problem with oversampling is *overfitting* and with undersamping is *information loss*.
- Trained a separate (expert) **Geo prior** EfficientNetB2 network for all of the countries in the datasets where the **unknown location was bundled to a separate country**.

### Inference
- We used the EfficientNetB2 classifier on the provided **MD3 bounding boxes (confidence > 0.4)** and **discarded classifier results with a confidence < 0.7**
- When the prediction was an animal that wasn't in the trainset for that location we moved to the expert country networks and relied on that output.
- We also trained a **classifier for the entire image**. The objective of this classifier was to act as a *prior for especially herds*. In the case of the multiple boxes we would like to prefer to classify the same species. We combined the full image classifier (weighted 0.15) with the species classifier (0.85).
### Counting
For the counting we just used the **max operator** on the number of bounding boxes. The **classifier output has been validated on 0.75**. This made our counting algorithm really conservative. We only counted the animals we were absolutely certain. For the final submission we added **mean absolute difference to the max**, which improved the results a little.
The reason for this is the use of MCRMSE where miss-classification is a sever penalty. The other problem with the MCRMSE is in large herds weigh really heavy on the leaderboard scores. Since we undercount we took some risk there.
### Things that didn't make the cut (yet)
We have set up and auxiliary network where we use the provided metadata to try and act as a *regularizer*. We used circular encoding for GPS and for the time of day and month. We used this information together with the image data to train the species classifier. Unfortunately we didn't have sufficient resources to fully pursue and evaluate this direction.
We did however use the same trick to create an evaluation network. The principle was the same and we **tried to predict the species based on only on the metadata provided**. Surprisingly we got validation score greater than 80%. So we still believe this is route to investigate in the future.
- **Metadata is valuable** (but we don't know yet how much)

## LB 1
[Discussion](https://www.kaggle.com/c/iwildcam2021-fgvc8/discussion/245460)

### Classifier
- We trained **two EfficientNet-B2 models**: one using **full images** and a second one using **square crops** from bounding boxes generated by MegaDetectorV4.
- We used **Balanced Group Softmax** to handle the class imbalance problem in the dataset.
- The prediction assigned to each image is the *weighted average* of the predictions assigned to the **bounding box with the highest score** and to the full image (**0.15 full image + 0.15 full image (flip) + 0.35 bbox + 0.35 bbox (flip)**).
- **To classify a sequence, we average the predictions of all nonempty images** for each sequence.
#### Training procedure
- We initialized EfficientNet-B2 with ImageNet weights and trained it for 20 epochs on the iWildCam2021 training set using the default input resolution (260x260). Then **we fine-tuned the last layers for 2 epochs using a higher input resolution (380x380) to fix the train/test resolution**
- Specifically for the bbox model, we **pretrained the classifier layer for 4 epochs before unfreezing all layers to train**.
- During the training, we used *label smoothing and SGD with momentum*. We also applied *warmup and cosine decay* to the learning rate.
- For the bbox model, we considered all **bounding boxes with confidence > 0.6** as an independent training sample and applied a square crop around it with the size of the largest side.
#### Image preprocessing
- Random crop, random flip, and **RandAugment (N=6, M=2)**.
- During the fix train/test resolution stage, we used the test preprocessing, which consists *only of resizing the image/crop to the network input* (Keras EfficientNet includes normalization layers).
#### Handling the class imbalance

#### Validation
We used the validation set to **adjust hyperparameters**. For the final submission, we used all training instances to train the model using the tuned hyperparameters.
- Initially, we were using voting among images predictions to classify a sequence, but after **adding bags** we found out that averaging worked better.

### Counting heuristic
The *maximum number of bounding boxes across any image in the sequence*. We only counted bounding boxes from MegaDetectorV4 with **confidence > 0.8**.

### Other things that we tried

#### Geo Prior Model
We tried to **add some noise** to data by varying the GPS coordinates within 5km radius and timestamp within 10 days. We also replaced the original Geo Prior model loss with the **focal loss**.

#### Multi-object tracking with DeepSORT
- We tried to use DeepSORT *to generate tracks over sequences and then classify each track by averaging the predictions of its bounding boxes* list using the bbox model. We used the **feature embedding from EfficientNet-B2** trained on bounding boxes.
