#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse

from tensorflow.keras.applications import EfficientNetB3, EfficientNetB7

from conabio_ml.utils.dataset_utils import read_labelmap_file
from conabio_ml_vision.datasets.datasets import ConabioImageDataset
from conabio_ml.utils.logger import get_logger

from scripts.utils import *
from iWildcam2021.src.utils_2021 import *

logger = get_logger(__name__)

"""En este script se hizo un ensamble doble, por una parte de dos modelos entrenados con los crops 
de las detecciones del Megadetector, luego de otros dos modelos entrenados con las imágenes enteras
de la colección WCS y finalmente se ensamblan ambas predicciones. Esto se hace la siguiente manera:
Se considera como `image-crop` un crop dummy de toda la imagen (coordenadas de toda la imagen,
etiqueta Animalia y score 1). Todas las imágenes del conjunto de prueba tienen su `image-crop`.
Para cada imagen:
- Si no tiene detecciones, se usan sólo las clasificaciones del `image-crop` para determinar la
clase presente (si no es `empty` se considerará 1 individuo).
- Si sí tiene detecciones, para cada una se hace una suma ponderada de sus clasificaciones con las
del `image-crop`, usando los pesos: {crops: 0.7, compl: 0.3}
- Se toma la clasificación más alta, que será la clasificación del `crop` o del `image-crop` y se
crea con ello un dataset. Con este dataset se crea el archivo que se envía a la competición.
"""


experiment = "ensemble_2_eval"
num_submission = 28
add_sub_suffix = False

model_file_name_efficient_b3 = 'efftnet_b3_complete.model.hdf5'
model_file_name_efficient_b7 = 'efftnet_b7_complete.model.hdf5'

TRAIN_PERC = 0.8
BATCH_SIZE_TRAIN = 32
BATCH_SIZE_EVAL = 16
N_EPOCHS = 20
SCORE_THRES = 0.3
EFFIC_B3_SIZES = (300, 300)
EFFIC_B7_SIZES = (600, 600)
MEGADET_LABELMAP = ConabioImageDataset.MEGADETECTOR_V4_LABELMAP

repo_base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020"
base_path = os.path.join(repo_base_path, "iWildcam2021")
files_path = os.path.join(base_path, "files")
data_path = os.path.join(base_path, "data")
results_path = os.path.join(base_path, "results", experiment)

inat_images = os.path.join(data_path, "iNat_images")
wcs_metadata = os.path.join(data_path, "WCS_metadata")
wcs_images = os.path.join(data_path, "WCS_images")
wcs_image_level_anns_json_file = os.path.join(wcs_metadata, "iwildcam2021_train_annotations.json")
wcs_test_info_json = os.path.join(wcs_metadata, "iwildcam2021_test_information.json")
sample_submission_path = os.path.join(wcs_metadata, "sample_submission.csv")
wcs_test_images_dir = os.path.join(wcs_images, "test_CLAHE_WB")
wcs_train_images_dir = os.path.join(wcs_images, "train_CLAHE_WB")
test_crops_dir = os.path.join(wcs_images, "crops_of_test_set_MegadetV4")
train_crops_dir = os.path.join(wcs_images, "crops_of_train_set_MegadetV4")
inat_17_18_train_images_dir = os.path.join(inat_images, "inaturalist_17_18_CLAHE_WB")
inat_21_train_images_dir = os.path.join(inat_images, "inaturalist_2021_CLAHE_WB")

files_dataset = os.path.join(files_path, "datasets")
files_dets = os.path.join(files_path, "detections")
files_fp = os.path.join(files_path, "false_positives")
files_iNat = os.path.join(files_path, "inat_cats")
files_gp = os.path.join(files_path, "geo_prior")
wcs_ds_crops_json_file = os.path.join(files_dataset, "wcs_from_dets_MegadetV4.json")
wcs_ds_crops_empty_json_file = os.path.join(files_dataset, "wcs_from_dets_empty_MegadetV4.json")
dataset_inat_21_json_file = os.path.join(files_dataset, "inat_21_from_dets_MegadetV4.json")
dataset_inat_17_18_json_file = os.path.join(files_dataset, "inat_17_18_curated.json")
detections_on_test_set_compl_path = os.path.join(files_dets, "test_img_compl_dummy.csv")
detections_on_test_set_path = os.path.join(files_dets, "test_MegadetV4.csv")
detections_on_test_set_valid_path = os.path.join(files_dets, "test_MegadetV4_valid.csv")
false_pos_dets_test_csv = os.path.join(files_fp, "test_Acc_Thrs40.csv")
inat_17_18_cats_file = os.path.join(files_iNat, "common_cats_inat_17_18_wcs.txt")
inat_17_18_mapping_file = os.path.join(files_iNat, "map_similar_cats_inat_17_18_to_wcs.csv")
inat_21_cats_file = os.path.join(files_iNat, "common_cats_inat_21_wcs.txt")
inat_21_mapping_file = os.path.join(files_iNat, "map_similar_cats_inat_21_to_wcs.csv")
geo_prior_compl_checkpoints_path = os.path.join(files_gp, "ckps_wcs_img_compl_inat_17_18_21")
test_geo_prior_compl_json = os.path.join(files_gp, "test_wcs_img_compl_inat_17_18_21.json")
geo_prior_crops_checkpoints_path = os.path.join(files_gp, "ckps_wcs_inat_17_18_21")
test_geo_prior_crops_json = os.path.join(files_gp, "test_wcs_inat_17_18_21.json")


results_classifs = os.path.join(results_path, "classifs_on_test_crops")
result_dataset_compl_path = os.path.join(results_path, f"dataset_compl.csv")
aux_dets_of_megadet_compl_ds_path = os.path.join(results_path, f"aux_dets_of_megadet_compl_ds.csv")
aux_dets_of_megadet_crops_ds_path = os.path.join(results_path, f"aux_dets_of_megadet_crops_ds.csv")
classifs_path = os.path.join(results_classifs, f"ensamble_2.csv")
visualize_results_dir = os.path.join(results_path, "test_images_classifs_bboxes_seq_id")
labelmap_classif_compl_file = os.path.join(results_path, f"labels.txt")
submission_csv = os.path.join(results_path, f'submission_{num_submission}.csv')

ensemble_method = 'weighted'

os.makedirs(results_classifs, exist_ok=True)

model_checkpoints = {
    'EfficientNetB3': os.path.join(results_path, model_file_name_efficient_b3),
    'EfficientNetB7': os.path.join(results_path, model_file_name_efficient_b7),
}
model_base_instances = {
    'EfficientNetB7': EfficientNetB7,
    'EfficientNetB3': EfficientNetB3
}
model_input_sizes = {
    'EfficientNetB7': EFFIC_B7_SIZES,
    'EfficientNetB3': EFFIC_B3_SIZES,
}


def classify_test_compl_imgs(model_name):
    compl_dataset = create_complete_imgs_dataset(wcs_dataset_json_file=wcs_image_level_anns_json_file,
                                                 wcs_images_dir=wcs_train_images_dir,
                                                 train_perc=TRAIN_PERC,
                                                 result_dataset_path=result_dataset_compl_path,
                                                 IWildcamBaseImageDataset=IWildcam2021ImageDataset)
    if model_name is not None:
        model = train(model_checkpoint_path=model_checkpoints[model_name],
                      dataset=compl_dataset,
                      labelmap_path=labelmap_classif_compl_file,
                      model_type=model_base_instances[model_name],
                      model_name=model_name,
                      images_size=model_input_sizes[model_name],
                      epochs=N_EPOCHS,
                      batch_size=BATCH_SIZE_TRAIN)
        classifs_base, ext = os.path.splitext(classifs_path)
        csv_path = f"{classifs_base}-{model_name}-compl{ext}"
        labelmap_classif_compl = read_labelmap_file(labelmap_classif_compl_file)
        classify_crops_in_test_set(detections_on_test_set_path=detections_on_test_set_compl_path,
                                   test_images_dir=wcs_test_images_dir,
                                   crops_dir=wcs_test_images_dir,
                                   score_threshold=SCORE_THRES,
                                   model=model,
                                   labelmap_classification=labelmap_classif_compl,
                                   classifs_on_test_crops_path=csv_path,
                                   aux_dets_of_megadet_ds_path=aux_dets_of_megadet_compl_ds_path,
                                   images_size=model_input_sizes[model_name],
                                   batch_size=BATCH_SIZE_EVAL,
                                   labelmap_detection=MEGADET_LABELMAP,
                                   geo_prior_checkpoints=geo_prior_compl_checkpoints_path,
                                   test_geo_prior_json=test_geo_prior_compl_json,
                                   IWildcamBaseImageDataset=IWildcam2021ImageDataset)


def ensemble():
    classifs_base, ext = os.path.splitext(classifs_path)
    csv_path = f"{classifs_base}-crops{ext}"
    models_crops_weights = {'EfficientNetB7-crops': .45, 'EfficientNetB3-crops': .55}
    models_crops_names = [x for x in models_crops_weights.keys()]
    crops_classifs_ds = ensemble_classification_models(models_names=models_crops_names,
                                                       classifs_csv=csv_path,
                                                       images_dir=test_crops_dir,
                                                       method='weighted',
                                                       model_weights=models_crops_weights,
                                                       max_classifs=10)

    classifs_base, ext = os.path.splitext(classifs_path)
    csv_path = f"{classifs_base}-compl{ext}"
    models_compl_weights = {'EfficientNetB7-compl': .45, 'EfficientNetB3-compl': .55}
    models_compl_names = [x for x in models_compl_weights.keys()]
    compl_classifs_ds = ensemble_classification_models(models_names=models_compl_names,
                                                       classifs_csv=csv_path,
                                                       images_dir=wcs_test_images_dir,
                                                       method='weighted',
                                                       model_weights=models_compl_weights,
                                                       max_classifs=10)
    models_weights = {
        'compl': 0.3,
        'crops': 0.7
    }
    classifs_on_test_crops_ds = ensemble_compl_and_crops_models(compl_ds=compl_classifs_ds,
                                                                crops_ds=crops_classifs_ds,
                                                                classifs_csv=classifs_path,
                                                                images_dir=wcs_test_images_dir,
                                                                method=ensemble_method,
                                                                model_weights=models_weights)

    create_file_for_competition(classifs_on_detection_crops_ds=classifs_on_test_crops_ds,
                                detections_on_test_set=detections_on_test_set_path,
                                test_images_dir=wcs_test_images_dir,
                                sample_submission_path=sample_submission_path,
                                wcs_image_level_anns_json_file=wcs_image_level_anns_json_file,
                                out_csv_file=submission_csv,
                                count_strategy='max_mode_sequence',
                                how_merge=None)


def visualize_detections_results():
    classifs_base, ext = os.path.splitext(classifs_path)
    classifs_csv = f"{classifs_base}-compl_crops-{ensemble_method}{ext}"
    visualize_classif_bboxes_in_ds(dataset_json=wcs_test_info_json,
                                   detections_csv=detections_on_test_set_path,
                                   classifs_csv=classifs_csv,
                                   images_dir=wcs_test_images_dir,
                                   dest_dir=visualize_results_dir,
                                   false_pos_dets_csv=false_pos_dets_test_csv,
                                   submission_file=submission_csv,
                                   cats_file=wcs_image_level_anns_json_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--classify_test_compl_imgs", default=False, action="store_true")
    parser.add_argument("--ensemble", default=False, action="store_true")
    parser.add_argument("--visualize_detections_results", default=False, action="store_true")

    parser.add_argument('--model', default=None, type=str)
    args = parser.parse_args()

    if args.classify_test_compl_imgs:
        classify_test_compl_imgs(args.model)
    if args.ensemble:
        ensemble()
    if args.visualize_detections_results:
        visualize_detections_results()
