#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import json
import pandas as pd
import argparse
from shutil import copyfile
from collections import defaultdict
import multiprocessing
from conabio_ml.utils.logger import get_logger

from conabio_ml.utils.utils import create_shared_defaultdict, str2bool

from conabio_ml_vision.datasets.datasets import ImagePredictionDataset, ImageDataset
from conabio_ml_vision.trainer.model import run_megadetector_inference
from conabio_ml_vision.utils.images_utils import draw_bboxes_in_image

from scripts.utils import *

logger = get_logger(__name__)

MIN_SCORE_TRUE_CLASSIFS = 0.5
GREEN = (0, 255, 0)
NUM_GPUS_PER_NODE = 2


class IWildcam2021ImageDataset(ImageDataset):
    """Definición de un dataset de la colección WCS para la competición iWildcam2021"""

    class IMAGES_FIELDS():
        ID = "id"
        FILENAME = "file_name"
        WIDTH = "width"
        HEIGHT = "height"
        DATE_CAPTURED = "datetime"
        LOCATION = "location"
        SUBLOCATION = "sub_location"
        SEQ_NUM_FRAMES = "seq_num_frames"
        SEQ_ID = "seq_id"
        SEQ_FRAME_NUM = "seq_frame_num"

        NAMES = [ID, FILENAME, WIDTH, HEIGHT, DATE_CAPTURED, LOCATION, SUBLOCATION, SEQ_NUM_FRAMES,
                 SEQ_ID, SEQ_FRAME_NUM]
        DEFAULTS = {
            SUBLOCATION: 0
        }


def test_create_file_for_competition(classifs_on_detection_crops_csv,
                                     sample_submission_path,
                                     wcs_image_level_anns_json_file,
                                     seq_id,
                                     count_strategy='max_mode_sequence'):
    """Función para depurar la creación del archivo submission para la competición

    Parameters
    ----------
    classifs_on_detection_crops_csv : str
        Ruta del archivo CSV que contiene las clasificaciones sobre el conjunto de prueba
    sample_submission_path : str
        Ruta del archivo CSV `sample_submission` de la competición
    wcs_image_level_anns_json_file : str
        Archivo JSON tipo COCO con el campo `categories` del cual se obtiene el labelmap con el id
        y el nombre de cada categoría
    seq_id : str
        Id de una secuencia en particular sobre la que se quiere hacer el procesamiento
    count_strategy : str, optional
        Cadena que indica la forma en que se agrupan las clasificaciones para realizar los conteos
        de especies por cada secuencia. By default 'max_mode_sequence'
    """
    classifs_ds = ImagePredictionDataset.from_csv(source_path=classifs_on_detection_crops_csv)
    create_file_for_competition(classifs_on_detection_crops_ds=classifs_ds,
                                sample_submission_path=sample_submission_path,
                                wcs_image_level_anns_json_file=wcs_image_level_anns_json_file,
                                out_csv_file=None,
                                count_strategy=count_strategy,
                                seq_id=seq_id)


def create_file_for_competition(classifs_on_detection_crops_ds,
                                detections_on_test_set,
                                test_images_dir,
                                sample_submission_path,
                                wcs_image_level_anns_json_file,
                                out_csv_file,
                                count_strategy,
                                min_score_classif=0.5,
                                how_to_merge='inner',
                                seq_id=None):
    """Función que a partir del dataset de predicciones `classifs_on_detection_crops_ds` con la 
    clasificación de cada detección sobre el conjunto de prueba, genera un archivo CSV que podrá
    ser enviado a la competición iWildcam2021 (formato dado en `sample_submission_path`)

    Parameters
    ----------
    classifs_on_detection_crops_ds : ImagePredictionDataset
        Dataset que contiene las predicciones del modelo de clasificación sobre cada uno de los
        recuadros encontrados por el Megadetector en el conjunto de prueba.
        Este dataset debe contener las columnas `image_id`, `seq_id`, `seq_frame_num` y
        `id` (el id de la detección)
    detections_on_test_set : str or ImagePredictionDataset
        Ruta del archivo CSV o instancia de un dataset con las predicciones hechas por un modelo de
        detección de objetos (p.e. Megadetector) sobre el conjunto de prueba
    test_images_dir : str
        Ruta donde se encuentran las imágenes del conjunto de prueba
    sample_submission_path : str
        Ruta del archivo CSV `sample_submission` de la competición
    wcs_image_level_anns_json_file : str
        Archivo JSON tipo COCO con el campo `categories` del cual se obtiene el labelmap con el id
        y el nombre de cada categoría
    out_csv_file : str
        Ruta del archivo CSV que se genera para enviar a la competición
    count_strategy : str
        Cadena que indica la forma en que se agrupan las clasificaciones para realizar los conteos
        de especies por cada secuencia.
    min_score_classif : float, optional
        Valor mínimo de probabilidad para que una clasificación no sea descartada, by default 0.5
    how_to_merge : str
        Forma en que se hará el merge entre los datasets de clasificación y detección.
        Las opciones válidas son las permitidas por la función `pd.merge`
    seq_id : str, optional
        Id de una secuencia en particular sobre la que se quiere hacer el procesamiento.
        Generalmente usado para depurar, by default None
    """
    wcs_data = json.load(open(wcs_image_level_anns_json_file))
    cats_to_id = sorted(wcs_data["categories"], key=lambda i: i["id"])[1:]

    if type(detections_on_test_set) is str:
        dets_ds = ImagePredictionDataset.from_csv(source_path=detections_on_test_set,
                                                  images_dir=test_images_dir)
    else:
        dets_ds = detections_on_test_set

    subm_sample = pd.read_csv(sample_submission_path)
    seq_ids = subm_sample["Id"].values

    seq_counts_dict = create_shared_defaultdict()
    classifs_df = classifs_on_detection_crops_ds.as_dataframe()
    dets_df = dets_ds.as_dataframe()

    if how_to_merge is not None:
        merged_dets_classifs = merge_dets_and_classifs_dfs(dets_df, classifs_df, how_to_merge)
        dets_classifs_df = filter_non_animal_labels(merged_dets_classifs)
    else:
        dets_classifs_df = classifs_df.rename(columns={"label": "label_classif",
                                                       "score": "score_classif"})

    if seq_id is not None:
        find_counts_of_species_in_sequence_mode(seq_id, dets_classifs_df, cats_to_id,
                                                seq_counts_dict, min_score_classif)
    else:
        logger.info(f"Processing {len(seq_ids)} sequences as {count_strategy}")
        with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
            pool.starmap(find_counts_of_species_in_sequence_mode,
                         [(_seq_id, dets_classifs_df, cats_to_id, seq_counts_dict,
                           min_score_classif)
                          for _seq_id in seq_ids])

    results = defaultdict(list)
    for _seq_id, counts_spcs in seq_counts_dict.items():
        results["Id"].append(_seq_id)
        for i, count_spcs in enumerate(counts_spcs):
            results[f"Predicted{cats_to_id[i]['id']}"].append(count_spcs)

    if out_csv_file is not None:
        data = pd.DataFrame(results)
        data.to_csv(out_csv_file, index=False, header=True)
        logger.info(f"File {out_csv_file} created")


def find_counts_of_species_in_sequence_mode(seq_id, dets_classifs_df, cats_to_id,
                                            seq_counts_dict, min_score_classif=0.5):
    """Función que para las entradas en `dets_classifs_df` de la secuencia `seq_id` calcula una
    etiqueta que será la más frecuente (moda) y con mayor score en caso de haber más de una, y será
    esta etiqueta la que se asignará a toda la secuencia.
    El conteo de individuos se realiza tomando el número de recuadros mayor en una imagen de la
    secuencia.

    Parameters
    ----------
    seq_id : str
        Id de la secuencia
    dets_classifs_df : DataFrame
        DataFrame que contiene la información de las clasificaciones hechas para cada recuadro
        encontrado por un modelo de detección de objetos en las imágenes de prueba
    cats_to_id : list of dicts
        Lista ordenada por `id` que contiene el nombre y el id de cada categoría del dataset en el
        modelo de clasificación
    seq_counts_dict : defaultdict of lists
        Diccionario al que se agrega como llave `seq_id` y como valor una lista con el número
        máximo de detecciones encontradas en alguna de las imágenes de la secuencia `seq_id`
        para cada categoría en `cats_to_id`
    min_score_classif : float, optional
        Valor mínimo de probabilidad para que una clasificación no sea descartada, by default 0.5
    """
    classifs_of_seq = dets_classifs_df[(dets_classifs_df["seq_id"] == seq_id) &
                                       (dets_classifs_df["label_classif"] != "empty") &
                                       (dets_classifs_df["score_classif"] >= min_score_classif)]
    if len(classifs_of_seq) > 0:
        labels_mode = classifs_of_seq["label_classif"].mode()
        if len(labels_mode) > 1:
            sorted_classifs_of_seq = classifs_of_seq.sort_values(by=["score_classif"], axis=0,
                                                                 ascending=False, inplace=False)
            selected_label = sorted_classifs_of_seq[sorted_classifs_of_seq["label_classif"].isin(
                labels_mode.values)].iloc[0]["label_classif"]
        else:
            selected_label = labels_mode.iloc[0]
        for cat_to_id in cats_to_id:
            if cat_to_id["name"] == selected_label:
                ids = classifs_of_seq["image_id"].unique()
                clsfs_p_img = [len(classifs_of_seq[classifs_of_seq["image_id"] == x]) for x in ids]
                seq_counts_dict[seq_id] += [max(clsfs_p_img)]
            else:
                seq_counts_dict[seq_id] += [0]
    else:
        seq_counts_dict[seq_id] = [0] * len(cats_to_id)


def visualize_classif_bboxes_in_ds(dataset_json,
                                   detections_csv,
                                   classifs_csv,
                                   images_dir,
                                   dest_dir,
                                   false_pos_dets_csv=None,
                                   only_detections=False,
                                   split_in_seqs=True,
                                   min_score_true_classifs=MIN_SCORE_TRUE_CLASSIFS,
                                   submission_file=None,
                                   cats_file=None,
                                   seq_id=None):
    """Función que dibuja los recuadros de las detecciones con la etiqueta y el score de su
    clasificación sobre un conjunto de datos, guardando las imágenes agrupadas en carpetas con el
    nombre de la secuencia y con la clasificación y el número de individuos finales encontrados
    como sufijo

    Parameters
    ----------
    dataset_json : str
        Ruta del archivo JSON que contiene el dataset con la información de todas las imágenes del
        conjunto. P.e. `WCS_metadata/iwildcam2021_test_information.json`
    detections_csv : srt
        Ruta del archivo CSV que contiene las detecciones hechas sobre el conjunto representado
        por `dataset_json`. Debe tener la columna `bbox` con las coordenadas de la detección.
        P.e. `files/detections_on_test_set_MegadetV4_CLAHE_WB.csv`
    classifs_csv : str
        Ruta del archivo CSV que contiene las clasificaciones hechas sobre los recuadros formados
        a partir de las detecciones de `detections_csv`. Debe tener las columnas `label` y `score`
        de la clasificación. P.e. `results/classifs_on_test_crops.csv`
    images_dir : str
        Carpeta donde están las imágenes originales del dataset. P.e. `WCS_images/test_CLAHE_WB`
    dest_dir : str
        Ruta de la carpeta raíz donde se guardarán las imágenes resultantes agrupadas por
        secuencia. P.e. `results/test_images_classifs_bboxes_seq_id`
    false_pos_dets_csv : str, optional
        Ruta del archivo CSV que contiene las detecciones que son consideradas falsos positivos. 
        Debe contener las columnas `seq_ids` y `det_ids`. By default None
    only_detections : bool, optional
        Bandera que indica si solo se procesarán las imágenes que tienen detecciones encontradas
        o no, by default False
    split_in_seqs : bool, optional
        Bandera que indica si las imágenes serán agrupadas en carpetas que se forman con el campo
        `seq_id` seguido de la clasificación y el número de individuos encontrados ('empty' en caso
        de no haber), by default True
    min_score_true_classifs : float, optional
        Valor de score mínimo para que las clasificaciones sean descartadas como falsas,
        by default MIN_SCORE_TRUE_CLASSIFS
    submission_file : str, optional
        Ruta del archivo CSV submission de la competición que será usado para determinar la
        etiqueta de clasificación y el número de individuos de la secuencia, by default None.
        P.e. `results/submission_x.csv`
    cats_file : str, optional
        Ruta del archivo JSON que contiene la definición de categorías de la competición, y que
        será usado para determinar la etiqueta de clasificación de la secuencia, by default None.
        P.e. `WCS_metadata/iwildcam2021_train_annotations.json`
    seq_id : str, optional
        Id de una secuencia en particular sobre la que se quiere hacer el procesamiento.
        Generalmente usado para depurar, by default None
    """

    orig_ds = IWildcam2021ImageDataset.from_json(source_path=dataset_json, images_dir=images_dir)
    dets_ds = ImagePredictionDataset.from_csv(source_path=detections_csv, images_dir=images_dir)
    classifs_ds = IWildcam2021ImageDataset.from_csv(source_path=classifs_csv, images_dir=images_dir)
    orig_df = orig_ds.as_dataframe(columns=["seq_id"])
    dets_df = dets_ds.as_dataframe()
    classifs_df = classifs_ds.as_dataframe()

    cats_to_id = None
    if cats_file is not None:
        cats_to_id = sorted(json.load(open(cats_file))["categories"], key=lambda i: i["id"])[1:]
    submission = None
    if submission_file is not None:
        submission = pd.read_csv(submission_file)
    if false_pos_dets_csv is not None and os.path.isfile(false_pos_dets_csv):
        false_pos_dets = pd.read_csv(false_pos_dets_csv, header=0)

    dets_classifs_df = pd.merge(left=dets_df, right=classifs_df, how='left', on='id')
    dets_classifs_df.rename(
        columns={"item_x": "item", "id_x": "id_det", "id_y": "id_classif",
                 "label_x": "label_det", "label_y": "label_classif",
                 "score_x": "score_det", "score_y": "score_classif"}, inplace=True)
    dets_classifs_df = dets_classifs_df[["item", "bbox",  "id_det", "id_classif",
                                         "label_det", "label_classif",
                                         "score_det", "score_classif"]]
    if seq_id is not None:
        draw_classification_bboxes_in_seq(seq_id, orig_df, dets_classifs_df, dest_dir,
                                          split_in_seqs, min_score_true_classifs,
                                          cats_to_id, submission, false_pos_dets)
    else:
        seqs_ids = dets_df["seq_id"].unique() if only_detections else orig_df["seq_id"].unique()
        logger.info(f"Visualizando la información de {len(seqs_ids)} secuencias en {dest_dir}")
        with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
            pool.starmap(draw_classification_bboxes_in_seq,
                         [(seq_id, orig_df, dets_classifs_df, dest_dir, split_in_seqs,
                           min_score_true_classifs, cats_to_id, submission, false_pos_dets)
                          for seq_id in seqs_ids])


def draw_classification_bboxes_in_seq(seq_id,
                                      orig_df,
                                      dets_classifs_df,
                                      dest_dir,
                                      split_in_seqs,
                                      min_score_true_classifs,
                                      cats_to_id,
                                      submission,
                                      false_pos_dets):
    """Función que dibuja los recuadros de las detecciones (con el nombre de la etiqueta de
    clasificación y la probabilidad de clasificación) en las imágenes de una secuencia. Las
    imágenes resultantes las guarda en una carpeta con el nombre del `seq_id` y con el sufijo de la
    clasificación y el conteo de individuos final, y en caso de no tener individuos detectados,
    el `seq_id` seguido de la leyenda 'empty'

    Parameters
    ----------
    seq_id :  str
        Id de la secuencia de imágenes
    orig_df : pd.DataFrame
        DataFrame que contiene todos los registros del conjunto que se va a analizar, incluso
        aquellos que no tengan detecciones o clasificaciones
    dets_classifs_df : pd.DataFrame
        DataFrame que contiene las detecciones encontradas sobre las imágenes de la secuencia,
        con la etiqueta y el score de clasificación en las columnas `label` y `score`
    dest_dir : str
        Ruta de la carpeta raíz donde se guardarán las imágenes resultantes agrupadas por secuencia
    split_in_seqs : bool
        Bandera que indica si las imágenes resultantes serán agrupadas en carpetas según `seq_id`
    min_score_true_classifs : float
        Valor de score mínimo para que las clasificaciones sean descartadas como falsas
    cats_to_id : list of dicts
        Lista que contiene las descripciones de las categorías y que será usado junto con
        `submission` para formar el sufijo del nombre de la carpeta en que se agruparán las
        imágenes de la secuencia
    submission : pd.DataFrame
        DataFrame que contiene el submission de la competición y que será usado junto con
        `cats_to_id` para formar el sufijo del nombre de la carpeta en que se agruparán las
        imágenes de la secuencia
    false_pos_dets : pd.DataFrame, optional
        DataFrame que contiene las detecciones que son consideradas falsos positivos.
        Estas detecciones serán dibujadas en color rojo y con la etiqueta 'FP'.
        Debe contener las columnas `seq_ids` y `det_ids`. By default None
    """
    orig_items_of_seq = orig_df[orig_df["seq_id"] == seq_id]["item"].unique()
    bboxes_info = get_bboxes_info_for_items(orig_items_of_seq, dets_classifs_df,
                                            false_pos_dets, min_score_true_classifs)
    empty_seq = any([clr == GREEN for _, bboxs in bboxes_info.items() for clr in bboxs['colors']])
    if split_in_seqs:
        dir_suffix = ""
        if cats_to_id is not None and submission is not None:
            if empty_seq:
                dir_suffix = '-empty'
            else:
                names, counts = list(), list()
                counts_of_seq = submission[submission["Id"] == seq_id].iloc[0][1:]
                for i, cat_counts in enumerate(counts_of_seq):
                    if cat_counts > 0:
                        names.append(cats_to_id[i]["name"])
                        counts.append(cat_counts)
                names_cnts = [f'{nm.replace(" ", "_")}_{cnt}' for nm, cnt in zip(names, counts)]
                dir_suffix = f'-{"-".join(names_cnts)}'
        seq_dir = os.path.join(dest_dir, f"{seq_id}{dir_suffix}")
        os.makedirs(seq_dir, exist_ok=True)
    for item in orig_items_of_seq:
        if split_in_seqs:
            img_dest_path = os.path.join(seq_dir, os.path.basename(item))
        else:
            img_dest_path = os.path.join(dest_dir, os.path.basename(item))
        if item in bboxes_info:
            lbls, scrs = bboxes_info[item]['labels'], bboxes_info[item]['scores']
            lbls_scrs = [f"{lbl}: {int(scr*100)}%" if lbl else ""
                         for lbl, scr in zip(lbls, scrs)]
            draw_bboxes_in_image(item, img_dest_path, bboxes_info[item]['bboxes'],
                                 lbls_scrs, bboxes_info[item]['colors'])
        else:
            copyfile(item, img_dest_path)


def generate_false_pos_dets_in_seqs(detections_csv,
                                    dataset_json,
                                    images_dir,
                                    sub_method,
                                    results_csv_path,
                                    num_tasks=None,
                                    task_num=None):
    """Función para generar las detecciones que son consideras falsos positivos del Megadetector
    a partir de un archivo CSV con las detecciones hechas sobre el conjunto de imágenes.

    Parameters
    ----------
    detections_csv : str
        Archivo CSV que contiene las predicciones hechas por un modelo de detección de objetos 
    dataset_json : str
        Ruta del archivo JSON tipo COCO que se usará para obtener todas las fotos de una secuencia
        y descartar los falsos positivos del Megadetector
    images_dir : str
        Ruta de la carpeta que contiene a las imágenes del dataset
    sub_method : str
        Método que se utilizará para determinar los falsos positivos. Puede ser 'Acc' o 'MOG'
    results_csv_path : str
        Ruta del archivo CSV donde se guardarán los falsos positivos resultantes con las columnas
        `seq_ids` y `det_ids`
    num_tasks : int, optional
        En caso de realizar el procesamiento de forma paralela (p.e. en varios nodos), este es el
        número total de tareas en las que se divide el procesamiento. P.e., si se cuenta con 4
        nodos con 2 GPUs en cada uno, el trabajo se puede dividir en `num_tasks = 8` tareas
        paralelas, para así maximizar el uso de los recursos y realizar el procesamiento en el
        menor tiempo posible
    task_num : int, optional
        En caso de realizar el procesamiento de forma paralela, este es el número de la tarea
        actual. Debe de ser un entero en el rango [1, `task_num`]
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`
    """
    dets_ds = ImagePredictionDataset.from_csv(source_path=detections_csv, images_dir=images_dir)
    get_false_positive_dets_in_seqs(dets_ds,
                                    dataset_json,
                                    images_dir,
                                    sub_method,
                                    results_csv_path,
                                    num_tasks=num_tasks,
                                    task_num=task_num,
                                    IWildcamBaseImageDataset=IWildcam2021ImageDataset)


def run_detector_inference_on_ds(json_file_ds,
                                 out_predictions_csv,
                                 images_dir,
                                 detector_model_path,
                                 num_tasks,
                                 task_num,
                                 categories,
                                 exclude_cats,
                                 min_score_threshold):
    """Función para ejecutar la inferencia de un modelo de detección de objetos sobre las imágenes
    del dataset `json_file_ds` tipo COCO con las columnas por defecto especificadas en
    `IWildcam2021ImageDataset`. Las detecciones se guardan en el archivo CSV de predicción
    `out_predictions_csv`.
    Esta función deberá ejecutarse en un ambiente con TF1 instalado y debidamente configurado

    Parameters
    ----------
    json_file_ds : str
        Ruta del archivo JSON tipo COCO que contiene el dataset
    out_predictions_csv : str
        Ruta del archivo CSV de predicción que se genera
    images_dir : str
        Carpeta que contiene las imágenes del dataset
    detector_model_path : str
        Ruta del archivo que contiene los pesos del modelo de detección de objetos que se utilizará
        en la inferencia
    num_tasks : int
        En caso de realizar el procesamiento de forma paralela (p.e. en varios nodos), este es el
        número total de tareas en las que se divide el procesamiento. P.e., si se cuenta con 4
        nodos con 2 GPUs en cada uno, el trabajo se puede dividir en `num_tasks = 8` tareas
        paralelas, para así maximizar el uso de los recursos y realizar el procesamiento en el
        menor tiempo posible
    task_num : int
        En caso de realizar el procesamiento de forma paralela, este es el número de la tarea
        actual. Debe de ser un entero en el rango [1, `num_tasks`]
    categories : str
        Cadena para filtrar las categorías (separadas por coma) que tendrá el dataset
    exclude_cats : str
        Cadena para filtrar las categorías (separadas por coma) que se quieren quitar del dataset
    min_score_threshold : float
        Score mínimo que deberán tener las detecciones resultantes. El resto se descartarán
    """
    min_score_threshold = min_score_threshold or 0.3
    wcs_ds = IWildcam2021ImageDataset.from_json(source_path=json_file_ds,
                                           images_dir=images_dir,
                                           categories=categories,
                                           exclude_categories=exclude_cats)
    run_megadetector_inference(dataset=wcs_ds,
                               out_predictions_csv=out_predictions_csv,
                               images_dir=images_dir,
                               model_path=detector_model_path,
                               min_score_threshold=min_score_threshold,
                               include_id=True,
                               keep_image_id=True,
                               inherit_fields=[IWildcam2021ImageDataset.IMAGES_FIELDS.SEQ_ID,
                                               IWildcam2021ImageDataset.IMAGES_FIELDS.SEQ_FRAME_NUM,
                                               IWildcam2021ImageDataset.IMAGES_FIELDS.SEQ_NUM_FRAMES,
                                               IWildcam2021ImageDataset.IMAGES_FIELDS.LOCATION,
                                               IWildcam2021ImageDataset.IMAGES_FIELDS.DATE_CAPTURED],
                               num_tasks=num_tasks,
                               task_num=task_num,
                               num_gpus_per_node=NUM_GPUS_PER_NODE)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--visualize_classif_bboxes_in_ds", default=False, action="store_true")
    parser.add_argument("--test_create_file_for_competition", default=False, action="store_true")
    parser.add_argument("--generate_false_pos_dets_in_seqs", default=False, action="store_true")
    parser.add_argument("--run_detector_inference_on_ds", default=False, action="store_true")
    parser.add_argument("--create_json_for_geo_priors", default=False, action="store_true")
    parser.add_argument("--create_dummy_dets_csv", default=False, action="store_true")

    parser.add_argument('--detections_file', default=None, type=str)
    parser.add_argument('--json_filename', default=None, type=str)
    parser.add_argument('--dataset_json_file', default=None, type=str)
    parser.add_argument('--images_dir_dest', default=None, type=str)
    parser.add_argument("--categories", default=None, type=str)
    parser.add_argument("--exclude_cats", default=None, type=str)
    parser.add_argument('--classification_csv_file', default=None, type=str)
    parser.add_argument('--images_dir', default=None, type=str)
    parser.add_argument('--num_tasks', default=None, type=int, required=False)
    parser.add_argument('--task_num', default=None, type=int, required=False)
    parser.add_argument('--results_csv_path', default=None, type=str, help="")
    parser.add_argument('--false_pos_dets_csv', default=None, type=str, help="")
    parser.add_argument('--min_score_true_classifs', default=None, type=float, help="")
    parser.add_argument('--submission_file', default=None, type=str)
    parser.add_argument('--cats_file', default=None, type=str)
    parser.add_argument('--seq_id', default=None, type=str)
    parser.add_argument('--sub_method', default='Acc', type=str)
    parser.add_argument('--detector_model_path', default=None, type=str, required=False)
    parser.add_argument('--min_score_thresh', default=None, type=float)
    parser.add_argument('--wcs_ds_path', default=None, type=str)
    parser.add_argument('--wcs_location_json', default=None, type=str)
    parser.add_argument('--inat_17_18_images_json', default=None, type=str)
    parser.add_argument('--inat_17_18_categories', default=None, type=str)
    parser.add_argument('--inat_17_18_mapping_classes', default=None, type=str)
    parser.add_argument('--inat_17_images_json', default=None, type=str)
    parser.add_argument('--inat_18_images_json', default=None, type=str)
    parser.add_argument('--inat_17_location_json', default=None, type=str)
    parser.add_argument('--inat_18_location_json', default=None, type=str)
    parser.add_argument('--inat_21_dataset_json_file', default=None, type=str)
    parser.add_argument('--inat_21_categories', default=None, type=str)
    parser.add_argument('--inat_21_mapping_classes', default=None, type=str)
    parser.add_argument('--inat_21_images_json', default=None, type=str)
    parser.add_argument('--out_images_json', default=None, type=str)
    parser.add_argument('--labelmap_path', default=None, type=str)
    parser.add_argument("--add_label", type=str2bool, nargs='?', const=True, default=True)

    args = parser.parse_args()

    if args.visualize_classif_bboxes_in_ds:
        visualize_classif_bboxes_in_ds(dataset_json=args.json_filename,
                                       detections_csv=args.detections_file,
                                       classifs_csv=args.classification_csv_file,
                                       images_dir=args.images_dir,
                                       dest_dir=args.images_dir_dest,
                                       false_pos_dets_csv=args.false_pos_dets_csv,
                                       min_score_true_classifs=args.min_score_true_classifs,
                                       submission_file=args.submission_file,
                                       cats_file=args.cats_file,
                                       seq_id=args.seq_id)
    if args.test_create_file_for_competition:
        test_create_file_for_competition(classifs_on_detection_crops_csv=args.classification_csv_file,
                                         sample_submission_path=args.submission_file,
                                         wcs_image_level_anns_json_file=args.json_filename,
                                         seq_id=args.seq_id)
    if args.generate_false_pos_dets_in_seqs:
        generate_false_pos_dets_in_seqs(detections_csv=args.detections_file,
                                        dataset_json=args.json_filename,
                                        images_dir=args.images_dir,
                                        sub_method=args.sub_method,
                                        results_csv_path=args.results_csv_path,
                                        num_tasks=args.num_tasks,
                                        task_num=args.task_num)
    if args.run_detector_inference_on_ds:
        run_detector_inference_on_ds(json_file_ds=args.dataset_json_file,
                                     out_predictions_csv=args.results_csv_path,
                                     images_dir=args.images_dir,
                                     detector_model_path=args.detector_model_path,
                                     num_tasks=args.num_tasks,
                                     task_num=args.task_num,
                                     categories=args.categories,
                                     exclude_cats=args.exclude_cats,
                                     min_score_threshold=args.min_score_thresh)
    if args.create_dummy_dets_csv:
        create_dummy_detections_csv(json_file_ds=args.dataset_json_file,
                                    out_predictions_csv=args.results_csv_path,
                                    images_dir=args.images_dir,
                                    IWildcamBaseImageDataset=IWildcam2021ImageDataset)
    if args.create_json_for_geo_priors:
        create_json_for_geo_priors(wcs_ds_path=args.wcs_ds_path,
                                   wcs_location_json=args.wcs_location_json,
                                   inat_17_18_images_json=args.inat_17_18_images_json,
                                   inat_17_18_categories=args.inat_17_18_categories,
                                   inat_17_18_mapping_classes=args.inat_17_18_mapping_classes,
                                   inat_17_images_json=args.inat_17_images_json,
                                   inat_18_images_json=args.inat_18_images_json,
                                   inat_17_location_json=args.inat_17_location_json,
                                   inat_18_location_json=args.inat_18_location_json,
                                   inat_21_dataset_json_file=args.inat_21_dataset_json_file,
                                   inat_21_categories=args.inat_21_categories,
                                   inat_21_mapping_classes=args.inat_21_mapping_classes,
                                   inat_21_images_json=args.inat_21_images_json,
                                   out_images_json=args.out_images_json,
                                   labelmap_path=args.labelmap_path,
                                   add_label=args.add_label,
                                   exclude_cats=args.exclude_cats,
                                   IWildcamBaseImageDataset=IWildcam2021ImageDataset)
