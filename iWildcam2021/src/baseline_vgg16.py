#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import vgg16
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
from keras.layers import Dense
from keras.models import Model

from conabio_ml.utils.dataset_utils import write_labelmap_file, read_labelmap_file
from conabio_ml.datasets.dataset import Partitions
from conabio_ml_vision.datasets.datasets import ConabioImageDataset

from iWildcam2021.src.utils_2021 import *
from scripts.utils import *

"""En este script se utilizan los recuadros de las detecciones del Megadetector V3 sobre las
imágenes del conjunto WCS (dadas por la competición) para entrenar un modelo de clasificación
Vgg16 (la última parte de clasificación y las últimas N_TRAINABLE_LAYERS capas convolucionales).
Sobre el conjunto de prueba se ejecuta la inferencia del Megadetector V4 para obtener los
recuadros de cada imagen y se ejecuta la inferencia del modelo entrenado para clasificar cada
recuadro con las categorías requeridas por la competición.
Después, con las clasificaciones para cada recuadro del conjunto de prueba se aplica la siguiente
regla para calcular el número de individuos de cada categoría en cada secuencia:
Para cada secuencia se itera sobre cada categoría, y se toma el número máximo de recuadros de esa
categoría que aparezcan en cualquiera de las imágenes de la secuencia.
"""


def build_model(n_cats):
    base_model = vgg16.VGG16(weights="imagenet", include_top=False,
                                     input_shape=(IMG_WIDTH, IMG_HEIGHT, 3), pooling='avg')
    for layer in base_model.layers[:-N_TRAINABLE_LAYERS]:
        layer.trainable = False
    last_layer = base_model.get_layer(base_model.layers[-1].name)
    last_output = last_layer.output
    x = Dense(n_cats, activation='softmax', name='softmax')(last_output)
    new_model = Model(inputs=base_model.input, outputs=x)
    new_model.compile(Adam(lr=0.0001), loss='categorical_crossentropy', metrics=['accuracy'])
    return new_model


def train_and_eval_model(model_checkpoint_path,
                         dataset,
                         labelmap_path,
                         eval=False):
    df_train, df_test = dataset.get_splitted(partitions=[Partitions.TRAIN, Partitions.TEST])
    n_cats = dataset.get_num_categories()
    classes = dataset.get_classes()

    strategy = tf.distribute.MirroredStrategy()
    if not os.path.isfile(model_checkpoint_path):
        train_batches = ImageDataGenerator().flow_from_dataframe(df_train,
                                                                 x_col="item",
                                                                 y_col="label",
                                                                 classes=classes,
                                                                 target_size=(IMG_WIDTH,
                                                                              IMG_HEIGHT),
                                                                 batch_size=BATCH_SIZE_TRAIN)
        labelmap = {v: k for k, v in train_batches.class_indices.items()}
        write_labelmap_file(labelmap=labelmap, dest_path=labelmap_path)
        checkpointer = ModelCheckpoint(model_checkpoint_path)
        with strategy.scope():
            model = build_model(n_cats)
        history = model.fit(train_batches, epochs=N_EPOCHS, verbose=1, callbacks=[checkpointer])
    else:
        with strategy.scope():
            model = load_model(model_checkpoint_path)

    if eval:
        test_batches = ImageDataGenerator().flow_from_dataframe(df_test,
                                                                x_col="item",
                                                                y_col="label",
                                                                classes=classes,
                                                                target_size=(
                                                                    IMG_WIDTH, IMG_HEIGHT),
                                                                batch_size=BATCH_SIZE_EVAL,
                                                                shuffle=False)
        scores = model.evaluate(test_batches, batch_size=BATCH_SIZE_EVAL, verbose=1)
        print(scores)

    return model


experiment = "baseline_vgg16"
model_file_name = 'vgg16_train1.model.hdf5'

TRAIN_PERC = 0.8
BATCH_SIZE_TRAIN = 32
BATCH_SIZE_EVAL = 64
N_TRAINABLE_LAYERS = 5
N_EPOCHS = 20
SCORE_THRES = 0.3
IMG_WIDTH, IMG_HEIGHT = 224, 224
MEGADET_LABELMAP = ConabioImageDataset.MEGADETECTOR_V4_LABELMAP

repo_base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020"
base_path = os.path.join(repo_base_path, "iWildcam2021")
files_path = os.path.join(base_path, "files")
data_path = os.path.join(base_path, "data")
results_path = os.path.join(base_path, "results", experiment)

annotations_file = os.path.join(data_path, "WCS_metadata", "iwildcam2021_train_annotations.json")
sample_submission_path = os.path.join(data_path, "WCS_metadata", "sample_submission.csv")
wcs_test_images_dir = os.path.join(data_path, "WCS_images", "test")
wcs_train_images_dir = os.path.join(repo_base_path, "iWildcam2020", "data", "train")

detections_on_test_set_path = os.path.join(files_path, "detections_on_test_set_MegadetV4.csv")
test_crops_dir = os.path.join(files_path, "crops_of_test_set_MegadetV4")
labelmap_classif_file = os.path.join(files_path, "labels.txt")
wcs_dataset_json_file = os.path.join(files_path, "dataset_WCS_from_dets_MegadetV3.json")
result_crops_dir = os.path.join(files_path, "crops_of_train_dataset_MegadetV3")

result_dataset_path = os.path.join(results_path, "wcs_dataset.csv")
model_checkpoint_path = os.path.join(results_path, model_file_name)
classifs_on_crops_path = os.path.join(results_path, f"classifs_on_test_crops.csv")
final_csv = os.path.join(results_path, 'submission_2.csv')

os.makedirs(results_path, exist_ok=True)
os.makedirs(result_crops_dir, exist_ok=True)

wcs_dataset = create_wcs_dataset_from_json(dataset_json_file=wcs_dataset_json_file,
                                           images_dir=wcs_train_images_dir,
                                           train_perc=TRAIN_PERC,
                                           result_dataset_path=result_dataset_path,
                                           result_crops_dir=result_crops_dir,
                                           IWildcamBaseImageDataset=IWildcam2021ImageDataset)
model = train_and_eval_model(model_checkpoint_path=model_checkpoint_path,
                             dataset=wcs_dataset,
                             labelmap_path=labelmap_classif_file)
classifs_on_test_crops_ds = \
    classify_crops_in_test_set(detections_on_test_set_path=detections_on_test_set_path,
                               test_images_dir=wcs_test_images_dir,
                               crops_dir=test_crops_dir,
                               score_threshold=SCORE_THRES,
                               model=model,
                               labelmap_classification=read_labelmap_file(labelmap_classif_file),
                               classifs_on_test_crops_path=classifs_on_crops_path,
                               images_size=(IMG_WIDTH, IMG_HEIGHT),
                               batch_size=BATCH_SIZE_EVAL,
                               labelmap_detection=MEGADET_LABELMAP,
                               IWildcamBaseImageDataset=IWildcam2021ImageDataset)
create_file_for_competition(classifs_on_detection_crops_ds=classifs_on_test_crops_ds,
                            detections_on_test_set=detections_on_test_set_path,
                            test_images_dir=wcs_test_images_dir,
                            sample_submission_path=sample_submission_path,
                            wcs_image_level_anns_json_file=annotations_file,
                            out_csv_file=final_csv,
                            count_strategy='max_each_species')
