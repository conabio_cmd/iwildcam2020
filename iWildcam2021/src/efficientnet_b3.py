#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse

from tensorflow.keras.applications import EfficientNetB7

from conabio_ml.utils.dataset_utils import read_labelmap_file
from conabio_ml_vision.datasets.datasets import ConabioImageDataset

from scripts.utils import *
from iWildcam2021.src.utils_2021 import *

"""En este script se utilizan los recuadros de las detecciones del Megadetector V4 sobre las
imágenes del conjunto WCS para entrenar un modelo de clasificación EfficientNetB3 (sólo la última
parte de clasificación).
Sobre el conjunto de prueba se ejecuta la inferencia del Megadetector V4 para obtener los
recuadros de cada imagen y se ejecuta la inferencia del modelo entrenado para clasificar cada
recuadro con las categorías requeridas por la competición.
Después, con las clasificaciones para cada recuadro del conjunto de prueba se aplica la siguiente
regla para calcular el número de individuos de cada categoría en cada secuencia:
Para cada secuencia se calcula una etiqueta que será la más frecuente (moda) y con mayor score (en
caso de haber más de una), y esta etiqueta será la que se asignará a todos los bboxes de la
secuencia. El conteo de individuos se realiza tomando el número de recuadros mayor en una imagen de
la secuencia.
Se utiliza el modelo gro prior para utilizar la información geo espacial de las colecciones y tener
información adicional que ayuden a clasificar mejor las imágenes.
"""

experiment = "efficientnet_b7_2_iNat_17_18_21_geo_prior_train100_600"
num_submission = 20
add_sub_suffix = False

model_file_name = 'efficientnet_b7_train_1.model.hdf5'

TRAIN_PERC = 1.
BATCH_SIZE_TRAIN = 32
BATCH_SIZE_EVAL = 16
N_EPOCHS = 20
SCORE_THRES = 0.3
EFFIC_RESNET_SIZES = (600, 600)
MEGADET_LABELMAP = ConabioImageDataset.MEGADETECTOR_V4_LABELMAP

repo_base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020"
base_path = os.path.join(repo_base_path, "iWildcam2021")
files_path = os.path.join(base_path, "files")
data_path = os.path.join(base_path, "data")
results_path = os.path.join(base_path, "results", experiment)

inat_images = os.path.join(data_path, "iNat_images")
wcs_metadata = os.path.join(data_path, "WCS_metadata")
wcs_images = os.path.join(data_path, "WCS_images")
wcs_image_level_anns_json_file = os.path.join(wcs_metadata, "iwildcam2021_train_annotations.json")
wcs_test_info_json = os.path.join(wcs_metadata, "iwildcam2021_test_information.json")
sample_submission_path = os.path.join(wcs_metadata, "sample_submission.csv")
wcs_test_images_dir = os.path.join(wcs_images, "test_CLAHE_WB")
wcs_train_images_dir = os.path.join(wcs_images, "train_CLAHE_WB")
test_crops_dir = os.path.join(wcs_images, "crops_of_test_set_MegadetV4")
train_crops_dir = os.path.join(wcs_images, "crops_of_train_set_MegadetV4")
inat_17_18_train_images_dir = os.path.join(inat_images, "inaturalist_17_18_CLAHE_WB")
inat_21_train_images_dir = os.path.join(inat_images, "inaturalist_2021_CLAHE_WB")

files_dataset = os.path.join(files_path, "datasets")
files_dets = os.path.join(files_path, "detections")
files_fp = os.path.join(files_path, "false_positives")
files_iNat = os.path.join(files_path, "inat_cats")
files_gp = os.path.join(files_path, "geo_prior")
wcs_dataset_json_file = os.path.join(files_dataset, "wcs_from_dets_MegadetV4.json")
wcs_dataset_empty_json_file = os.path.join(files_dataset, "wcs_from_dets_empty_MegadetV4.json")
dataset_inat_21_json_file = os.path.join(files_dataset, "inat_21_from_dets_MegadetV4.json")
dataset_inat_17_18_json_file = os.path.join(files_dataset, "inat_17_18_curated.json")
detections_on_test_set_path = os.path.join(files_dets, "test_MegadetV4.csv")
false_pos_dets_test_csv = os.path.join(files_fp, "test_Acc_Thrs40.csv")
inat_17_18_cats_file = os.path.join(files_iNat, "common_cats_inat_17_18_wcs.txt")
inat_17_18_mapping_file = os.path.join(files_iNat, "map_similar_cats_inat_17_18_to_wcs.csv")
inat_21_cats_file = os.path.join(files_iNat, "common_cats_inat_21_wcs.txt")
inat_21_mapping_file = os.path.join(files_iNat, "map_similar_cats_inat_21_to_wcs.csv")
geo_prior_checkpoints_path = os.path.join(files_gp, "ckps_wcs_inat_17_18_21")
test_geo_prior_json = os.path.join(files_gp, "test_wcs_inat_17_18_21.json")
augmented_nightly_train_imgs_csv = os.path.join(files_dataset, "augm_nigh_train_crops.csv")

result_dataset_path = os.path.join(results_path, f"dataset.csv")
aux_dets_of_megadet_ds_path = os.path.join(results_path, f"aux_dets_of_megadet_ds.csv")
classifs_on_crops_path = os.path.join(results_path, f"classifs_on_test_crops_geo_priors_2.csv")
eval_results_dir = os.path.join(results_path, f"eval_test_part_results")
visualize_results_dir = os.path.join(results_path, "test_images_classifs_bboxes_seq_id")
model_checkpoint_path = os.path.join(results_path, model_file_name)
labelmap_classif_file = os.path.join(results_path, f"labels.txt")
submission_csv = os.path.join(results_path, f'submission_{num_submission}.csv')

os.makedirs(results_path, exist_ok=True)


def main(not_submission, eval, visualize_results):
    labelmap_classif = read_labelmap_file(labelmap_classif_file)
    dataset = create_combined_dataset(wcs_dataset_json_file=wcs_dataset_json_file,
                                      wcs_images_dir=wcs_train_images_dir,
                                      wcs_dataset_empty_json_file=wcs_dataset_empty_json_file,
                                      inat_17_18_dataset_json_file=dataset_inat_17_18_json_file,
                                      inat_17_18_images_dir=inat_17_18_train_images_dir,
                                      inat_17_18_categories=inat_17_18_cats_file,
                                      inat_17_18_mapping_classes=inat_17_18_mapping_file,
                                      inat_21_dataset_json_file=dataset_inat_21_json_file,
                                      inat_21_images_dir=inat_21_train_images_dir,
                                      inat_21_categories=inat_21_cats_file,
                                      inat_21_mapping_classes=inat_21_mapping_file,
                                      result_crops_dir=train_crops_dir,
                                      train_perc=TRAIN_PERC,
                                      result_dataset_path=result_dataset_path,
                                      IWildcamBaseImageDataset=IWildcam2021ImageDataset)
    model = train(model_checkpoint_path=model_checkpoint_path,
                  dataset=dataset,
                  labelmap_path=labelmap_classif_file,
                  model_type=EfficientNetB7, model_name="EfficientNetB7",
                  images_size=EFFIC_RESNET_SIZES,
                  epochs=N_EPOCHS,
                  batch_size=BATCH_SIZE_TRAIN)
    if not not_submission:
        classifs_on_test_crops_ds = \
            classify_crops_in_test_set(detections_on_test_set_path=detections_on_test_set_path,
                                       test_images_dir=wcs_test_images_dir,
                                       crops_dir=test_crops_dir,
                                       score_threshold=SCORE_THRES,
                                       model=model,
                                       labelmap_classification=labelmap_classif,
                                       classifs_on_test_crops_path=classifs_on_crops_path,
                                       aux_dets_of_megadet_ds_path=aux_dets_of_megadet_ds_path,
                                       images_size=EFFIC_RESNET_SIZES,
                                       batch_size=BATCH_SIZE_EVAL,
                                       labelmap_detection=MEGADET_LABELMAP,
                                       test_ds_json=wcs_test_info_json,
                                       false_positives_dets_csv=false_pos_dets_test_csv,
                                       geo_prior_checkpoints=geo_prior_checkpoints_path,
                                       test_geo_prior_json=test_geo_prior_json,
                                       IWildcamBaseImageDataset=IWildcam2021ImageDataset)
        create_file_for_competition(classifs_on_detection_crops_ds=classifs_on_test_crops_ds,
                                    sample_submission_path=sample_submission_path,
                                    wcs_image_level_anns_json_file=wcs_image_level_anns_json_file,
                                    out_csv_file=submission_csv,
                                    count_strategy='max_mode_sequence')
    if eval:
        eval_model(model=model,
                   dataset=dataset,
                   labelmap_path=labelmap_classif_file,
                   results_dir=eval_results_dir,
                   batch_size=BATCH_SIZE_EVAL)
    if visualize_results:
        visualize_classif_bboxes_in_ds(dataset_json=wcs_test_info_json,
                                       detections_csv=detections_on_test_set_path,
                                       classifs_csv=classifs_on_crops_path,
                                       images_dir=wcs_test_images_dir,
                                       dest_dir=visualize_results_dir,
                                       submission_file=submission_csv,
                                       cats_file=wcs_image_level_anns_json_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--not_submission", default=False, action="store_true")
    parser.add_argument("--eval", default=False, action="store_true")
    parser.add_argument("--visualize_results", default=False, action="store_true")
    args = parser.parse_args()

    main(not_submission=args.not_submission,
         eval=args.eval,
         visualize_results=args.visualize_results)
