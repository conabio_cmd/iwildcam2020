#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

from tensorflow.keras.applications import EfficientNetB3

from conabio_ml.utils.logger import get_logger

from scripts.utils import *
from iWildcam2021.src.utils_2021 import *

logger = get_logger(__name__)

experiment = "baseline-wo_CLAHE-wo_inat-wo_empty"
model_file_name_efficient_b3 = 'efficientnet_b3_train_1.model.hdf5'

TRAIN_PERC = 0.8
BATCH_SIZE_TRAIN = 32
N_EPOCHS = 20
EFFIC_B3_SIZES = (300, 300)

repo_base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020"
base_path = os.path.join(repo_base_path, "iWildcam2021")
files_path = os.path.join(base_path, "files")
data_path = os.path.join(base_path, "data")
results_path = os.path.join(base_path, "results", experiment)

wcs_images = os.path.join(data_path, "WCS_images")
wcs_train_images_dir = "/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2020/data/train/"
train_crops_dir = os.path.join(wcs_images, "crops_of_train_set_Md-wo_CLAHE-wo_inat-wo_empty")

files_dataset = os.path.join(files_path, "datasets")
wcs_dataset_json_file = os.path.join(files_dataset, "wcs_from_dets_MegadetV4-wo_CLAHE.json")

dataset_csv = os.path.join(results_path, f"dataset.csv")
labelmap_classif_file = os.path.join(results_path, f"labels.txt")

os.makedirs(results_path, exist_ok=True)


def main():
    model_name = "EfficientNetB3"
    dataset = create_wcs_dataset_from_json(dataset_json_file=wcs_dataset_json_file,
                                           images_dir=wcs_train_images_dir,
                                           train_perc=TRAIN_PERC,
                                           result_crops_dir=train_crops_dir,
                                           result_dataset_path=dataset_csv,
                                           IWildcamBaseImageDataset=IWildcam2021ImageDataset)
    _ = train(model_checkpoint_path=os.path.join(results_path, model_file_name_efficient_b3),
              dataset=dataset,
              labelmap_path=labelmap_classif_file,
              model_type=EfficientNetB3,
              model_name=model_name,
              images_size=EFFIC_B3_SIZES,
              epochs=N_EPOCHS,
              batch_size=BATCH_SIZE_TRAIN)


if __name__ == "__main__":
    main()
