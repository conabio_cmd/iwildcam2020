#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse

from tensorflow.keras.applications import EfficientNetB3, EfficientNetB7, ResNet152

from conabio_ml.utils.dataset_utils import read_labelmap_file
from conabio_ml_vision.datasets.datasets import ConabioImageDataset
from conabio_ml.utils.logger import get_logger

from scripts.utils import *
from iWildcam2021.src.utils_2021 import *

logger = get_logger(__name__)


experiment = "baseline-wo_CLAHE"

model_file_name_efficient_b3 = 'efficientnet_b3_train_1.model.hdf5'

TRAIN_PERC = 0.8
BATCH_SIZE_TRAIN = 32
N_EPOCHS = 20
EFFIC_B3_SIZES = (300, 300)

repo_base_path = "/LUSTRE/users/ecoinf_admin/iwildcam2020"
base_path = os.path.join(repo_base_path, "iWildcam2021")
files_path = os.path.join(base_path, "files")
data_path = os.path.join(base_path, "data")
results_path = os.path.join(base_path, "results", experiment)

inat_images = os.path.join(data_path, "iNat_images")
wcs_images = os.path.join(data_path, "WCS_images")
wcs_train_images_dir = os.path.join(repo_base_path, "iWildcam2020", "data", "train")
train_crops_dir = os.path.join(wcs_images, "crops_of_train_set_MegadetV4-wo_CLAHE")
inat_17_18_train_images_dir = os.path.join(inat_images, "inaturalist_17_18_CLAHE_WB")
inat_21_train_images_dir = os.path.join(inat_images, "inaturalist_2021_CLAHE_WB")

files_dataset = os.path.join(files_path, "datasets")
files_iNat = os.path.join(files_path, "inat_cats")
wcs_dataset_json_file = os.path.join(files_dataset, "wcs_from_dets_MegadetV4-wo_CLAHE.json")
wcs_dataset_empty_json_file = os.path.join(files_dataset, "wcs_from_dets_empty_MegadetV4-wo_CLAHE.json")
dataset_inat_21_json_file = os.path.join(files_dataset, "inat_21_from_dets_MegadetV4.json")
dataset_inat_17_18_json_file = os.path.join(files_dataset, "inat_17_18_curated.json")
inat_17_18_cats_file = os.path.join(files_iNat, "common_cats_inat_17_18_wcs.txt")
inat_17_18_mapping_file = os.path.join(files_iNat, "map_similar_cats_inat_17_18_to_wcs.csv")
inat_21_cats_file = os.path.join(files_iNat, "common_cats_inat_21_wcs.txt")
inat_21_mapping_file = os.path.join(files_iNat, "map_similar_cats_inat_21_to_wcs.csv")

result_dataset_path = os.path.join(results_path, f"dataset.csv")
labelmap_classif_file = os.path.join(results_path, f"labels.txt")

os.makedirs(results_path, exist_ok=True)

model_checkpoints = {
    'EfficientNetB3': os.path.join(results_path, model_file_name_efficient_b3)
}
model_base_instances = {
    'EfficientNetB3': EfficientNetB3,
}
model_input_sizes = {
    'EfficientNetB3': EFFIC_B3_SIZES
}


def main():
    dataset = create_combined_dataset(wcs_dataset_json_file=wcs_dataset_json_file,
                                      wcs_images_dir=wcs_train_images_dir,
                                      wcs_dataset_empty_json_file=wcs_dataset_empty_json_file,
                                      inat_17_18_dataset_json_file=dataset_inat_17_18_json_file,
                                      inat_17_18_images_dir=inat_17_18_train_images_dir,
                                      inat_17_18_categories=inat_17_18_cats_file,
                                      inat_17_18_mapping_classes=inat_17_18_mapping_file,
                                      inat_21_dataset_json_file=dataset_inat_21_json_file,
                                      inat_21_images_dir=inat_21_train_images_dir,
                                      inat_21_categories=inat_21_cats_file,
                                      inat_21_mapping_classes=inat_21_mapping_file,
                                      result_crops_dir=train_crops_dir,
                                      train_perc=TRAIN_PERC,
                                      result_dataset_path=result_dataset_path,
                                      IWildcamBaseImageDataset=IWildcam2021ImageDataset)
    model_name = 'EfficientNetB3'
    _ = train(model_checkpoint_path=model_checkpoints[model_name],
                    dataset=dataset,
                    labelmap_path=labelmap_classif_file,
                    model_type=model_base_instances[model_name],
                    model_name=model_name,
                    images_size=model_input_sizes[model_name],
                    epochs=N_EPOCHS,
                    batch_size=BATCH_SIZE_TRAIN)

if __name__ == "__main__":
    main()
