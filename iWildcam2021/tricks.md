
# Trucos

**Contar el número de secuencias vacías de submission_x**
```
import pandas as pd
sub = pd.read_csv("/Users/jlopez/Downloads/Files/submission_11.csv")
len([row["Id"] for _, row in sub.iterrows() if row[1:].sum() > 0])

# Obtener el máximo de la secuencia seq_id
seqs_counts = {row["Id"]: row[1:].max() for _, row in sub.iterrows()}
seqs_counts[seq_id]
```

**Sacar secuencias vacías de una entrega**
```
import pandas as pd
sub_x = pd.read_csv("/Users/jlopez/Downloads/Files/submission_8.csv")
empty_x = set([row["Id"] for _, row in sub_x.iterrows() if row[1:].sum() == 0])
len(empty_x)
```

**Sacar categorías comunes entre WCS y iNat**
```
import pandas as pd
from conabio_ml_vision.datasets.datasets import ConabioImageDataset

wcs_json_ds="/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2020/data/WCS_metadata/iwildcam2020_train_annotations.json"
inat_json_ds="/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2021/files/dataset_iNat_17_18_curated.json"

inat = ConabioImageDataset.from_json(inat_json_ds)
inat_cats = inat.data["label"].unique()

wcs = ConabioImageDataset.from_json(wcs_json_ds)
wcs_cats = wcs.data["label"].unique()

common_cats = list(set(wcs_cats) & set(inat_cats))

tuples = list()
for inat_cat in list(set(inat_cats)-set(common_cats)):
    for wcs_cat in wcs_cats:
        if len(set(inat_cat.split()) & set(wcs_cat.split())) > 0:
            tuples.append((inat_cat, wcs_cat))

res = {
    "inat_cats": [x[0] for x in tuples if x[0][0] == x[1][0]],
    "wcs_cats": [x[1] for x in tuples if x[0][0] == x[1][0]],
}
data = pd.DataFrame(res)
data.to_csv("especies_similares_wcs_inat.csv", index=False, header=True)
```

**Contar los individuos de cada especie para la secuencia `seq_id`**
```
import pandas as pd
import json

seq_id = "a92614e0-0cd3-11eb-bed1-0242ac1c0002"
sub = pd.read_csv("/Users/jlopez/Downloads/Files/submission_12.csv")
cats_json = json.load(open("/Users/jlopez/Downloads/Files/categories.json"))
cats_to_id = sorted(cats_json["categories"], key=lambda i: i["id"])[1:]
[f'{cats_to_id[i]["name"]}: {cat_counts}' for i, cat_counts in enumerate([x for x in sub[sub["Id"] == seq_id].iloc[0][1:]]) if cat_counts > 0]
```

**Quitar de un dataset de predicción las detecciones de imágenes de las clases 'unknown' y 'unidentifiable', 'misfire' con detecciones de "Animalia", 'motorcycle', 'start' o 'end' con detecciones de "Animalia"; esto para la colección WCS de la competición iWildcam2020**
```
from conabio_ml_vision.datasets.datasets import ImagePredictionDataset
dets_ds = ImagePredictionDataset.from_csv("/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2020/files/detections_on_train_set_MegadetV4.csv")
dets_df = dets_ds.as_dataframe()

anns_ds = IWildcam2020ImageDataset.from_json("/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2020/data/WCS_metadata/iwildcam2020_train_annotations.json")
anns_df = anns_ds.as_dataframe(columns=["image_id"])

un_im_ids = anns_df[(anns_df["label"] == "unknown") | (anns_df["label"] == "unidentifiable")]["image_id"].values
un_dets_ids = dets_df[dets_df["image_id"].isin(un_im_ids)]["id"].values

mis_im_ids = anns_df[(anns_df["label"] == "misfire")]["image_id"].values
mis_dets_ids = dets_df[(dets_df["image_id"].isin(mis_im_ids)) & (dets_df["label"] == "Animalia")]["id"].values

mot_ids = anns_df[(anns_df["label"] == "motorcycle") | (anns_df["label"] == "start") | (anns_df["label"] == "end")]["image_id"].values
mot_dets_ids = dets_df[(dets_df["image_id"].isin(mot_ids)) & (dets_df["label"] == "Animalia")]["id"].values

animals_ids = anns_df[(~anns_df["image_id"].isin(un_im_ids)) & (~anns_df["image_id"].isin(mis_im_ids)) & (~anns_df["image_id"].isin(mot_ids))]["image_id"].values
wrong_animals_dets_ids = dets_df[(dets_df["image_id"].isin(animals_ids)) & (dets_df["label"] != "Animalia")]["id"].values

dets_ds.data = dets_df[(~dets_df["id"].isin(un_dets_ids)) & (~dets_df["id"].isin(mis_dets_ids)) & (~dets_df["id"].isin(mot_dets_ids)) & (~dets_df["id"].isin(wrong_animals_dets_ids))]
dets_ds.data = dets_ds.data.reset_index(drop=True)
dets_ds.to_csv("/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2020/files/detections_on_train_set_MegadetV4.csv")
```

**Curar las secuencias en un dataset de detección respecto a las secuencias en un archivo JSON**
```
import pandas as pd
from conabio_ml_vision.datasets.datasets import ImagePredictionDataset
from iWildcam2020.scripts.utils_2020 import IWildcam2020ImageDataset
ds = IWildcam2020ImageDataset.from_json("/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2020/data/WCS_metadata/iwildcam2020_test_information_curated_seq_id.json")
df = ds.as_dataframe(columns=["image_id", "seq_id", "seq_num_frames", "frame_num"])[["image_id", "seq_id", "seq_num_frames", "frame_num"]]

dets = ImagePredictionDataset.from_csv("/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2020/files/detections_on_test_set_MegadetV4.csv")
dets_df = dets.as_dataframe()
dets_df = dets_df.drop(columns=["seq_id", "seq_num_frames", "frame_num"], axis=1)

new = pd.merge(left=dets_df, right=df, left_on='image_id', right_on='image_id')
new.to_csv("/LUSTRE/users/ecoinf_admin/iwildcam2020/iWildcam2020/files/detections_on_test_set_MegadetV4_curated_seq_id_2.csv", header=True, index=False)
```