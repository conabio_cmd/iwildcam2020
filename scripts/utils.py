#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import json
import pandas as pd
import argparse
from glob import glob
import cv2
from shutil import copyfile, rmtree
from datetime import datetime
import numpy as np
from math import floor, isnan
from collections import defaultdict
import multiprocessing
from multiprocessing import Manager

try:
    import tensorflow as tf
    from keras.callbacks import ModelCheckpoint
    from keras.models import load_model
    from keras.preprocessing.image import ImageDataGenerator
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.layers.experimental import preprocessing
    from keras.optimizers import Adam
    from keras.layers import (
        Dense,
        Input,
        GlobalAveragePooling2D,
        BatchNormalization,
        Dropout,
    )
    from keras.models import Model
except:
    pass

from conabio_ml.datasets.dataset import Partitions
from conabio_ml.utils.dataset_utils import (
    read_labelmap_file,
    write_labelmap_file,
)
from conabio_ml.utils.utils import create_shared_defaultdict, get_chunk
from conabio_ml.utils.logger import get_logger

from conabio_ml_vision.datasets.datasets import ConabioImageDataset
from conabio_ml_vision.datasets.datasets import ImagePredictionDataset
from conabio_ml_vision.evaluator.evaluator import ImageClassificationEvaluator
from conabio_ml_vision.evaluator.evaluator import ImageClassificationMetrics
from conabio_ml_vision.utils.images_utils import draw_bboxes_in_image

logger = get_logger(__name__)


EMPTY_CAT_ID = 0
MEGADET_LABELMAP = ConabioImageDataset.MEGADETECTOR_V4_LABELMAP
MAX_SCORE_FALSE_POS = 0.9
GREEN = (0, 255, 0)
RED = (0, 0, 255)
ORANGE = (0, 188, 255)
STD_DATEFORMAT = "%Y-%m-%d %H:%M:%S.000"
OUT_DATEFORMAT = "%Y-%m-%d %H:%M:%S+00:00"


# region Datasets creation


def create_wcs_dataset_from_json(
    dataset_json_file,
    images_dir,
    train_perc,
    result_crops_dir,
    dataset_empty_json_file=None,
    result_dataset_path=None,
    IWildcamBaseImageDataset=None,
):
    """Función que crea un dataset de tipo `classification` a partir del archivo JSON de tipo COCO
    `dataset_json_file` con las columnas por defecto del dataset especificado en `IWildcamBaseImageDataset`,
    recortando en las imágenes que están en la carpeta `images_dir` los recuadros de las
    anotaciones y guardando las imágenes generadas en la ruta `result_crops_dir`.
    El dataset es particionado con `train_perc` para train y `1-train_perc` para test.
    Al terminar se guarda el dataset como archivo CSV en la ruta `result_dataset_path`.

    Parameters
    ----------
    dataset_json_file : str
        Ruta del archivo JSON tipo COCO con las anotaciones del dataset.
        P.e. `files/dataset_WCS_from_dets_MegadetV4_CLAHE_WB.json`
    images_dir : str
        Carpeta donde están las imágenes originales del dataset. P.e. `iWildcam2020/data/train`
    train_perc : float
        Porcentaje de la partición de `train`. A la de `test` se le asigna `1-train_perc`
    result_crops_dir : str
        Ruta de la carpeta donde se guardarán las imágenes recortadas de las anotaciones.
        P.e. `results/crops_of_train_dataset`
    dataset_empty_json_file : str, optional
        Ruta del archivo JSON tipo COCO con las anotaciones vacías ("empty") del dataset.
        P.e. `files/dataset_WCS_from_dets_empty_MegadetV4_CLAHE_WB.json`
    result_dataset_path : str, optional
        Ruta del archivo CSV donde se guardará el dataset. P.e. `results/dataset.csv`
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`

    Returns
    -------
    IWildcam2021ImageDataset
        Dataset de tipo clasificación cuyos elementos se forman a partir de las anotaciones a nivel
        de objeto de un archivo JSON de tipo COCO, y las imágenes se recortan se forman a partir de
        los bounding boxes de dichas anotaciones.
    """
    if result_dataset_path is not None and os.path.isfile(result_dataset_path):
        return IWildcamBaseImageDataset.from_csv(
            source_path=result_dataset_path,
            split_by_column="partition",
            images_dir=result_crops_dir,
        )
    inherit_fields = [
        "image_id",
        IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_ID,
        IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_FRAME_NUM,
        IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_NUM_FRAMES,
        IWildcamBaseImageDataset.IMAGES_FIELDS.LOCATION,
        IWildcamBaseImageDataset.IMAGES_FIELDS.DATE_CAPTURED,
    ]
    wcs_det_ds = IWildcamBaseImageDataset.from_json(
        source_path=dataset_json_file,
        images_dir=images_dir,
        exclude_corrupted_images=True,
    )
    wcs_classif_ds = wcs_det_ds.create_classif_ds_from_bboxes_crops(
        dest_path=result_crops_dir,
        include_id=True,
        inherit_fields=inherit_fields,
    )

    if dataset_empty_json_file is not None:
        wcs_empty_det_ds = IWildcamBaseImageDataset.from_json(
            source_path=dataset_empty_json_file,
            images_dir=images_dir,
            allow_bboxes_with_label_empty=True,
        )
        wcs_empty_classif_ds = (
            wcs_empty_det_ds.create_classif_ds_from_bboxes_crops(
                dest_path=result_crops_dir,
                inherit_fields=inherit_fields,
                allow_label_empty=True,
            )
        )
        wcs_classif_ds = IWildcamBaseImageDataset.from_datasets(
            wcs_classif_ds, wcs_empty_classif_ds
        )
        wcs_classif_ds.set_images_dir(result_crops_dir)

    wcs_classif_ds.split(
        train_perc=train_perc,
        val_perc=0,
        test_perc=1.0 - train_perc,
        group_by_field="location",
    )

    if result_dataset_path is not None:
        wcs_classif_ds.to_csv(dest_path=result_dataset_path)

    return wcs_classif_ds


def create_inat_dataset_from_json(
    dataset_json_file,
    images_dir,
    result_crops_dir,
    categories=[],
    mapping_classes=None,
    train_perc=0.8,
):
    """Función que crea un dataset de tipo `classification` a partir del archivo JSON de tipo COCO
    `dataset_json_file` con las columnas por defecto del dataset especificado en
    `ConabioImageDataset`, recortando en las imágenes que están en la carpeta `images_dir`
    los recuadros de las anotaciones y guardando las imágenes generadas en la ruta
    `result_crops_dir`. El dataset es particionado con `train_perc` para train y `1-train_perc`
    para test. Al terminar se guarda el dataset como archivo CSV en la ruta `result_dataset_path`.

    Parameters
    ----------
    dataset_json_file : str
        Ruta del archivo JSON tipo COCO con las anotaciones del dataset.
        P.e. `files/dataset_iNat_21_from_dets.json`
    images_dir : str
        Carpeta donde están las imágenes originales del dataset.
        P.e. `data/trainiNat_images/inaturalist_2021_CLAHE_WB`
    result_crops_dir : str
        Ruta de la carpeta donde se guardarán las imágenes recortadas de las anotaciones.
        P.e. `results/crops_of_train_dataset`
    categories : list of str or str, optional
        List of categories to filter images, or path of a CSV file or a text file.
        If empty list, all categories will be included.
        If it is a CSV file, it should have the categories in the first column and should not
        have a header. If it is a text file, it must have the categories separated by a line
        break. If None, labeled images will not be included. (default is [])
    mapping_classes : dict or str, optional
        Dictionary or path to a CSV file containing the mappings of classes, by default None
    train_perc : float, optional
        Porcentaje de la partición de `train`. A la de `test` se le asigna `1-train_perc`

    Returns
    -------
    ConabioImageDataset
        Dataset de tipo clasificación cuyos elementos se forman a partir de las anotaciones a nivel
        de objeto de un archivo JSON de tipo COCO, y las imágenes se recortan se forman a partir de
        los bounding boxes de dichas anotaciones.
    """
    inat_ds = ConabioImageDataset.from_json(
        source_path=dataset_json_file,
        images_dir=images_dir,
        categories=categories,
    )
    if mapping_classes is not None:
        inat_ds.map_categories(mapping_classes)
    inat_classif_ds = inat_ds.create_classif_ds_from_bboxes_crops(
        dest_path=result_crops_dir
    )

    inat_classif_ds.split(
        train_perc=train_perc, val_perc=0, test_perc=1.0 - train_perc
    )

    return inat_classif_ds


def create_combined_dataset(
    wcs_dataset_json_file,
    wcs_images_dir,
    wcs_dataset_empty_json_file,
    inat_17_18_dataset_json_file,
    inat_17_18_images_dir,
    inat_17_18_categories,
    inat_17_18_mapping_classes,
    inat_21_dataset_json_file,
    inat_21_images_dir,
    inat_21_categories,
    inat_21_mapping_classes,
    result_crops_dir,
    train_perc,
    result_dataset_path,
    augmented_dataset_csv=None,
    IWildcamBaseImageDataset=None,
):
    """Función que crea un dataset combinado a partir de datasets de las colecciones WCS,
    iNat 2017, iNat 2018 y iNat 2021.

    Parameters
    ----------
    wcs_dataset_json_file : str
        Ruta del archivo JSON tipo COCO con las anotaciones del dataset WCS
    wcs_images_dir : str
        Carpeta donde están las imágenes originales del dataset WCS
    wcs_dataset_empty_json_file : str
        Ruta del archivo JSON tipo COCO con las anotaciones vacías ("empty") del dataset WCS
    inat_17_18_dataset_json_file : str
        Ruta del archivo JSON tipo COCO con las anotaciones del dataset iNat 2017-2018
    inat_17_18_images_dir : str
        Carpeta donde están las imágenes originales del dataset iNat 2017-2018
    inat_17_18_categories : str
        Ruta del archivo de texto con las categorías que se van a filtrar del dataset iNat
        2017-2018. Por lo general son las categorías comunes con el dataset WCS, pero también puede
        incluir nombres de categorías que no están en WCS, pero que posteriormente se mapearán con
        el parámetro `inat_17_18_mapping_classes`
    inat_17_18_mapping_classes : str
        Ruta del archivo CSV que mapeará los nombres de las categorías en el dataset iNat
        2017-2018 a los nombres de las categorías en el dataset WCS
    inat_21_dataset_json_file : str
        Ruta del archivo JSON tipo COCO con las anotaciones del dataset iNat 2021
    inat_21_images_dir : str
        Carpeta donde están las imágenes originales del dataset iNat 2021
    inat_21_categories : str
        Ruta del archivo de texto con las categorías que se van a filtrar del dataset iNat
        2021. Por lo general son las categorías comunes con el dataset WCS, pero también puede
        incluir nombres de categorías que no están en WCS, pero que posteriormente se mapearán con
        el parámetro `inat_21_mapping_classes`
    inat_21_mapping_classes : str
        Ruta del archivo CSV que mapeará los nombres de las categorías en el dataset iNat
        2021 a los nombres de las categorías en el dataset WCS
    result_crops_dir : str
        Ruta de la carpeta donde se guardarán las imágenes recortadas de las anotaciones.
        P.e. `results/crops_of_train_dataset`
    train_perc : float
        Porcentaje de la partición de `train`. A la de `test` se le asigna `1-train_perc`
    result_dataset_path : str
        Ruta del archivo CSV que se creará para contener los registros del dataset combinado
    augmented_dataset_csv : str
        Ruta del archivo CSV de un dataset que se usará como aumento de datos al dataset combinado
        de WCS y iNat
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`

    Returns
    -------
    IWildcam2021ImageDataset
        Dataset combinado de los datasets WCS, iNat 2017, iNat 2018 y iNat 2021
    """
    if os.path.isfile(result_dataset_path):
        return IWildcamBaseImageDataset.from_csv(
            source_path=result_dataset_path,
            split_by_column="partition",
            images_dir=result_crops_dir,
        )
    wcs_ds = create_wcs_dataset_from_json(
        dataset_json_file=wcs_dataset_json_file,
        images_dir=wcs_images_dir,
        train_perc=train_perc,
        result_crops_dir=result_crops_dir,
        dataset_empty_json_file=wcs_dataset_empty_json_file,
        IWildcamBaseImageDataset=IWildcamBaseImageDataset,
    )
    inat_17_18_ds = create_inat_dataset_from_json(
        dataset_json_file=inat_17_18_dataset_json_file,
        images_dir=inat_17_18_images_dir,
        result_crops_dir=result_crops_dir,
        categories=inat_17_18_categories,
        mapping_classes=inat_17_18_mapping_classes,
        train_perc=train_perc,
    )
    inat_21_ds = create_inat_dataset_from_json(
        dataset_json_file=inat_21_dataset_json_file,
        images_dir=inat_21_images_dir,
        result_crops_dir=result_crops_dir,
        categories=inat_21_categories,
        mapping_classes=inat_21_mapping_classes,
        train_perc=train_perc,
    )
    ds = IWildcamBaseImageDataset.from_datasets(wcs_ds, inat_17_18_ds, inat_21_ds)
    if augmented_dataset_csv is not None:
        augmented_ds = IWildcamBaseImageDataset.from_csv(
            source_path=augmented_dataset_csv,
            split_by_column="partition",
            images_dir=result_crops_dir,
        )
        ds = IWildcamBaseImageDataset.from_datasets(ds, augmented_ds)
    ds.set_images_dir(result_crops_dir)
    ds.to_csv(dest_path=result_dataset_path)

    return ds


def create_complete_imgs_dataset(
    wcs_dataset_json_file,
    wcs_images_dir,
    train_perc,
    result_dataset_path,
    IWildcamBaseImageDataset=None,
):
    """Función que crea un dataset con las imágenes completas de la colección WCS

    Parameters
    ----------
    wcs_dataset_json_file : str
        Ruta del archivo JSON que contiene el dataset con anotaciones a nivel de imagen del
        conjunto de entrenamiento de la colección WCS
    wcs_images_dir : str
        Ruta de la carpeta que contiene las imágenes del conjunto de entrenamiento de la colección
        WCS
    train_perc : float
        Proporción de la partición de entrenamiento del dataset
    result_dataset_path : str
        Ruta del archivo CSV creado del dataset
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`

    Returns
    -------
    ImageDataset
        Instancia del dataset creado
    """
    if os.path.isfile(result_dataset_path):
        return IWildcamBaseImageDataset.from_csv(
            source_path=result_dataset_path,
            split_by_column="partition",
            images_dir=wcs_images_dir,
        )
    FLDS = IWildcamBaseImageDataset.IMAGES_FIELDS
    wcs_classif_ds = IWildcamBaseImageDataset.from_json(
        source_path=wcs_dataset_json_file, images_dir=wcs_images_dir
    )
    wcs_classif_ds.split(
        train_perc=train_perc,
        val_perc=0,
        test_perc=1.0 - train_perc,
        group_by_field="location",
    )

    wcs_classif_ds.to_csv(
        dest_path=result_dataset_path,
        columns=[
            "image_id",
            FLDS.SEQ_ID,
            FLDS.SEQ_FRAME_NUM,
            FLDS.SEQ_NUM_FRAMES,
            FLDS.LOCATION,
            FLDS.DATE_CAPTURED,
        ],
    )
    return wcs_classif_ds


def create_dummy_detections_csv(
    json_file_ds, out_predictions_csv, images_dir, IWildcamBaseImageDataset=None
):
    """Función que crea un dataset dummy de detección a partir de las imágenes del dataset
    `json_file_ds`, asumiendo que el bbox abarca toda la imagen, que tiene la etiqueta 'Animalia' y
    score 1.

    Parameters
    ----------
    json_file_ds : str
        Ruta del archivo JSON tipo COCO que contiene la información con la que se va a construir
        el dataset
    out_predictions_csv : str
        Ruta del archivo CSV generado para el dataset de predicción
    images_dir :  str
        Ruta de la carpeta que contiene las imágenes del dataset
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`
    """
    wcs_ds = IWildcamBaseImageDataset.from_json(
        source_path=json_file_ds, images_dir=images_dir
    )
    FLDS = IWildcamBaseImageDataset.IMAGES_FIELDS
    wcs_df = wcs_ds.as_dataframe(
        columns=[
            FLDS.SEQ_ID,
            FLDS.SEQ_FRAME_NUM,
            FLDS.SEQ_NUM_FRAMES,
            FLDS.LOCATION,
            FLDS.DATE_CAPTURED,
            FLDS.WIDTH,
            FLDS.HEIGHT,
            FLDS.ID,
            "image_id",
        ]
    )
    results = defaultdict(list)
    for _, row in wcs_df.iterrows():
        results["item"].append(row["item"])
        results["label"].append("Animalia")
        results["bbox"].append(f"0,0,{row[FLDS.WIDTH]},{row[FLDS.HEIGHT]}")
        results["score"].append(1.0)
        results["image_id"].append(row["image_id"])
        results[FLDS.SEQ_ID].append(row[FLDS.SEQ_ID])
        results[FLDS.SEQ_FRAME_NUM].append(row[FLDS.SEQ_FRAME_NUM])
        results[FLDS.SEQ_NUM_FRAMES].append(row[FLDS.SEQ_NUM_FRAMES])
        results[FLDS.LOCATION].append(row[FLDS.LOCATION])
        results[FLDS.DATE_CAPTURED].append(row[FLDS.DATE_CAPTURED])
        results[FLDS.ID].append(row["image_id"])
    data = pd.DataFrame(results)
    prediction_dataset = ImagePredictionDataset(
        data, info={}, images_dir=images_dir
    )
    prediction_dataset.to_csv(dest_path=out_predictions_csv)


def create_wcs_json_from_dets_and_anns(
    detections_file,
    annotations_file,
    include_empty,
    output_json_name,
    bboxes_min_longest_side=1,
    max_diff_greater_conf=1.0,
):
    """Función que genera un archivo JSON de tipo COCO con anotaciones a nivel de objeto a partir
    de dos archivos JSON: Uno con las predicciones de un detector de objetos (P.e. Megadetector)
    con 'anotaciones' a nivel de objeto y con clases genericas (p.e. Animal, Person) dado en
    `detections_file`, y otro con anotaciones a nivel de imagen y con clases epecíficas
    (p.e. Canis Latrans) dado en `annotations_file`.
    Para cada imagen se toman los bounding boxes del detector de objetos y a cada uno se le asigna
    la clase dada a nivel de imagen, para formar anotaciones a nivel de objeto con clases
    específicas. El archivo final se guardará en la ruta dada por `output_json_name`.

    Parameters
    ----------
    detections_file : str
        Ruta del archivo JSON con las predicciones de un detector de objetos (Megadetector).
        P.e. `WCS_metadata/iwildcam2021_megadetector_results.json`
    annotations_file : str
        Ruta del archivo JSON con las anotaciones a nivel de imagen y con clases específicas.
        P.e. `WCS_metadata/iwildcam2021_train_annotations.json`
    include_empty : bool
        Bandera que indica si se agregarán anotaciones vacías o no. La anotación vacía no tendrá
        campo `bbox` y su `id` será `EMPTY_CAT_ID = 0`
    output_json_name : str
        Nombre del archivo JSON resultante.
        P.e. `results_path/dataset_WCS_from_dets_MegadetV3.json`
    bboxes_min_longest_side : int, optional
        Tamaño mínimo del lado mayor de los bboxes que se tomarán en cuenta, by default 1
    max_diff_greater_conf : float, optional
        Diferencia máxima de las detecciones respecto a la detección más alta de una misma imagen
        para ser tomadas en cuenta, by default 1.
    """
    detections_data = json.load(open(detections_file))
    annotations_data = json.load(open(annotations_file))
    images_to_ann = dict()
    images_dict = dict()
    categories_dict = dict()

    for ann in annotations_data["annotations"]:
        images_to_ann[str(ann["image_id"])] = ann
    for image in annotations_data["images"]:
        images_dict[str(image["id"])] = image
    for category in annotations_data["categories"]:
        categories_dict[category["id"]] = category["name"]
    anns_json = []

    def _insert_empty(id, image_id):
        anns_json.append(
            {"id": id, "image_id": image_id, "category_id": EMPTY_CAT_ID}
        )

    for image_det in detections_data["images"]:
        image_id = image_det["id"]
        if image_id not in images_to_ann:  # Test images
            continue
        image = images_dict[image_id]
        detections = image_det["detections"]
        annotation = images_to_ann[image_id]
        category_id = annotation["category_id"]

        ann_inserted = False

        if category_id == EMPTY_CAT_ID and include_empty:
            _insert_empty(annotation["id"], image_id)
        else:
            max_conf = max([x["conf"] for x in detections])
            for i, det in enumerate(detections):
                (x, y, w, h) = det["bbox"]
                (height, width) = image["height"], image["width"]
                (x, y, w, h) = (
                    floor(x * width),
                    floor(y * height),
                    floor(w * width),
                    floor(h * height),
                )
                if max(w, h) < bboxes_min_longest_side:
                    continue
                if max_conf - det["conf"] > max_diff_greater_conf:
                    continue
                bbox = [x, y, w, h]
                anns_json.append(
                    {
                        "id": f"{annotation['id']}-{det.get('id', str(i).zfill(3))}",
                        "image_id": image_id,
                        "category_id": category_id,
                        "bbox": bbox,
                    }
                )
                ann_inserted = True
            if not ann_inserted and include_empty:
                _insert_empty(annotation["id"], image_id)
    categories = [{"id": k, "name": v} for k, v in categories_dict.items()]
    json_data = {
        "info": annotations_data.get("info", {}),
        "images": annotations_data["images"],
        "categories": categories,
        "annotations": anns_json,
    }
    with open(
        output_json_name,
        "w",
    ) as outfile:
        json.dump(json_data, outfile)


def create_wcs_json_from_detections_csv(
    detections_csv_file,
    annotations_file,
    images_dir,
    output_json_name,
    aux_json_detections_file=None,
    false_pos_dets_csv=None,
    max_score_false_pos=MAX_SCORE_FALSE_POS,
    min_score_detections=0.3,
    categories=None,
):
    """Función que genera un archivo JSON de tipo COCO con anotaciones a nivel de objeto a partir
    de dos archivos: Un archivo CSV con las predicciones de un detector de objetos (P.e.
    Megadetector) con 'anotaciones' a nivel de objeto y con clases genericas (p.e. Animal, Person)
    dado en `detections_csv_file`, y otro con anotaciones a nivel de imagen y con clases epecíficas
    (p.e. Canis Latrans) dado en `annotations_file`.
    Para cada imagen se toman los bounding boxes del detector de objetos y a cada uno se le asigna
    la clase dada a nivel de imagen, para formar anotaciones a nivel de objeto con clases
    específicas. El archivo final se guardará en la ruta dada por `output_json_name`.

    Parameters
    ----------
    detections_csv_file : str
        Ruta del archivo CSV con las predicciones de un detector de objetos (Megadetector).
        P.e. `files/detections_on_train_set_MegadetV4.csv`
    annotations_file : str
        Ruta del archivo JSON con las anotaciones a nivel de imagen y con clases específicas.
        P.e. `WCS_metadata/iwildcam2021_train_annotations.json`
    images_dir : str
        Carpeta donde están las imágenes originales del dataset. P.e. `iWildcam2020/data/train`
    output_json_name : str
        Nombre del archivo JSON resultante.
        P.e. `results_path/dataset_WCS_from_dets_MegadetV3.json`
    aux_json_detections_file : str
        Ruta del archivo JSON auxiliar con las detecciones. Si es None se crea el archivo en la
        ruta actual con el nombre `__aux.json ` y se elimina al terminar la ejecución
    false_pos_dets_csv : str, optional
        Ruta del archivo CSV que contiene las detecciones que son consideradas falsos positivos.
        Debe contener las columnas `seq_ids` y `det_ids`. By default None
    max_score_false_pos : float, optional
        Score máximo que podrán tener las detecciones que se consideran falsos positivos para que
        sean filtradas, by default `MAX_SCORE_FALSE_POS`
    min_score_detections : float, optional
        Valor de score mínimo de las detecciones que se van a considerar. El resto se descartan
    categories : str
        Cadena para filtrar las categorías (separadas por coma) que serán consideradas en el
        dataset representado por `detections_csv_file` (P.e., 'Animalia')
    """
    detections_ds = ImagePredictionDataset.from_csv(
        source_path=detections_csv_file,
        images_dir=images_dir,
        labelmap=MEGADET_LABELMAP,
    )
    min_score_detections = min_score_detections or 0.3
    if categories is not None:
        categories = categories.split(",")
        detections_ds.data = detections_ds.data.loc[
            detections_ds.data["label"].isin(categories)
        ]
        detections_ds.data = detections_ds.data.reset_index(drop=True)

    if false_pos_dets_csv is not None:
        false_pos = pd.read_csv(false_pos_dets_csv, header=0)
        false_pos_dets_ids = false_pos["det_ids"].values
        detections_ds.data = detections_ds.data.loc[
            (~detections_ds.data["id"].isin(false_pos_dets_ids))
            | (detections_ds.data["score"] >= max_score_false_pos)
        ]
        detections_ds.data = detections_ds.data.reset_index(drop=True)

    _aux_json_detections_file = aux_json_detections_file or "./__aux.json"
    detections_ds.to_json(
        dest_path=_aux_json_detections_file,
        score_threshold=min_score_detections,
        add_detection_id=True,
    )
    create_wcs_json_from_dets_and_anns(
        detections_file=_aux_json_detections_file,
        annotations_file=annotations_file,
        include_empty=False,
        output_json_name=output_json_name,
    )
    if aux_json_detections_file is None:
        os.remove(_aux_json_detections_file)


# endregion

# region Training classifiers


def build_model(num_classes, model_type, name, input_shape):
    """Función para construir y compilar el modelo de `model_type`, aplicando operaciones de
    aumento de datos y haciendo transfer learning del modelo entrenado con el conjunto ImageNet

    Parameters
    ----------
    num_classes : int
        Número de elementos en la capa de salida "softmax"
    model_type : keras.models.Model
        Instancia del modelo base que se usará para hacer Transfer Learning
    name : str
        Nombre que se le dará al modelo
    input_shape : (int, int, int)
        Forma que tendrán los elementos de entrada. Se da en una tupla con la resolución de las
        imágenes de entrada del modelo, considerando 3 canales. P.e., (224, 224, 3)

    Returns
    -------
    keras.models.Model
        Instancia del modelo construído, que podrá ser utilizado para realizar el entrenamiento
    """
    img_augmentation = Sequential(
        [
            preprocessing.RandomRotation(factor=0.15),  # [-54°, 54°]
            preprocessing.RandomTranslation(
                height_factor=0.1, width_factor=0.1
            ),
            preprocessing.RandomFlip(mode="horizontal"),
            preprocessing.RandomContrast(factor=0.1),
        ],
        name="img_augmentation",
    )

    inputs = Input(shape=input_shape)
    x = img_augmentation(inputs)
    model = model_type(include_top=False, input_tensor=x, weights="imagenet")

    # Freeze the pretrained weights
    model.trainable = False

    # Rebuild top
    x = GlobalAveragePooling2D(name="avg_pool")(model.output)
    x = BatchNormalization()(x)

    top_dropout_rate = 0.2
    x = Dropout(top_dropout_rate, name="top_dropout")(x)
    outputs = Dense(num_classes, activation="softmax", name="pred")(x)

    # Compile
    model = Model(inputs, outputs, name=name)
    optimizer = Adam(learning_rate=1e-2)
    model.compile(
        optimizer=optimizer,
        loss="categorical_crossentropy",
        metrics=["accuracy"],
    )
    return model


def train(
    model_checkpoint_path,
    dataset,
    labelmap_path,
    model_type,
    model_name,
    images_size,
    epochs,
    batch_size,
):
    """Función que realiza el entrenamiento de un modelo con el conjunto de imágenes contenidas en
    `dataset`

    Parameters
    ----------
    model_checkpoint_path : str
        Ruta de la carpeta donde se guardarán los archivos de checkpoint del entrenamiento
    dataset : ImageDataset
        Instacia de un dataset de imágenes con al menos la partición de `TRAIN`
    labelmap_path : str
        Ruta del archivo de labelmap que se generará para el modelo, y que podrá ser utilizado
        porteriormente para hacer la clasificación
    model_type : keras.models.Model
        Instancia del modelo base que se usará para hacer Transfer Learning
    model_name : str
        Nombre que se le dará al modelo
    images_size : (int, int)
        Resolución de las imágenes de entrada del modelo. P.e., (224, 224)
    epochs : int
        Número de épocas del entrenamiento
    batch_size : int
        Tamaño del batch de entrenamiento

    Returns
    -------
    keras.models.Model
        Instancia del modelo entrenado que podrá ser utilizado para hacer la inferencia sobre un
        conjunto de imágenes
    """
    if not os.path.isfile(model_checkpoint_path):
        df_train = dataset.get_splitted(partitions=Partitions.TRAIN)
        n_cats = dataset.get_num_categories()
        classes = dataset.get_classes()
        train_batches = ImageDataGenerator().flow_from_dataframe(
            df_train,
            x_col="item",
            y_col="label",
            classes=classes,
            target_size=images_size,
            batch_size=batch_size,
            validate_filenames=False,
        )
        labelmap = {v: k for k, v in train_batches.class_indices.items()}
        write_labelmap_file(labelmap=labelmap, dest_path=labelmap_path)
        with tf.distribute.MirroredStrategy().scope():
            model = build_model(
                num_classes=n_cats,
                model_type=model_type,
                name=model_name,
                input_shape=(images_size[0], images_size[1], 3),
            )
        logger.info(f"Training model {model_name}")
        model.fit(
            train_batches,
            epochs=epochs,
            verbose=1,
            callbacks=[ModelCheckpoint(model_checkpoint_path)],
        )
    else:
        with tf.distribute.MirroredStrategy().scope():
            model = load_model(model_checkpoint_path)

    return model


# endregion

# region Classification


def classify_crops_in_test_set(
    detections_on_test_set_path,
    test_images_dir,
    crops_dir,
    score_threshold,
    model,
    labelmap_classification,
    classifs_on_test_crops_path,
    aux_dets_of_megadet_ds_path,
    images_size,
    batch_size,
    labelmap_detection,
    test_ds_json=None,
    false_positives_dets_csv=None,
    max_score_false_pos=MAX_SCORE_FALSE_POS,
    geo_prior_checkpoints=None,
    test_geo_prior_json=None,
    tta_steps=1,
    IWildcamBaseImageDataset=None,
):
    """Función que produce un dataset con las clasificaciones de `model` para cada uno de los
    recuadros generados por un modelo de detección de objetos sobre las imágenes de prueba

    Parameters
    ----------
    detections_on_test_set_path : str
        Ruta del archivo CSV con las predicciones hechas por un modelo de detección de objetos
        (p.e. Megadetector) sobre el conjunto de prueba
    test_images_dir : str
        Ruta de la carpeta que contiene a las imágenes de prueba
    crops_dir : str
        Carpeta que contendrá las imágenes generadas al recortar los bounding boxes resultantes de
        la inferencia del modelo de detección de objetos sobre las imágenes de prueba
    score_threshold : float
        Valor mínimo de score para el que se considerarán o se descartarán las predicciones del
        modelo de detección de objetos
    model : keras.models.Model
        Modelo de clasificación ajustado con las imágenes de entrenamiento que tiene como salida
        alguna de las categorías de `labelmap`
    labelmap_classification : dict
        Labelmap con las categorías del modelo de clasificación
    classifs_on_test_crops_path : str
        Ruta del archivo CSV con las clasificaciones generadas
    aux_dets_of_megadet_ds_path : str
        Ruta del archivo CSV auxiliar que almacena un dataset de clasificación con las detecciones
        del Megadetector. Se utiliza para ahorrar tiempo en la creación de este dataset
    images_size : tuple
        Tamaño de las imágenes que serán pasadas al método `predict` del modelo
    batch_size : int
        Tamaño de batch en el método `predict` del modelo
    labelmap_detection : dict
        Labelmap del modelo de detección de objetos (p.e., Megadetector)
    test_ds_json : str, optional
        Ruta del archivo JSON tipo COCO que se usará para obtener todas las fotos de una secuencia
        y descartar los falsos positivos del Megadetector, by default None
    false_positives_dets_csv : str, optional
        Ruta del archivo CSV que contiene las detecciones que son consideradas falsos positivos.
        Debe contener las columnas `seq_ids` y `det_ids`. By default None
    max_score_false_pos : float, optional
        Score máximo que podrán tener las detecciones que se consideran falsos positivos para que
        sean filtradas, by default `MAX_SCORE_FALSE_POS`
    geo_prior_checkpoints : str, optional
        Carpeta que contiene los checkpoints del modelo `geo prior` entrenado con datos de
        localización y fecha (estación del año) de las colecciones WCS y iNat, y con el que se
        ajustarán las probabilidades de cada clase en la salida del modelo de clasificación de
        acuerdo a la probabilidad a priori de que cada especie se encuentre en un lugar específico.
        By default None
    test_geo_prior_json : str, optional
        Ruta del archivo JSON tipo COCO que contiene los datos del dataset `test` que serán usados
        para realizar la inferencia del modelo `geo prior` sobre las imágenes de este conjunto.
        La información de este archivo deberá estar en el siguiente formato:
        {'images': [{'id': str, 'latitude': decimal, 'longitude': decimal,
        'date': %Y-%m-%d %H:%M:%S+00:00}, ...]}.
        Este archivo podría haber sido creado con la función `create_json_for_geo_priors`.
        By default None
    tta_steps : int, optional
        Número de pasos para aplicar la técnica TTA (Test Time Augmentation), by default 1
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`

    Returns
    -------
    ImagePredictionDataset
        Dataset de predicción que contiene las clasificaciones de `model` para cada uno de los
        elementos en el dataset de las detecciones del Megadetector
    """
    if os.path.isfile(aux_dets_of_megadet_ds_path):
        crops_of_test_set_ds = ImagePredictionDataset.from_csv(
            source_path=aux_dets_of_megadet_ds_path,
            images_dir=crops_dir,
            labelmap=MEGADET_LABELMAP,
        )
    else:
        dets_on_test_set_ds = ImagePredictionDataset.from_csv(
            source_path=detections_on_test_set_path,
            images_dir=test_images_dir,
            labelmap=labelmap_detection,
        )
        if test_ds_json is not None and false_positives_dets_csv is not None:
            false_pos = get_false_positive_dets_in_seqs(
                dets_ds=dets_on_test_set_ds,
                dataset_json=test_ds_json,
                images_dir=test_images_dir,
                sub_method="Acc",
                results_csv_path=false_positives_dets_csv,
                IWildcamBaseImageDataset=IWildcamBaseImageDataset,
            )
            false_dets_ids = false_pos["det_ids"].values
            not_false_pos = ~dets_on_test_set_ds.data["id"].isin(false_dets_ids)
            high_score = (
                dets_on_test_set_ds.data["score"] >= max_score_false_pos
            )
            dets_on_test_set_ds.data = dets_on_test_set_ds.data[
                not_false_pos | high_score
            ]
            dets_on_test_set_ds.data = dets_on_test_set_ds.data.reset_index(
                drop=True
            )
        FLDS = IWildcamBaseImageDataset.IMAGES_FIELDS
        crops_of_test_set_ds = (
            dets_on_test_set_ds.create_classif_ds_from_det_crops(
                dest_path=crops_dir,
                score_threshold=score_threshold,
                only_highest_score=False,
                include_id=True,
                inherit_fields=[
                    "image_id",
                    FLDS.SEQ_ID,
                    FLDS.SEQ_FRAME_NUM,
                    FLDS.SEQ_NUM_FRAMES,
                    FLDS.LOCATION,
                    FLDS.DATE_CAPTURED,
                ],
                labelmap=labelmap_detection,
            )
        )
        crops_of_test_set_ds.to_csv(dest_path=aux_dets_of_megadet_ds_path)
    return get_classifs_on_test_crops_ds(
        crops_of_test_set_ds=crops_of_test_set_ds,
        classifs_on_test_crops_csv=classifs_on_test_crops_path,
        labelmap=labelmap_classification,
        model=model,
        images_size=images_size,
        batch_size=batch_size,
        max_classifs=10,
        geo_prior_checkpoints=geo_prior_checkpoints,
        test_geo_prior_json=test_geo_prior_json,
        tta_steps=tta_steps,
        IWildcamBaseImageDataset=IWildcamBaseImageDataset,
    )


def get_classifs_on_test_crops_ds(
    crops_of_test_set_ds,
    classifs_on_test_crops_csv,
    labelmap,
    model,
    images_size,
    batch_size,
    max_classifs=1,
    geo_prior_checkpoints=None,
    test_geo_prior_json=None,
    tta_steps=1,
    IWildcamBaseImageDataset=None,
):
    """Función que produce un dataset con las clasificaciones de `model` para cada uno de los
    recuadros generados por un modelo de detección de objetos (cuyas predicciones están contenidas
    en `crops_of_test_set_ds`) sobre las imágenes de prueba

    Parameters
    ----------
    crops_of_test_set_ds : ImageDataset
        Dataset de clasificación generado a partir de las predicciones de un modelo de detección de
        objetos sobre las imágenes del conjunto de prueba.
        Este dataset debe contener las columnas `image_id`, `seq_id`, `seq_frame_num` y
        `id` (el id de la detección)
    classifs_on_test_crops_csv : str
        Ruta del archivo CSV con las clasificaciones generadas
    labelmap : dict
        Diccionario de la forma {`id`: `nombre`} de las categorías del modelo
    model : keras.models.Model
        Modelo de clasificación ajustado con las imágenes de entrenamiento que tiene como salida
        alguna de las categorías de `labelmap`
    images_size : tuple of the form (int, int)
        Tamaño de las imágenes que serán pasadas al método `predict` del modelo
    batch_size : int
        Tamaño de batch en el método `predict` del modelo
    max_classifs : int, optional
        Número máximo de predicciones que se agregarán en la clasificación por cada item, ordenados
        de manera descendente por la probabilidad. By defuault 1
    geo_prior_checkpoints : str, optional
        Carpeta que contiene los checkpoints del modelo `geo prior` entrenado con datos de
        localización y fecha (estación del año) de las colecciones WCS y iNat, y con el que se
        ajustarán las probabilidades de cada clase en la salida del modelo de clasificación de
        acuerdo a la probabilidad a priori de que cada especie se encuentre en un lugar específico.
        By default None
    test_geo_prior_json : str, optional
        Ruta del archivo JSON tipo COCO que contiene los datos del dataset `test` que serán usados
        para realizar la inferencia del modelo `geo prior` sobre las imágenes de este conjunto.
        La información de este archivo deberá estar en el siguiente formato:
        {'images': [{'id': str, 'latitude': decimal, 'longitude': decimal,
        'date': %Y-%m-%d %H:%M:%S+00:00}, ...]}
        By default None
    tta_steps : int, optional
        Número de pasos para aplicar la técnica TTA (Test Time Augmentation), by default 1
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`

    Returns
    -------
    ImagePredictionDataset
        Dataset de predicción que contiene las clasificaciones de `model` para cada uno de los
        elementos en el dataset `crops_of_test_set_ds`
    """
    if os.path.isfile(classifs_on_test_crops_csv):
        return ImagePredictionDataset.from_csv(
            source_path=classifs_on_test_crops_csv
        )

    from keras.preprocessing.image import ImageDataGenerator

    FLDS = IWildcamBaseImageDataset.IMAGES_FIELDS
    is_geo_prior = (
        geo_prior_checkpoints is not None and test_geo_prior_json is not None
    )

    max_classifs = min(max_classifs, len(labelmap))
    results = defaultdict(list)
    test_df = crops_of_test_set_ds.as_dataframe()
    if tta_steps > 1:
        data_gen = ImageDataGenerator(
            rotation_range=54,
            width_shift_range=0.1,
            height_shift_range=0.1,
            horizontal_flip=True,
            vertical_flip=True,
        )
    else:
        data_gen = ImageDataGenerator()
    test_batches = data_gen.flow_from_dataframe(
        test_df,
        x_col="item",
        class_mode=None,
        target_size=images_size,
        batch_size=batch_size,
        shuffle=False,
        validate_filenames=False,
    )
    if is_geo_prior:
        import tensorflow as tf

        prior_preds, prior_valid = get_geo_prior_predictions(
            test_geo_prior_json,
            geo_prior_checkpoints,
            num_classes=len(labelmap),
        )
    if tta_steps > 1:
        predictions = []
        for i in range(tta_steps):
            preds = model.predict(test_batches, batch_size=batch_size)
            predictions.append(preds)
        preds = np.mean(predictions, axis=0)
    else:
        preds = model.predict(test_batches, batch_size=batch_size)
    for i, (_, row) in enumerate(test_df.iterrows()):
        if is_geo_prior and row["id"] in prior_preds:
            valid = tf.expand_dims(prior_valid[row["id"]], axis=-1)
            _preds = (
                preds[i] * prior_preds[row["id"]] * valid
                + (1 - valid) * prior_preds[row["id"]]
            )
            _preds = _preds.numpy()
        else:
            _preds = preds[i]
        sorted_inds = [
            y[0]
            for y in sorted(enumerate(_preds), key=lambda x: x[1], reverse=True)
        ]
        for k in range(max_classifs):
            ind = sorted_inds[k]
            results["item"].append(row["item"])
            results["label"].append(labelmap[ind])
            results["score"].append(_preds[ind])
            results["image_id"].append(row["image_id"])
            results[FLDS.SEQ_ID].append(row[FLDS.SEQ_ID])
            results[FLDS.SEQ_FRAME_NUM].append(row[FLDS.SEQ_FRAME_NUM])
            results["id"].append(row["id"])
    data = pd.DataFrame(results)
    images_dir = crops_of_test_set_ds.get_images_dir()
    classifs_on_test_crops_ds = ImagePredictionDataset(
        data, info={}, images_dir=images_dir
    )
    classifs_on_test_crops_ds.to_csv(dest_path=classifs_on_test_crops_csv)
    return classifs_on_test_crops_ds


# endregion

# region Evaluation


def eval_model(
    model, dataset, labelmap_path, results_dir, images_size, batch_size
):
    logger.info("Evaluating model...")
    labelmap = read_labelmap_file(labelmap_path)
    preds_on_test_part_csv = os.path.join(results_dir, "predictions.csv")
    os.makedirs(results_dir, exist_ok=True)
    if os.path.isfile(preds_on_test_part_csv):
        preds_on_test_part_ds = ImagePredictionDataset.from_csv(
            source_path=preds_on_test_part_csv,
            images_dir=dataset.get_images_dir(),
            labelmap=labelmap,
        )
    else:
        preds_on_test_part_ds = predict_on_ds(
            model=model,
            dataset=dataset,
            labelmap=labelmap,
            images_size=images_size,
            batch_size=batch_size,
            partition=Partitions.TEST,
        )
        preds_on_test_part_ds.to_csv(dest_path=preds_on_test_part_csv)
    res_eval = ImageClassificationEvaluator.eval(
        dataset_true=dataset,
        dataset_pred=preds_on_test_part_ds,
        eval_config={
            "dataset_partition": Partitions.TEST,
            "metrics_set": {
                ImageClassificationMetrics.Sets.MULTICLASS: {"average": "micro"}
            },
        },
    )
    res_eval.result_plots(dest_path=results_dir, report=True)
    res_eval.store_eval_metrics(
        dest_path=os.path.join(results_dir, "eval_results.json")
    )


def predict_on_ds(
    model, dataset, labelmap, images_size, batch_size, partition=None
):
    """Función que realiza la predicción sobre las imágenes de `dataset` usando el modelo `model`
    y crea una instancia de `ImagePredictionDataset` agregando las columnas `image_id`, `seq_id`,
    `seq_frame_num`, `id`

    Parameters
    ----------
    model : Model
        Modelo con el que se realizará la inferencia sobre las imágenes
    dataset : ImageDataset
        Dataset con las imágenes sobre las que se realizará la inferencia
    labelmap : dict
        Labelmap con la definición de las categorías del dataset, de la forma {`id`: `name`}
    images_size : (int, int)
        Tamaño al que se convertirán las imágenes para realizar la predicción
    batch_size : int
        Tamaño del batch de procesamiento
    partition : str, optional
        Partición de `dataset` sobre la que se realizará la inferencia, by default None

    Returns
    -------
    ImagePredictionDataset
        Dataset que contiene las predicciones del modelo sobre las imágenes de la partición
        `partition` de `dataset`
    """
    from keras.preprocessing.image import ImageDataGenerator

    results = defaultdict(list)
    df = dataset.get_splitted(partitions=partition)
    batches = ImageDataGenerator().flow_from_dataframe(
        df,
        x_col="item",
        class_mode=None,
        target_size=images_size,
        batch_size=batch_size,
        shuffle=False,
        validate_filenames=False,
    )
    preds = model.predict(batches, batch_size=batch_size)
    for i, (_, row) in enumerate(df.iterrows()):
        ind = np.argmax(preds[i])
        results["item"].append(row["item"])
        results["label"].append(labelmap[ind])
        results["score"].append(preds[i][ind])
        for image_field in ["image_id", "seq_id", "seq_frame_num", "id"]:
            if image_field in row:
                results[image_field].append(row[image_field])
    data = pd.DataFrame(results)
    return ImagePredictionDataset(data, images_dir=dataset.get_images_dir())


# endregion

# region Ensembles


def ensemble_classification_models(
    models_names,
    classifs_csv,
    images_dir,
    method="mean_divide_n_models",
    model_weights=None,
    max_classifs=1,
    item=None,
):
    """Función que realiza el ensamble de los modelos `models_names` buscando los archivos CSV que
    contengan las clasificaciones hechas previamente y en caso de encontrar todos los datasets de
    predicción los ensamble en un solo dataset que será considerado como el producto de los modelos

    Parameters
    ----------
    models_names : list of str
        Lista con los nombres de los modelos, que será utilizada para buscar los archivod CSV con
        el sufijo de cada modelo, y en caso de encontrarse todos los datasets, realiza el ensamble
        de los modelos
    classifs_csv : str
        Nombre del archivo CSV que contendrá las clasificaciones del ensamble de los modelos
        `models_names`. También será usado para buscar las clasificaciones de los modelos usando
        el sufijo de cada uno
    images_dir : str
        Ruta de la carpeta que contiene las imágenes sobre las que se realizará la inferencia de
        los modelos
    model_weights : dict, optional
        Diccionario con los pesos de cada modelo cuando `method = 'weighted'`. By defuault None
    max_classifs : int, optional
        Número máximo de predicciones que se agregarán en la clasificación por cada item, ordenados
        de manera descendente por la probabilidad. By defuault 1
    item : str, optional
        Item en particular sobre la que se quiere hacer el procesamiento.
        Generalmente usado para depurar, by default None

    Returns
    -------
    ImagePredictionDataset
        Dataset con las predicciones del ensamble de los modelos
    """
    if type(models_names) is str:
        models_names = models_names.split(",")

    classifs_base, ext = os.path.splitext(classifs_csv)
    if len(models_names) == 1:
        out_csv = f"{classifs_base}-{models_names[0]}-unique{ext}"
        if os.path.isfile(out_csv):
            return ImagePredictionDataset.from_csv(
                source_path=out_csv, images_dir=images_dir
            )

        ds = ImagePredictionDataset.from_csv(
            source_path=f"{classifs_base}-{models_names[0]}{ext}"
        )
        res_df = ds.as_dataframe(only_highest_score=True)
    else:
        models_to_dfs = dict()
        out_csv = f"{classifs_base}-{method}{ext}"
        if os.path.isfile(out_csv):
            return ImagePredictionDataset.from_csv(
                source_path=out_csv, images_dir=images_dir
            )

        if not all(
            [
                os.path.isfile(f"{classifs_base}-{mdl_nm}{ext}")
                for mdl_nm in models_names
            ]
        ):
            logger.info(
                f"Not all {len(models_names)} models have finished training. Exit"
            )
            sys.exit()

        for model in models_names:
            ds = ImagePredictionDataset.from_csv(
                source_path=f"{classifs_base}-{model}{ext}"
            )
            df = ds.as_dataframe()
            models_to_dfs[model] = df

        logger.info(
            f"Doing ensemble of models {','.join(models_names)} using method {method}"
        )
        base_df = ds.as_dataframe(only_highest_score=True).set_index("item")
        base_dict = base_df.to_dict(orient="index")
        item_to_lbls_scores = create_shared_defaultdict()
        if item is not None:
            set_label_and_score_for_item_in_ensemble(
                item, models_to_dfs, method, item_to_lbls_scores, model_weights
            )
            return
        else:
            with multiprocessing.Pool(
                processes=multiprocessing.cpu_count()
            ) as pool:
                pool.starmap(
                    set_label_and_score_for_item_in_ensemble,
                    [
                        (
                            item,
                            models_to_dfs,
                            method,
                            item_to_lbls_scores,
                            model_weights,
                        )
                        for item in base_df.index.values
                    ],
                )

        results = defaultdict(list)
        rest_cols = list(set(base_df.columns) - {"item", "label", "score"})
        for item, lbls_scors_list in item_to_lbls_scores.items():
            for i, lbl_scor in enumerate(lbls_scors_list):
                if i >= max_classifs:
                    break
                for label, score in lbl_scor.items():
                    results["item"].append(item)
                    results["label"].append(label)
                    results["score"].append(score)
            for col in rest_cols:
                results[col] += [base_dict[item][col]] * min(
                    max_classifs, len(lbls_scors_list)
                )
        res_df = pd.DataFrame(results)
    res_ds = ImagePredictionDataset(res_df, info={}, images_dir=images_dir)
    res_ds.to_csv(dest_path=out_csv)

    return res_ds


def set_label_and_score_for_item_in_ensemble(
    item, models_to_dfs, method, item_to_lbls_scores, model_weights=None
):
    """Función que toma las predicciones hechas por varios modelos sobre un conjunto de datos, y a
    partir de algún criterio, genera una clasificación final por cada item.
    El criterio para determinar la clasificación final es promediar las clasificaciones hechas por
    todos los modelos y tomar la que resulte mayor


    Parameters
    ----------
    item : str
        Elementos de la columna `item` de cada DataFrame que se filtrarán para generar la
        clasificación final
    models_to_dfs : dict
        Diccionario con los DataFrames de cada modelo que contienen las predicciones hechas sobre
        el conjunto de datos
    method : str
        Método que se utilizará para realizar el ensamble de los modelos.
        Puede ser uno de {'mean_divide_counts', 'mean_divide_n_models', 'weighted'}
    item_to_lbls_scores : dict
        Diccionario de listas que contiene los elementos ordenados por el score resultante los
        elementos del ensamble, de la forma:  {`item`: [{`label`: `score`}, ], }
    model_weights : dict, optional
        Diccionario con los pesos de cada modelo cuando `method = 'weighted'`, by default None
    """
    n_models = len(models_to_dfs)
    label_to_scores = defaultdict(list)
    for model, df in models_to_dfs.items():
        for _, crop in df[df["item"] == item].iterrows():
            score = crop["score"]
            if method == "weighted":
                score *= model_weights[model]
            label_to_scores[crop["label"]].append(score)
    label_to_mean = dict()
    for label, scores in label_to_scores.items():
        if method == "mean_divide_counts":
            label_to_mean[label] = np.mean(np.array(scores))
        if method == "mean_divide_n_models":
            label_to_mean[label] = np.sum(np.array(scores)) / n_models
        if method == "weighted":
            label_to_mean[label] = np.sum(np.array(scores))
    for lbl, scor in dict(
        sorted(label_to_mean.items(), key=lambda x: x[1], reverse=True)
    ).items():
        item_to_lbls_scores[item] += [{lbl: scor}]


def ensemble_compl_and_crops_models(
    compl_ds,
    crops_ds,
    classifs_csv,
    images_dir,
    method,
    model_weights,
    max_classifs=1,
    image_id=None,
):
    """Función que realiza el ensamble de dos modelos: uno creado a partir de las imágenes
    completas de un dataset, llamado `compl`, y uno creado a partir de las detecciones de un modelo
    de detección (i.e. Megadetector) , llamado `crops`. El ensamble se hace ponderando cada modelo
    según los pesos en `model_weights`. El dataset resultante se guarda en una ruta relativa a
    `classifs_csv`

    Parameters
    ----------
    compl_ds : ImagePredictionDataset
        Dataset con las predicciones sobre las imágenes enteras del dataset de prueba
    crops_ds : ImagePredictionDataset
        Dataset con las predicciones sobre los crops de las detecciones del dataset de prueba
    classifs_csv : str
        Ruta donde se encuentran almacenados en archivos CSV las predicciones de los modelos que se
        desean ensamblar
    images_dir : str
        Ruta donde están almacenadas las imágenes del conjunto de prueba
    method : str
        Nombre del método que se utilizará para hacer el ensamble
    model_weights : dict
        Diccionario con los pesos de cada modelo, de la forma
        {'compl': `weight`, 'crops': `weight`}
    max_classifs : int, optional
        Número máximo de predicciones que se agregarán en la clasificación por cada item, ordenados
        de manera descendente por la probabilidad. By defuault 1
    image_id : str, optional
        Id de un elemento en particular sobre el que se quiere hacer el procesamiento.
        Generalmente usado para depurar, by default None

    Returns
    -------
    ImagePredictionDataset
        Instancia del dataset resultante del ensamble
    """
    classifs_base, ext = os.path.splitext(classifs_csv)
    out_csv = f"{classifs_base}-compl_crops-{method}{ext}"
    if os.path.isfile(out_csv):
        return ImagePredictionDataset.from_csv(out_csv, images_dir=images_dir)

    id_to_lbls_scores = create_shared_defaultdict()
    compl_df = compl_ds.as_dataframe()
    crops_df = crops_ds.as_dataframe()
    images_ids = compl_df["image_id"].unique()

    if image_id is not None:
        set_label_and_score_for_item_in_ensemble_compl_crops(
            image_id, compl_df, crops_df, id_to_lbls_scores, model_weights
        )
    else:
        with multiprocessing.Pool(
            processes=multiprocessing.cpu_count()
        ) as pool:
            pool.starmap(
                set_label_and_score_for_item_in_ensemble_compl_crops,
                [
                    (
                        _image_id,
                        compl_df,
                        crops_df,
                        id_to_lbls_scores,
                        model_weights,
                    )
                    for _image_id in images_ids
                ],
            )

    compl_df = compl_df.drop_duplicates(subset="item", keep="first").set_index(
        "id"
    )
    crops_df = crops_df.drop_duplicates(subset="item", keep="first").set_index(
        "id"
    )
    compl_dict = compl_df.to_dict(orient="index")
    crops_dict = crops_df.to_dict(orient="index")
    results = defaultdict(list)
    rest_cols = list(set(compl_df.columns) - {"id", "label", "score"})
    for id, lbls_scors_list in id_to_lbls_scores.items():
        for i, lbl_scor in enumerate(lbls_scors_list):
            if i >= max_classifs:
                break
            for label, score in lbl_scor.items():
                results["id"].append(id)
                results["label"].append(label)
                results["score"].append(score)
        for col in rest_cols:
            img = compl_dict.get(id, crops_dict.get(id))
            results[col] += [img[col]] * min(max_classifs, len(lbls_scors_list))
    res_df = pd.DataFrame(results)
    res_ds = ImagePredictionDataset(res_df, info={}, images_dir=images_dir)
    res_ds.to_csv(dest_path=out_csv)

    return res_ds


def set_label_and_score_for_item_in_ensemble_compl_crops(
    image_id, compl_df, crops_df, id_to_lbls_scores, model_weights
):
    """Función que asigna la etiqueta y el score final para la imagen (o crops) con `image_id`
    según las etiquetas y scores en `compl_df` y `crops_df`, y los guarda en `id_to_lbls_scores`

    Parameters
    ----------
    image_id : str
        Valor de la columna 'image_id' para el elemento que se desea examinar, que están contenidos
        en los DataFrames `compl_df` y `crops_df`
    compl_df : pd.DataFrame
        DataFrame con las predicciones sobre las imágenes completas del conjunto de prueba
    crops_df : pd.DataFrame
        DataFrame con las predicciones sobre los crops de las detecciones del conjunto de prueba
    id_to_lbls_scores : dict
        Diccionario de listas que contiene los elementos ordenados por el score resultante los
        elementos del ensamble, de la forma:  {`id`: [{`label`: `score`}, ], }
        donde `id` es ya sea el id de la detección o el id de la imagen
    model_weights : dict
        Diccionario que contiene los pesos de los modelos que se desea ensamblar, en la forma
        {'compl': `weight`, 'crops': `weight`}
    """
    crops_of_img = crops_df[crops_df["image_id"] == image_id]
    compl_of_img = compl_df[compl_df["image_id"] == image_id]
    if len(crops_of_img) == 0:
        for _, pred in compl_of_img.iterrows():
            id_to_lbls_scores[pred["id"]] += [{pred["label"]: pred["score"]}]
    else:
        compl_lbl_scor = {
            pred["label"]: pred["score"] for _, pred in compl_of_img.iterrows()
        }
        crop_id_lbl_scor = defaultdict(lambda: defaultdict(float))
        for _, det in crops_of_img.iterrows():
            # Fix: hacer esto para cada etiqueta posible también de compl
            compl_fact = (
                compl_lbl_scor.get(det["label"], 0.0) * model_weights["compl"]
            )
            crop_id_lbl_scor[det["id"]][det["label"]] += (
                det["score"] * model_weights["crops"] + compl_fact
            )
        for crop_id, lbl_scor in crop_id_lbl_scor.items():
            for lbl, scor in sorted(
                lbl_scor.items(), key=lambda x: x[1], reverse=True
            ):
                id_to_lbls_scores[crop_id] += [{lbl: scor}]


# endregion

# region Geo-prior


def build_input_data(test_data_json, num_classes, batch_size=1024):
    """Función que genera una instancia de `tf.data.Dataset` a partir de la información del archivo
    JSON tipo COCO `test_data_json`

    Parameters
    ----------
    test_data_json : str
        Ruta del archivo JSON tipo COCO que contiene la información con la que se va a construir
        el dataset
    num_classes : int
        Número de categorías que contiene el dataset
    batch_size : int
        Tamaño del batch con el que se va a consumir el dataset

    Returns
    -------
    (tf.data.Dataset, int)
        Tupla con el dataset que podrá ser consumido para aplicar la inferencia del modelo `FCNet`
        y el número de `features` que se analizan
    """
    from geo_prior import dataloader

    input_data = dataloader.JsonInatInputProcessor(
        test_data_json,
        batch_size=batch_size,
        is_training=False,
        remove_invalid=False,
        provide_validity_info_output=True,
        num_classes=num_classes,
        provide_instance_id=True,
        batch_drop_remainder=False,
    )
    dataset, _, _, _, num_feats = input_data.make_source_dataset()

    return dataset, num_feats


def load_prior_model(num_feats, num_classes, ckpt_dir):
    """Función que construye la arquitectura del modelo `geo prior` y carga los pesos de `ckpt_dir`

    Parameters
    ----------
    num_feats : int
        Número de elementos en la entrada del modelo
    num_classes : int
        Número de elementos en la capa de salida del modelo
    ckpt_dir : str
        Ruta de la carpeta que contiene los pesos del modelo entrenado

    Returns
    -------
    tf.keras.Model
        Instancia de `Model` con la que se podrá aplicar la inferencia del modelo `geo prior`
    """
    from geo_prior import dataloader
    from geo_prior.models import FCNet

    randgen = dataloader.RandSpatioTemporalGenerator(
        loc_encode="encode_cos_sin",
        date_encode="encode_cos_sin",
        use_date_feats=True,
    )
    model = FCNet(
        num_inputs=num_feats,
        embed_dim=256,
        num_classes=num_classes,
        rand_sample_generator=randgen,
        num_users=0,
        use_bn=False,
    )
    checkpoint_path = os.path.join(ckpt_dir, "ckp")
    model.load_weights(checkpoint_path)
    return model


def get_geo_prior_predictions(
    test_geo_prior_json, geo_prior_checkpoints, num_classes
):
    """Función que regresa las predicciones del modelo `geo prior` cuyos pesos están en la carpeta
    `geo_prior_checkpoints` sobre los datos del conjunto de prueba `test_geo_prior_json`

    Parameters
    ----------
    test_geo_prior_json : str
        Ruta del archivo JSON tipo COCO que contiene la información de las imágenes sobre las que
        se harán las predicciones
    geo_prior_checkpoints : str
        Ruta de la carpeta que contiene los pesos del modelo `geo prior`
    num_classes : int
        Número de categorías que contiene el dataset

    Returns
    -------
    (dict, dict)
        Tupla con:
            - El diccionario que contiene las predicciones del modelo `geo prior` sobre el dataset
            `test_geo_prior_json` en el formato: {`id`: [num_classes probabilidades]}
            - El diccionario que indica si cada elemento contiene predicciones válidas o no, en la
            forma: {`id`: 1. if `valid` else 0.}
    """
    dataset, num_feats = build_input_data(test_geo_prior_json, num_classes)
    prior_model = load_prior_model(
        num_feats, num_classes, geo_prior_checkpoints
    )
    res_geo_prior, valid_geo_prior = dict(), dict()
    for batch, metadata in dataset:
        label, valid, ids = metadata
        prior_preds = prior_model(batch, training=False)
        valid_np = valid.numpy()
        for i, id in enumerate(ids.numpy()):
            _id = id.decode("utf-8")
            res_geo_prior[_id] = prior_preds[i]
            valid_geo_prior[_id] = valid_np[i]
    return res_geo_prior, valid_geo_prior


# endregion

# region False positives


def get_false_positive_dets_in_seqs(
    dets_ds,
    dataset_json,
    images_dir,
    sub_method,
    results_csv_path,
    num_tasks=None,
    task_num=None,
    IWildcamBaseImageDataset=None,
):
    """Función que obtiene un DataFrame que contiene las detecciones que son consideras falsos
    positivos del Megadetector. Contiene las columnas `seq_ids` y `det_ids`

    Parameters
    ----------
    dets_ds : ImagePredictionDataset
        Dataset con las predicciones hechas por un modelo de detección de objetos
        (p.e. Megadetector) sobre el conjunto de imágenes
    dataset_json : str
        Ruta del archivo JSON tipo COCO que se usará para obtener todas las fotos de una secuencia
        y descartar los falsos positivos del Megadetector
    images_dir : str
        Ruta de la carpeta que contiene a las imágenes del dataset
    sub_method : str
        Método que se utilizará para determinar los falsos positivos. Puede ser 'Acc' o 'MOG'
    results_csv_path : str
        Ruta del archivo CSV donde se guardarán los falsos positivos resultantes con las columnas
        `seq_ids` y `det_ids`
    num_tasks : int, optional
        En caso de realizar el procesamiento de forma paralela (p.e. en varios nodos), este es el
        número total de tareas en las que se divide el procesamiento. P.e., si se cuenta con 4
        nodos con 2 GPUs en cada uno, el trabajo se puede dividir en `num_tasks = 8` tareas
        paralelas, para así maximizar el uso de los recursos y realizar el procesamiento en el
        menor tiempo posible
    task_num : int, optional
        En caso de realizar el procesamiento de forma paralela, este es el número de la tarea
        actual. Debe de ser un entero en el rango [1, `task_num`]
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`

    Returns
    -------
    pd.DataFrame
        DataFrame con las detecciones que se consideran falsos positivos del Megadetector.
        Contiene las columnas `seq_ids` y `det_ids`
    """
    if os.path.isfile(results_csv_path):
        logger.info(f"Using false positives detections from {results_csv_path}")
        return pd.read_csv(results_csv_path, header=0)

    false_positives_list = Manager().list()
    all_ds = IWildcamBaseImageDataset.from_json(
        source_path=dataset_json, images_dir=images_dir
    )
    df_all = all_ds.as_dataframe(
        columns=[
            IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_ID,
            IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_FRAME_NUM,
        ]
    )
    df_dets = dets_ds.as_dataframe(
        columns=[IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_ID]
    )
    seq_ids = df_dets[IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_ID].unique()

    if num_tasks is not None and task_num is not None:
        format_split_path = "false_pos_dets_{0}_of_{1}.csv"
        glob_pattern = "false_pos_dets_*_of_*.csv"
        false_pos_dirname = os.path.dirname(results_csv_path)
        split_name = f"splitted_{os.path.splitext(os.path.basename(results_csv_path))[0]}"
        splitted_false_pos_dets_path = os.path.join(
            false_pos_dirname, split_name
        )
        os.makedirs(splitted_false_pos_dets_path, exist_ok=True)
        splitted_dets_path = os.path.join(
            splitted_false_pos_dets_path,
            format_split_path.format(task_num, num_tasks),
        )
        seq_ids = get_chunk(
            elements=seq_ids, num_tasks=num_tasks, task_num=task_num
        )

    tuples = [
        (seq_id, df_all, df_dets, false_positives_list, sub_method)
        for seq_id in seq_ids
    ]
    logger.info(f"Getting false positives from {len(tuples)} sequences")
    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        pool.starmap(append_false_positive_dets_in_seq, tuples)

    data = {
        "seq_ids": [x["seq_id"] for x in false_positives_list],
        "det_ids": [x["det_id"] for x in false_positives_list],
    }
    false_pos = pd.DataFrame(data)

    if num_tasks is not None and task_num is not None:
        false_pos.to_csv(splitted_dets_path, index=False, header=True)
        splits_files = glob(
            os.path.join(splitted_false_pos_dets_path, glob_pattern)
        )
        if len(splits_files) < num_tasks:
            logger.info(
                f"Exit program after create  split {task_num} of {num_tasks}."
            )
            sys.exit()
        logger.info(f"Creation of {num_tasks} splits complete.")
        false_pos = pd.DataFrame()
        for fle in splits_files:
            false_pos = pd.concat(
                [false_pos, pd.read_csv(fle, header=0)], ignore_index=True
            )
        rmtree(splitted_false_pos_dets_path, ignore_errors=True)
        logger.info(f"Folder {splitted_false_pos_dets_path} was deleted")

    false_pos.to_csv(results_csv_path, index=False, header=True)
    return false_pos


def append_false_positive_dets_in_seq(
    seq_id,
    df_all,
    df_dets,
    false_positives_list,
    sub_method,
    accAvg=0.35,
    threshT=40,
    mogvariance=25,
    learningRate=0.09,
    kernel_size=(9, 9),
    IWildcamBaseImageDataset=None,
):
    """Función que agrega los falsos positivos de las detecciones de la secuencia `seq_id` a la
    lista `false_positives_list` en forma de diccionario con las entradas `seq_id` y `det_id`.
    La forma de calcular los falsos positivos se realiza dependiendo del parámetro `sub_method`.

    Si `sub_method` es 'Acc' los falsos positivos se calculan de la siguiente manera:
    Para cada detección del Megadetector en la secuencia `seq_id`, se hace la suma acumulada de los
    pixeles de la región del bbox de esa detección para todas las imágenes de la secuencia, y al
    final esta suma acumulada, que representa un modelo de esa región de las imágenes de la
    secuencia, se compara con el recuadro de la detección a través de la diferencia absoluta y se
    aplica una operación de umbralado (threshold) a estos valores. A este resultado se aplican
    operaciones morfológicas (apertura: erosión seguida de dilatación) para eliminar el ruido, y
    finalmente se aplica una función que busca contornos, y en caso de no haber ningún contorno
    en el resultado, se asume que en esta región de la secuencia no hay movimiento, y por lo tanto
    la detección original se agrega a `false_positives_list`.

    Parameters
    ----------
    seq_id : str
        Id de la secuencia a analizar
    df_all : pd.DataFrame
        DataFrame que contiene los datos de todos los registros en el dataset y que será utilizado
        para obtener todas las imágenes en la secuencia `seq_id`
    df_dets : pd.DataFrame
        DataFrame que contiene las detecciones hechas sobre el dataset. Se utiliza para obtener las
        detecciones en la secuencia `seq_id` y analizar las regiones de cada detección sobre todas
        las imágenes de la secuencia y determinar si la detección es un falso positivo
    false_positives_list : list
        Lista a la que se agregan las detecciones consideradas falsos positivos en forma de
        diccionario con las entradas `seq_id` y `det_id`
    sub_method : str
        Método que se utilizará para determinar los falsos positivos. Puede ser 'Acc' o 'MOG'
    accAvg : float, optional
        Peso de la imagen de entrada que se envía a la función `cv2.accumulateWeighted` cuando
        `sub_method` == 'Acc', by default 0.35
    threshT : int, optional
        Valor de umbral que se utiliza cuando `sub_method` == 'Acc', by default 40
    mogvariance : int, optional
        Umbral en la distancia de Mahalanobis al cuadrado entre el píxel y el modelo para decidir
        si un píxel está bien descrito por el modelo de fondo cuando `sub_method` == 'MOG',
        by default 25
    learningRate : float, optional
        Learning rate del método `apply` cuando `sub_method` == 'MOG', by default 0.09
    kernel_size : tuple of (int, int), optional
        Tamaño del kernel para los operaciones morfológicas
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`
    """

    imgs_of_seq = df_all[df_all[IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_ID] == seq_id]
    if len(imgs_of_seq) > 1:
        dets_of_seq = df_dets[
            df_dets[IWildcamBaseImageDataset.IMAGES_FIELDS.SEQ_ID] == seq_id
        ]
        for _, det_of_seq in dets_of_seq.iterrows():
            [x, y, width, height] = [
                int(x) for x in det_of_seq["bbox"].split(",")
            ]
            crop_of_det = cv2.imread(det_of_seq["item"])[
                y : y + height, x : x + width
            ]
            crops_of_imgs = [
                cv2.imread(item)[y : y + height, x : x + width]
                for item in imgs_of_seq["item"].values
            ]
            if sub_method == "Acc":
                try:
                    running_average_image = np.float32(crops_of_imgs[0])
                    for crop_of_img in crops_of_imgs[1:]:
                        color_image = cv2.GaussianBlur(crop_of_img, (3, 3), 0)
                        cv2.accumulateWeighted(
                            color_image, running_average_image, accAvg
                        )
                    running_average_in_display_color_depth = (
                        cv2.convertScaleAbs(running_average_image)
                    )
                    difference = cv2.absdiff(
                        crop_of_det, running_average_in_display_color_depth
                    )
                    grey_image = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
                    ret, grey_image = cv2.threshold(
                        grey_image, threshT, 255, cv2.THRESH_BINARY
                    )
                except Exception as ex:
                    logger.error(
                        f"Ocurrió un error al procesar la secuencia {seq_id}: {ex}"
                    )
                    continue
            elif sub_method == "MOG":
                fgbg = cv2.createBackgroundSubtractorMOG2(
                    detectShadows=False, varThreshold=float(mogvariance)
                )
                fgbg.setBackgroundRatio(0.95)
                for crop_of_img in crops_of_imgs:
                    grey_image = fgbg.apply(
                        crop_of_img, learningRate=learningRate
                    )
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, kernel_size)
            new_grey_image = cv2.morphologyEx(
                grey_image, cv2.MORPH_OPEN, kernel
            )
            contours, hierarchy = cv2.findContours(
                new_grey_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
            )
            if len(contours) == 0:
                false_positives_list.append(
                    {"seq_id": seq_id, "det_id": det_of_seq["id"]}
                )


# endregion

# region Auxiliars


def get_bboxes_info_for_items(
    items, dets_classifs_df, false_pos_dets, min_score_true_classifs
):
    """Función que obtiene la información de los bounding boxes para que puedan ser mostrados en
    las imágenes de `items`. El color y la etiqueta dependerán de varios factores de la
    clasificación:
    - Si un ítem no tiene `score_classif` (NaN o None), o si `id_det in false_pos_dets`, el bbox
    será de color rojo y la etiqueta 'FP' (false positive) o 'NoScore'.
    - Si `label_classif == 'empty'` o `score_classif < min_score_true_classifs` el bbox será de
    color naranja.
    - En caso contrario el bbox será de color verde, con la etiqueta formada con los valores de
    `label_classif` y `score_classif`.

    Parameters
    ----------
    items : list
        Lista con los items de los que se quiere obtener la información
    dets_classifs_df : pd.DataFrame
        Dataframe que contiene la información conjunta de las detecciones y la clasificación de
        cada una. Debe contener las columnas 'bbox', 'score_classif', 'id_det', 'label_classif'
    false_pos_dets : pd.DataFrame
        DataFrame que contiene las detecciones que son consideradas falsos positivos.
        Estas detecciones serán dibujadas en color rojo y con la etiqueta 'FP'.
        Debe contener las columnas `seq_ids` y `det_ids`
    min_score_true_classifs : float
        Valor de score mínimo para que las clasificaciones sean descartadas como falsas

    Returns
    -------
    dict
        Diccionario con la información de los bounding boxes en la forma
        {`item`: 'bboxes': [`bboxes`], 'colors': [`colors`],
                 'labels': [`labels`], 'scores': [`scores`]}
    """
    bboxes_info = dict()
    false_det_ids = (
        false_pos_dets["det_ids"].values if false_pos_dets is not None else []
    )
    for item in items:
        dets_item = dets_classifs_df[dets_classifs_df["item"] == item]
        if len(dets_item) > 0:
            bboxes, colors, labels, scores = list(), list(), list(), list()
            for _, row in dets_item.iterrows():
                bboxes.append([int(x) for x in row["bbox"].split(",")])
                if (
                    isnan(row["score_classif"])
                    or row["score_classif"] is None
                    or row["id_det"] in false_det_ids
                ):
                    colors.append(RED)
                    scores.append("")
                    if row["id_det"] in false_det_ids:
                        labels.append("FP")
                    else:
                        labels.append("NoScore")
                else:
                    labels.append(row["label_classif"])
                    scores.append(row["score_classif"])
                    if (
                        row["label_classif"] == "empty"
                        or row["score_classif"] < min_score_true_classifs
                    ):
                        colors.append(ORANGE)
                    else:
                        colors.append(GREEN)
            bboxes_info[item] = {
                "bboxes": bboxes,
                "colors": colors,
                "labels": labels,
                "scores": scores,
            }
    return bboxes_info


def merge_dets_and_classifs_dfs(dets_df, classifs_df, how_to_merge):
    """Función que hace un merge (inner join) de los Dataframes de las detecciones y las
    clasificaciones sobre la columna `id` de ambos

    Parameters
    ----------
    dets_df : pd.DataFrame
        Dataframe con las detecciones del Megadetector
    classifs_df : pd.DataFrame
        Dataframe con las predicciones de un modelo de clasificación sobre las imágenes de
        `dets_df`
    how_to_merge : str
        Forma en que se hará el merge entre los datasets de clasificación y detección.
        Las opciones válidas son las permitidas por la función `pd.merge`

    Returns
    -------
    pd.DataFrame
        DataFrame con las columnas {"item", "bbox", "label_det", "label_classif", "location",
        "score_det", "score_classif", "id", "image_id", "seq_id"}
    """
    lett = "y" if how_to_merge == "right" else "x"
    df = pd.merge(
        left=dets_df,
        right=classifs_df,
        left_on="image_id",
        right_on="image_id",
        how=how_to_merge,
    )
    df.rename(
        columns={
            f"item_{lett}": "item",
            f"image_id_{lett}": "image_id",
            f"seq_id_{lett}": "seq_id",
            f"label_x": "label_det",
            "label_y": "label_classif",
            "score_x": "score_det",
            "score_y": "score_classif",
        },
        inplace=True,
    )
    df = df[
        [
            "item",
            "bbox",
            "label_det",
            "label_classif",
            "location",
            "score_det",
            "score_classif",
            "image_id",
            "seq_id",
        ]
    ]
    return df


def filter_non_animal_labels(df):
    """Función que quita las detecciones de Animal con clasificaciones de 'misfire, motorcycle,
    start, end' y las detecciones de Vehicle o Human con clasificaciones de animales

    Parameters
    ----------
    df : pd.DataFrame
        DataFrame que se desea filtrar

    Returns
    -------
    pd.DataFrame
        DataFrame resultante
    """
    non_animal_lbls = ["misfire", "motorcycle", "start", "end"]
    animal_wrng_ids = df[
        (df["label_classif"].isin(non_animal_lbls))
        & (df["label_det"] == "Animalia")
    ]["image_id"].values
    non_animal_wrng = df[
        (~df["label_classif"].isin(non_animal_lbls))
        & (~df["label_det"].isnull() & (df["label_det"] != "Animalia"))
    ]
    no_animal_wrng_ids = non_animal_wrng["image_id"].values
    return df[
        (~df["image_id"].isin(animal_wrng_ids))
        & (~df["image_id"].isin(no_animal_wrng_ids))
    ]


# endregion

# region Command-line


def visualize_false_positives_in_ds(
    dataset_json,
    detections_csv,
    images_dir,
    false_pos_dets_csv,
    dest_dir,
    false_pos_dets_ids=None,
    split_in_seqs=False,
    max_score_false_pos=MAX_SCORE_FALSE_POS,
):
    """Función que permite visualizar las detecciones que se consideran falsos positivos en un
    dataset de predicción almacenado en `detections_csv`, a partir de la detección de movimiento
    en las secuencias de imágenes del dataset

    Parameters
    ----------
    dataset_json : str
        Ruta del archivo JSON tipo COCO que se usará para obtener todas las fotos de una secuencia
        y descartar los falsos positivos del Megadetector
    detections_csv : str
        Ruta del archivo CSV que contiene las detecciones del Megadetector
    images_dir : str
        Ruta de la carpeta que contiene a las imágenes del dataset
    false_pos_dets_csv : str
        Ruta del archivo CSV donde se guardarán los falsos positivos resultantes con las columnas
        `seq_ids` y `det_ids`
    dest_dir : str
        Ruta de la carpeta donde se guardarás las imágenes resultantes
    false_pos_dets_ids : list, optional
        Lista con los ids de las detecciones que se consideran falsos positivos, by default None
    split_in_seqs : bool, optional
        Indica si las imágenes resultantes se guardarán en una carpeta con el nombre de la columna
        `seq_id` de `dets_df` o no, by default True
    max_score_false_pos : float, optional
        Score máximo que podrán tener las detecciones que se consideran falsos positivos para que
        sean filtradas, by default `MAX_SCORE_FALSE_POS`
    """
    dets_ds = ImagePredictionDataset.from_csv(
        source_path=detections_csv, images_dir=images_dir
    )
    dets_df = dets_ds.as_dataframe()
    if false_pos_dets_ids is None:
        false_pos = get_false_positive_dets_in_seqs(
            dets_ds,
            dataset_json,
            images_dir,
            sub_method="Acc",
            results_csv_path=false_pos_dets_csv,
        )
        false_pos_dets_ids = false_pos["det_ids"].values
    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        pool.starmap(
            draw_false_pos_det_in_seq,
            [
                (
                    item,
                    dets_df,
                    dest_dir,
                    false_pos_dets_ids,
                    split_in_seqs,
                    max_score_false_pos,
                )
                for item in dets_df["item"].unique()
            ],
        )


def draw_false_pos_det_in_seq(
    item,
    dets_df,
    dest_dir,
    false_pos_dets_ids,
    split_in_seqs,
    max_score_false_pos,
):
    """Función que filtra las detecciones de `item` en `dets_df` que se consideran falsos positivos
    y dibuja los bounding boxes resultantes en la imagen que se guardará en `dest_dir`

    Parameters
    ----------
    item : str
        Item del que se filtrarán las detecciones
    dets_df : pd.DataFrame
        Dataframe contiene las detecciones
    dest_dir : str
        Ruta de la carpeta donde se guardará la imagen resultante
    false_pos_dets_ids : list
        Lista con los ids de las detecciones que se consideran falsos positivos
    split_in_seqs : bool
        Indica si la imagen resultante se guardará en una carpeta con el nombre de la columna
        `seq_id` de `dets_df` o no
    max_score_false_pos : float
        Score máximo que podrán tener las detecciones que se consideran falsos positivos para que
        sean filtradas
    """
    dets_item = dets_df[
        (dets_df["item"] == item)
        & (dets_df["id"].isin(false_pos_dets_ids))
        & (dets_df["score"] < max_score_false_pos)
    ]
    if len(dets_item) > 0:
        if split_in_seqs:
            seq_dir = os.path.join(dest_dir, dets_item.iloc[0]["seq_id"])
            img_dest_path = os.path.join(seq_dir, os.path.basename(item))
            os.makedirs(seq_dir, exist_ok=True)
        else:
            img_dest_path = os.path.join(dest_dir, os.path.basename(item))
        bboxes = [
            [int(x) for x in row["bbox"].split(",")]
            for _, row in dets_item.iterrows()
        ]
        draw_bboxes_in_image(item, img_dest_path, bboxes)


def create_json_for_geo_priors(
    wcs_ds_path,
    wcs_location_json,
    inat_17_18_images_json,
    inat_17_18_categories,
    inat_17_18_mapping_classes,
    inat_17_images_json,
    inat_18_images_json,
    inat_17_location_json,
    inat_18_location_json,
    inat_21_dataset_json_file,
    inat_21_categories,
    inat_21_mapping_classes,
    inat_21_images_json,
    out_images_json,
    labelmap_path,
    add_label,
    exclude_cats,
    IWildcamBaseImageDataset=None,
):
    """Función que crea un archivo JSON que se pueda utilizar para entrenar un modelo que genere
    información geográfica a priori (geo_prior) usando el repositorio `geo_prior_tf`, a partir de
    los metadatos (localización y fecha) de los conjuntos de imágenes WCS, iNat 2017, 2018 y 2021

    Parameters
    ----------
    wcs_ds_path : str
        Ruta del archivo JSON tipo COCO con las anotaciones del dataset WCS
    wcs_location_json : str
        Ruta del archivo JSON que contiene la información de localización de la colección.
        P.e. `gps_locations.json`
    inat_17_18_images_json : str
        Ruta del archivo JSON tipo COCO con las anotaciones del dataset iNat 2017-2018
    inat_17_18_categories : str
        Ruta del archivo de texto con las categorías que se van a filtrar del dataset iNat
        2017-2018. Por lo general son las categorías comunes con el dataset WCS, pero también puede
        incluir nombres de categorías que no están en WCS, pero que posteriormente se mapearán con
        el parámetro `inat_17_18_mapping_classes`
    inat_17_18_mapping_classes : str
        Ruta del archivo CSV que mapeará los nombres de las categorías en el dataset iNat
        2017-2018 a los nombres de las categorías en el dataset WCS
    inat_17_images_json : str
        Ruta del archivo JSON tipo COCO de la colección iNat 2017 que se usará para obtener las
        fechas de las imágenes
    inat_18_images_json : str
        Ruta del archivo JSON tipo COCO de la colección iNat 2018 que se usará para obtener las
        fechas de las imágenes
    inat_17_location_json : str
        Ruta del archivo JSON que contiene la información de localización de la colección
        iNat 2017. P.e. `train2017_locations.json`
    inat_18_location_json : str
        Ruta del archivo JSON que contiene la información de localización de la colección
        iNat 2018. P.e. `train2018_locations.json`
    inat_21_dataset_json_file : str
        Ruta del archivo JSON tipo COCO con las anotaciones del dataset iNat 2021.
        P.e. `dataset_iNat_21_from_dets.json`
    inat_21_categories : str
        Ruta del archivo de texto con las categorías que se van a filtrar del dataset iNat
        2021. Por lo general son las categorías comunes con el dataset WCS, pero también puede
        incluir nombres de categorías que no están en WCS, pero que posteriormente se mapearán con
        el parámetro `inat_21_mapping_classes`
    inat_21_mapping_classes : str
        Ruta del archivo CSV que mapeará los nombres de las categorías en el dataset iNat
        2021 a los nombres de las categorías en el dataset WCS
    inat_21_images_json : str
        Ruta del archivo JSON tipo COCO de la colección iNat 2021 que se usará para obtener las
        fechas de las imágenes
    out_images_json : str
        Ruta del archivo JSON generado, y que se podrá utilizar como entrada en el módulo
        `geo_prior_tf`
    labelmap_path : str
        Ruta del archivo TXT que contiene la definición de labelmap del dataset, con el que se va
        a generar el contenido del campo 'categories' del archivo JSON resultante
    add_label : bool
        Bandera que indica si se va a incluir el campo 'annotations' en el archivo JSON resultante
        o no
    exclude_cats : str
        Cadena para filtrar las categorías (separadas por coma) que se quieren quitar del dataset
    IWildcamBaseImageDataset : ImageDataset, optional
        Clase de un dataset de imágenes que define los nombres de los campos de la colección WCS
        que se desea utilizar (p.e. `IWildcam2020ImageDataset`). By default `IWildcam2021ImageDataset`
    """
    FLDS = IWildcamBaseImageDataset.IMAGES_FIELDS
    # WCS
    if wcs_ds_path.endswith(".json"):
        wcs_images = IWildcamBaseImageDataset.from_json(
            wcs_ds_path, exclude_categories=exclude_cats
        )
    elif wcs_ds_path.endswith(".csv"):
        wcs_images = IWildcamBaseImageDataset.from_csv(wcs_ds_path)
    wcs_images_df = wcs_images.as_dataframe(
        columns=[FLDS.LOCATION, FLDS.DATE_CAPTURED]
    )
    wcs_locs_json = json.load(open(wcs_location_json))
    wcs_locs = [
        {
            FLDS.LOCATION: str(k),
            "latitude": v["latitude"],
            "longitude": v["longitude"],
        }
        for k, v in wcs_locs_json.items()
    ]
    wcs_locs_df = pd.DataFrame(wcs_locs)
    wcs_df = pd.merge(wcs_images_df, wcs_locs_df, on=FLDS.LOCATION)
    wcs_df_cols = ["item", "id", "latitude", "longitude", FLDS.DATE_CAPTURED]
    if "label" in wcs_df.columns:
        wcs_df_cols.append("label")
    wcs_df = wcs_df[wcs_df_cols]

    if inat_17_18_images_json and inat_21_dataset_json_file:
        # iNat 2017 2018
        inat_17_18_ds = ConabioImageDataset.from_json(
            source_path=inat_17_18_images_json, categories=inat_17_18_categories
        )
        inat_17_18_ds.map_categories(inat_17_18_mapping_classes)
        inat_17_imgs_df = pd.DataFrame(
            json.load(open(inat_17_images_json))["images"]
        )
        inat_17_locs_df = pd.DataFrame(json.load(open(inat_17_location_json)))
        inat_17_df = pd.merge(
            inat_17_imgs_df, inat_17_locs_df, how="left", on="id"
        )
        inat_17_df["file_name"] = inat_17_df["file_name"].map(
            lambda x: x.replace("train_val_images", "inaturalist_2017")
        )
        inat_18_imgs_df = pd.DataFrame(
            json.load(open(inat_18_images_json))["images"]
        )
        inat_18_locs_df = pd.DataFrame(json.load(open(inat_18_location_json)))
        inat_18_df = pd.merge(
            inat_18_imgs_df, inat_18_locs_df, how="left", on="id"
        )
        inat_18_df["file_name"] = inat_18_df["file_name"].map(
            lambda x: x.replace("train_val2018", "inaturalist_2018")
        )
        inat_17_18_df = pd.concat([inat_17_df, inat_18_df], ignore_index=True)
        inat_17_18_df = inat_17_18_df.rename(
            columns={
                "file_name": "item",
                "date": "datetime",
                "lat": "latitude",
                "lon": "longitude",
            }
        )
        inat_17_18_df = inat_17_18_df[
            ["item", "id", "latitude", "longitude", "datetime"]
        ]
        inat_17_18_df = pd.merge(
            inat_17_18_ds.as_dataframe(), inat_17_18_df, how="left", on="item"
        ).rename(columns={"id_x": "id"})
        inat_17_18_df = inat_17_18_df[
            ["item", "label", "id", "latitude", "longitude", "datetime"]
        ]
        inat_17_18_df = inat_17_18_df[~inat_17_18_df.latitude.isnull()]
        inat_17_18_df["datetime"] = inat_17_18_df["datetime"].map(
            lambda x: datetime.strptime(x, "%Y-%m-%d").strftime(STD_DATEFORMAT)
        )

        # iNat 2021
        inat_21_ds = ConabioImageDataset.from_json(
            source_path=inat_21_dataset_json_file, categories=inat_21_categories
        )
        inat_21_ds.map_categories(inat_21_mapping_classes)
        inat_21_df = inat_21_ds.as_dataframe(columns=["latitude", "longitude"])
        inat_21_df = inat_21_df[
            ["item", "label", "id", "latitude", "longitude"]
        ]
        inat_21_images = pd.DataFrame(
            json.load(open(inat_21_images_json))["images"]
        )
        inat_21_images = inat_21_images.rename(
            columns={"file_name": "item", "date": "datetime"}
        )
        inat_21_images = inat_21_images[["item", "datetime"]]
        inat_21_df = pd.merge(inat_21_df, inat_21_images, how="left", on="item")
        inat_21_df["datetime"] = inat_21_df["datetime"].map(
            lambda x: datetime.strptime(x, OUT_DATEFORMAT).strftime(
                STD_DATEFORMAT
            )
        )
        df_result = pd.concat(
            [wcs_df, inat_17_18_df, inat_21_df], ignore_index=True
        )
        df_result = pd.concat([wcs_df, inat_17_18_df], ignore_index=True)
    else:
        df_result = wcs_df

    df_result["datetime"] = df_result["datetime"].map(
        lambda x: datetime.strptime(x, STD_DATEFORMAT).strftime(OUT_DATEFORMAT)
    )
    labelmap = read_labelmap_file(labelmap_path)
    inverse_labelmap = {
        cat_name: cat_id for cat_id, cat_name in labelmap.items()
    }
    images = [
        {
            "id": row["id"],
            "latitude": row["latitude"],
            "longitude": row["longitude"],
            "date": row["datetime"],
        }
        for _, row in df_result.iterrows()
    ]
    categories = [
        {"id": cat_id, "name": cat_name}
        for cat_id, cat_name in labelmap.items()
    ]
    json_data = {"info": {}, "images": images, "categories": categories}
    if "label" in df_result.columns and add_label:
        anns = [
            {
                "image_id": row["id"],
                "category_id": inverse_labelmap.get(
                    row["label"], inverse_labelmap["empty"]
                ),
            }
            for _, row in df_result.iterrows()
        ]
        json_data["annotations"] = anns
    with open(out_images_json, "w") as outfile:
        json.dump(json_data, outfile)


# endregion


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--create_wcs_json_from_dets_and_anns",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "--create_wcs_json_from_dets_csv", default=False, action="store_true"
    )
    parser.add_argument(
        "--visualize_false_positives_in_ds", default=False, action="store_true"
    )
    parser.add_argument(
        "--ensemble_classification_models", default=False, action="store_true"
    )

    parser.add_argument("--detections_file", default=None, type=str)
    parser.add_argument("--annotations_file", default=None, type=str)
    parser.add_argument("--include_empty", default=True, type=bool)
    parser.add_argument("--json_filename", default=None, type=str)
    parser.add_argument("--images_dir_dest", default=None, type=str)
    parser.add_argument("--categories", default=None, type=str)
    parser.add_argument("--exclude_cats", default=None, type=str)
    parser.add_argument("--detections_csv_file", default=None, type=str)
    parser.add_argument("--classification_csv_file", default=None, type=str)
    parser.add_argument("--images_dir", default=None, type=str)
    parser.add_argument("--aux_json_detections_file", default=None, type=str)
    parser.add_argument("--false_pos_dets_csv", default=None, type=str, help="")
    parser.add_argument(
        "--max_score_false_pos",
        default=MAX_SCORE_FALSE_POS,
        type=float,
        help="",
    )
    parser.add_argument(
        "--min_score_detections", default=None, type=float, help=""
    )
    parser.add_argument("--bboxes_min_longest_side", default=None, type=float)
    parser.add_argument("--max_diff_greater_conf", default=None, type=float)
    parser.add_argument("--models_names", default=None, type=str)
    parser.add_argument("--item", default=None, type=str)
    parser.add_argument("--max_classifs", default=1, type=int)

    args = parser.parse_args()

    if args.create_wcs_json_from_dets_and_anns:
        create_wcs_json_from_dets_and_anns(
            detections_file=args.detections_file,
            annotations_file=args.annotations_file,
            include_empty=args.include_empty,
            output_json_name=args.json_filename,
            bboxes_min_longest_side=args.bboxes_min_longest_side,
            max_diff_greater_conf=args.max_diff_greater_conf,
        )
    if args.create_wcs_json_from_dets_csv:
        create_wcs_json_from_detections_csv(
            detections_csv_file=args.detections_csv_file,
            annotations_file=args.annotations_file,
            images_dir=args.images_dir,
            output_json_name=args.json_filename,
            aux_json_detections_file=args.aux_json_detections_file,
            false_pos_dets_csv=args.false_pos_dets_csv,
            max_score_false_pos=args.max_score_false_pos,
            min_score_detections=args.min_score_detections,
            categories=args.categories,
        )
    if args.visualize_false_positives_in_ds:
        visualize_false_positives_in_ds(
            dataset_json=args.json_filename,
            detections_csv=args.detections_file,
            images_dir=args.images_dir,
            false_pos_dets_csv=args.false_pos_dets_csv,
            dest_dir=args.images_dir_dest,
        )
    if args.ensemble_classification_models:
        ensemble_classification_models(
            models_names=args.models_names,
            classifs_csv=args.classification_csv_file,
            images_dir=args.images_dir,
            max_classifs=args.max_classifs,
            item=args.item,
        )
