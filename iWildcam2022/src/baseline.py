#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import pandas as pd
import argparse
from collections import defaultdict

from conabio_ml.utils.logger import get_logger
from conabio_ml_vision.datasets.datasets import ImagePredictionDataset

from utils_2022 import *

logger = get_logger(__name__)

res_path = os.path.join(results_path, 'baseline')


def get_optimal_score_thres():
    train_ds = IWildcam2022ImageDataset.from_json(iwild_train_anns_json)
    counts_dict = pd.read_csv(iwild_train_seq_cnts_csv).set_index("seq_id").to_dict()

    train_ds.set_images_info_field_by_expr(
        "count", lambda x: counts_dict["count"].get(x.seq_id, np.nan))
    train_ds.set_data_field_by_expr(
        "partition", lambda x: "train" if x.seq_id in counts_dict["count"] else "test")
    train_ds.split_by_column("partition").filter_by_column("partition", "train")
    train_ds.set_images_dir(
        images_dir="dummy_images_dir",
        append_partition_to_item_path=True,
        validate_filenames=False)
    train_df = train_ds.as_dataframe()
    train_items = train_ds.get_unique_items()

    item_to_seq = train_df[['item', 'seq_id']].set_index('item').to_dict('index')

    dets_ds = ImagePredictionDataset.from_json(
        iwild_train_dets_json,
        images_dir="dummy_images_dir",
        validate_filenames=False,
        score_threshold=0)
    dets_ds.filter_by_column("item", train_items)
    dets_df = dets_ds.as_dataframe()
    dets_df['seq_id'] = dets_df.apply(
        lambda x, item_to_seq: item_to_seq[x['item']]['seq_id'], axis=1, item_to_seq=item_to_seq)

    seqs_ids = train_df.seq_id.unique()

    res_errors = defaultdict(list)
    for score_thres in np.linspace(INIT_THRES, END_THRES, num=NUM_STEPS):
        thres_str = f"{int(round(score_thres, 2)*100)}"
        dets_df_thres = dets_df[dets_df['score'] >= score_thres]

        sum_error = 0
        for seq_id in seqs_ids:
            seq_dets = dets_df_thres[dets_df_thres.seq_id == seq_id]
            counts_pred = seq_dets['item'].value_counts().max() if len(seq_dets) > 0 else 0
            counts_true = train_df[train_df.seq_id == seq_id]['count'].max()
            sum_error += abs(counts_pred - counts_true)
        res_errors["Threshold"].append(thres_str)
        res_errors["MAE"].append(sum_error / len(seqs_ids))

    data_error_x_thres = pd.DataFrame(res_errors)
    data_error_x_thres.set_index('Threshold')
    data_error_x_thres.to_csv(os.path.join(res_path, "MAE_thres.csv"), index=False)
    opt_thres = data_error_x_thres.sort_values(by='MAE', ascending=True).iloc[0]['Threshold']
    opt_MAE = data_error_x_thres.sort_values(by='MAE', ascending=True).iloc[0]['MAE']
    print(f'Optimal threshold: {opt_thres}, gives a MAE value: {opt_MAE}')
    return opt_thres


def create_baseline_submission():
    test_ds = IWildcam2022ImageDataset.from_json(iwild_test_json)
    test_ds.set_data_field_by_expr("partition", lambda x: "test")
    test_ds.split_by_column("partition")
    test_ds.set_images_dir(
        images_dir="dummy_images_dir",
        append_partition_to_item_path=True,
        validate_filenames=False)
    test_df = test_ds.as_dataframe()
    test_items = test_ds.get_unique_items()

    item_to_seq = pd.Series(test_df['seq_id'].values, index=test_df['item'].values).to_dict()
    dets_ds = ImagePredictionDataset.from_json(
        iwild_train_dets_json,
        images_dir="dummy_images_dir",
        validate_filenames=False,
        score_threshold=0)
    dets_ds.filter_by_column("item", test_items)
    dets_df = dets_ds.as_dataframe()
    dets_df['seq_id'] = dets_df.apply(
        lambda x, item_to_seq: item_to_seq[x['item']], axis=1, item_to_seq=item_to_seq)

    opt_thres = 0.75  # Optimal threshold
    dets_df_thres = dets_df[dets_df['score'] >= opt_thres]

    seqs_counts_df = dets_df_thres.groupby('seq_id').apply(
        lambda x: x['item'].value_counts().max()).to_frame()
    seqs_counts_df = seqs_counts_df.reset_index().rename(columns={'seq_id': 'Id', 0: 'Predicted'})
    seqs_counts_df.to_csv(os.path.join(res_path, "submission_baseline.csv"), index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--get_optimal_score_thres", default=False, action="store_true")
    parser.add_argument("--create_baseline_submission", default=False, action="store_true")

    parser.add_argument("--detections_file", default=None, type=str)

    args = parser.parse_args()

    if args.get_optimal_score_thres:
        get_optimal_score_thres()
    if args.create_baseline_submission:
        create_baseline_submission()
