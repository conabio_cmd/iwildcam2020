#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from conabio_ml.utils.logger import get_logger

from conabio_ml_vision.datasets.datasets import ImageDataset

from scripts.utils import *
from params import *

logger = get_logger(__name__)


class IWildcam2022ImageDataset(ImageDataset):
    """Definición de un dataset de la colección WCS para la competición iWildcam2022"""

    class IMAGES_FIELDS:
        ID = "id"
        FILENAME = "file_name"
        WIDTH = "width"
        HEIGHT = "height"
        DATE_CAPTURED = "datetime"
        LOCATION = "location"
        SUBLOCATION = "sub_location"
        SEQ_NUM_FRAMES = "seq_num_frames"
        SEQ_ID = "seq_id"
        SEQ_FRAME_NUM = "seq_frame_num"

        NAMES = [
            ID,
            FILENAME,
            WIDTH,
            HEIGHT,
            DATE_CAPTURED,
            LOCATION,
            SUBLOCATION,
            SEQ_NUM_FRAMES,
            SEQ_ID,
            SEQ_FRAME_NUM,
        ]
        DEFAULTS = {SUBLOCATION: 0}
