import os

INIT_THRES = 0.
END_THRES = 1.
STEP_THRES = 0.05
NUM_STEPS = int((END_THRES-INIT_THRES)*(.1/(STEP_THRES))*10)+1

iwild_data_dir = os.path.join('..', 'data', 'iwildcam')
iwild_metadata_dir = os.path.join(iwild_data_dir, 'metadata')
results_path = os.path.join('..', 'results')
# Metadata
iwild_train_anns_json = os.path.join(iwild_metadata_dir, 'iwildcam2022_train_annotations.json')
iwild_test_json = os.path.join(iwild_metadata_dir, 'iwildcam2022_test_information.json')
iwild_train_seq_cnts_csv = os.path.join(iwild_metadata_dir, 'train_sequence_counts.csv')
iwild_train_dets_json = os.path.join(iwild_metadata_dir, 'iwildcam2022_mdv4_detections.json')

