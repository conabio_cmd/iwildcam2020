# Plan para implementar

- ~~Hacer una evaluación de los conteos de individuos en cada secuencia, variando el umbral del MD, tomando como referencia los conteos proporcionados en la competencia, calculando el MAE y sacando el valor óptimo del umbral.~~
- ~~Hacer un baseline usando el umbral óptimo que se obtuvo en el punto anterior: Se filtran las detecciones por el score del umbral óptimo. Se agrupan las detecciones por `seq_id` y se hace un count_values() de `item` y se saca el `max()` en cada secuencia, que se toma como su valor de conteo.~~
- **Probar métodos para descartar falsos positivos**:
    - Usar un modelo entrenado con clases de especies y `empty`, para filtrar los crops que sean clasificados como `empty`.
        - Usar el ensamble de modelos de crops de la competencia pasada
        - Usar también el ensamble de modelos de images completas para ayudar a filtrar fotos vacías
    - Detección de movimiento

- Segmentación de objetos: [DeepMAC](https://www.kaggle.com/code/vighneshbgoogle/iwildcam-visualize-instance-masks/notebook)
- Tracking de objetos: Deep sort
- Data augmentation (RandAugment)
- Mejorar clasificación de especies para poder descartar más falsos positivos:
    - Hacer tuning de los hiperparámetros de los modelos y entrenar algunas capas convolucionales de los modelos [link](https://keras.io/guides/transfer_learning/)
    - Geoprior (with focal loss)
    - Usar imágenes satelitales
    - Deep Sort para class imbalance
    - Class imbalance: BAGS
